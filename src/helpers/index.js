import { IS_TEST } from "../store/constants";
export const sendMetrik = (name, payload) => {
  const consoleMetrik = (id, type, name, payload) => console.log(name, payload);
  const ym = IS_TEST ? consoleMetrik : window.ym || consoleMetrik;
  ym(53183122, "reachGoal", `Virus.${name}`, payload);
};
