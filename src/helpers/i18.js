import translation from "./translation";

export const FALLBACK_LANG = "en";
const detectLang = () => {
  if (localStorage.getItem("covidlang"))
    return localStorage.getItem("covidlang");
  if (window.navigator.languages)
    return window.navigator.languages.some(
      (item) => item === "ru" || item === "ru-RU"
    )
      ? "ru"
      : FALLBACK_LANG;
  const language = window.navigator.userLanguage || window.navigator.language;
  return language === "ru" || language === "ru-RU" ? "ru" : FALLBACK_LANG;
};

export const lang = detectLang();

export const text = (phrase) => {
  return translation[phrase] ? translation[phrase][lang] || phrase : phrase;
};
