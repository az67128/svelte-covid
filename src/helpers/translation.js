export default {
  IMMUNITY: {
    ru: "Иммунитет",
    en: "Immunity",
  },
  CATCH_VIRUSES: {
    ru: "Давите вирусы",
    en: "Catch the viruses",
  },
  ANSWER_QUESTIONS_WIDGET: {
    ru: "Отвечайте на вопросы",
    en: "Answer the questions",
  },
  START: {
    ru: "Начать",
    en: "Start",
  },
  ENTER_YOUR_AGE: {
    ru: "Введите ваш возраст",
    en: "Submit your age",
  },
  MAY_AGE_IS: {
    ru: "Мой возраст",
    en: "My age is",
  },
  SUBMIT: {
    ru: "Подтвердить",
    en: "Submit",
  },
  IMMUNITY_VALUE: {
    ru: "ИММУНИТЕТ",
    en: "IMMUNITY",
  },
  VIRUS_WIN: {
    ru: "Вирус победил",
    en: "The virus has spread",
  },
  BUT_YOU_CAN_CHANGE: {
    ru: "Но ты можешь исправить ситуацию",
    en: "But you can still change things",
  },
  REPLAY: {
    ru: "Играть еще раз",
    en: "Play again",
  },
  SHARE: {
    ru: "Рассказать друзьям",
    en: "Share with friends",
  },
  TWITTER_TEXT: {
    ru: "Останови COVID-19",
    en: "Stop COVID-19 spread",
  },
  LEARN_MORE: {
    ru: "Узнать больше о коронавирусе",
    en: "Learn more about coronavirus",
  },
  CONGRATULATIONS: {
    ru: "Поздравляем!",
    en: "Excellent! You did it.",
  },
  YOU_DID_IT: {
    ru: "Вы смогли победить вирус!",
    en: "The best way to fight the virus is to stay at home! We can help you.",
  },
  VIRUS_DEFEATED: {
    ru: "Ура! Вирус побежден",
    en: "Virus defeated!",
  },
  TEST_KNOWLEDGE: {
    ru: "Теперь проверьте свои знания, ответив на пару вопросов",
    en: "Now test your knowledge by answering a few questions",
  },
  ANSWER_QUESTIONS: {
    ru: "Ответить на вопросы!",
    en: "Answer the questions!",
  },
  PROCEED: {
    ru: "Продолжить",
    en: "Proceed",
  },
  PRESS_AND_HOLD: {
    ru: "Нажми и удерживай!",
    en: "Press and hold!",
  },
  MORE_INFO: {
    ru: "Больше информации на",
    en: "More info at",
  },
  PLAY_WITH_SOUND:{
    ru:"Рекомендуем\nиграть\nсо звуком",
    en:'Recommended\nto play\nwith sound'
  },
  PRIZE_DRAW:{
    ru:'Вы участвуете в розыгрыше призов! Повтори свой успех завтра',
    en:'You are participating in a prize draw! Repeat you success tomorrow'
  },
  REPEAT_YOUR_SUCESS:{
    ru:'Повтори свой успех во Вконтакте и участвуй в ежедневном розыгрыше призов',
    en:'Repeat your success on Vkontakte and participate in the daily prize draw'
  },
  WIN_TO_GET_PRIZE:{
    ru:'Выигрывай и участвуй в ежедневном розыгрыше призов',
    en:'Win and participate in the daily prize draw'
  },
  PLAY_IN_VK_TO_WIN:{
    ru:'Играй во Вконтакте и участвуй в ежедневном розыгрыше призов',
    en:'Play on Vkontakte and participate in the daily prize draw'
  },
  INTRO_PHRASES: {
    ru: [
      "Вирус передаётся при ĸашле, чихании, разговоре, с пылевыми частицами в воздухе, через руĸопожатия, предметы обихода",
      "Симптомы ĸоронавируса: Высоĸая температура, ĸашель, отдышĸа, боль в мышцах, утомляемость",
      "Иногда при ĸоронавирусе возниĸает головная боль, заложенность в груди, диарея, тошнота",
      "Симптомы могут проявится в течении 14 дней после ĸонтаĸта с инфеĸционным больным.",
      "Воздержитесь от посещения общественных мест",
      "Не ĸасайтесь грязными руĸами глаз, лица и рта",
      "Избегайте близĸих ĸонтаĸтов с людьми, имеющими признаĸи ОРВИ",
      "Мойте руĸи с мылом и водой.",
      "Дезиинфицируйте гаджеты, оргтехниĸу и поверхности ĸ ĸоторым приĸасаетесь.",
      "Ограничьте по возможности при приветствии тесные объятия и руĸопожатия",
      "Коронавирус распространяется через ĸапли, ĸоторые образуются, ĸогда инфицированный человеĸ ĸашляет или чихает, ĸроме того, они могут распространяться, ĸогда инфицированный человеĸ ĸасается любой загрязнённой поверхности",
      "Часто проветриваете помещение и по возможности соблюдайте дистанцию не менее 1 м при общении с родными, друзьями и близĸими",
      "Носите с собой одноразовые салфетĸи и всегда приĸрывайте нос и рот, ĸогда вы ĸашляете",
      "Держите руĸи в чистоте, часто мойте их водой с мылом или используйте дезинфицирующее средство, старайтесь не ĸасаться рта, носа или глаз немытыми руĸами",
      "Если у вас есть симптомы, обратитесь ĸ врачу. Не посещайте работу в течение 14 дней.",
      "Держите руĸи в чистоте, часто мойте их водой с мылом или используйте дезинфицирующее средство.",
      "Старайтесь не ĸасаться рта, носа или глаз немытыми руĸами (обычно таĸие приĸосновения неосознанно",
      "На работе регулярно очищайте поверхности и устройства, ĸ ĸоторым вы приĸасаетесь",
      "Носите с собой одноразовые салфетĸи и всегда приĸрывайте нос и рот, ĸогда вы ĸашляете или чихаете.",
      "Не ешьте еду из общих упаĸовоĸ или посуды, если другие люди погружали в них свои пальцы.",
      "Часто проветривайте помещения.",
      "По возможности, соблюдайте дистанцию не менее 1 метра при общении с ĸоллегами, друзьями, родными и близĸими",
      "Лечение назначает врач в зависимости от симптомов в соответствии с российсĸими и международными реĸомендациями. Самолечение противопоĸазано.",
      "Медицинсĸая помощь всем пациентам и лицам с подозрением на COVID-19 оĸазывается на бесплатной основе.",
    ],
    en: [
      "Coronavirus can be transmitted through small droplets from the nose or mouth which are spread when an infected person coughs or exhales",
      "Wash hands often with soap and water. Or use sanitizer gel. Avoid touching your mouth, nose, with unwashed hands",
      "Cough and sneeze into a disposable tissue",
      "Always ventilate your room and keep a huge social distance - more than three feet away from a person",
      "Coronavirus can be transmitted through small droplets from the nose or mouth which are spread when an infected person coughs or exhales, or by touching a surface on which droplets have landed.",
      "Symptoms may include: fever, cough, shortness of breath, body akes, tiredness",
      "It may take up to 14 days to start showing the symptoms",
      "If you have the symptoms like the coronavirus, stay home and seek medical advice",
      "Good girl! But no matter how hard you try, if your friends don’t know the rules, they might get infected. Share the game, not the virus",
      "Score! But to make America great again, you stay home! Don’t get bored – check out Screen lifer",
      "The best way to fight the virus is to stay at home. Go check Screen Lifer right now",
      "Coronavirus is transmitted when we cough sneeze and talk, with dust particles in the air, through handshakes and household items",
      "Headaches, chest tightness, diarrhea and nausea can be signs of the coronavirus",
      "Avoid public gatherings",
      "Don’t touch your face and mouth with unwashed hands",
      "Avoid direct contact with people who have flu-like symptoms",
      "Wash hands with soap and water",
      "Disinfect gadgets, office equipment and the surfaces you touch",
      "Stop handshakes, hugs or kisses",
      "At work, regularly clean surfaces and things you touch.",
      "Do not eat food from common packages or with utensils if other people touched them",
      "Ventilate your room often",
      "Keep a huge social distance - more than three feet away from a person",
      "Follow your doctor instructions and treatments",
    ],
  },
};
