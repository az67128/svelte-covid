export default {
  vk: [
    {
      title: "Узнай больше о вирусе",
      content:
        'Читай тут <a href="https://vk.com/stopcoronavirusrf" target="_blank">стопкоронавирус</a>',
      link: "https://vk.com/stopcoronavirusrf",
      id: "vk стопкоронавирус.рф",
    },
    {
      title: "Что делать на карантине?",
      content:
        'Развлекись на <a href="https://vk.com/screenlifer" target="_blank">screenlifer</a>',
      link: "https://vk.com/screenlifer",
      id: "vk screenlifer",
    },
    {
      title: "Защити себя",
      content:
        'Закажи наобходимое на <a href="https://vk.com/ae_app#/" target="_blank">aliexpress</a>',
      link: "https://vk.com/ae_app#/",
      id: "vk aliexpress",
    },
  ].sort(() => 0.5 - Math.random()),
  widget: [
    {
      title: "Узнай больше о вирусе",
      content:
        'Читай тут <a href="https://стопкоронавирус.рф" target="_blank">стопкоронавирус.рф</a>',
      link: "https://стопкоронавирус.рф",
      id: "стопкоронавирус.рф",
    },
    {
      title: "Что делать на карантине?",
      content:
        'Развлекись на <a href="https://screenlifer.com/quarantine" target="_blank">screenlifer.com</a>',
      link: "https://screenlifer.com/quarantine",
      id: "screenlifer",
    },
    {
      title: "Защити себя",
      content:
        'Закажи наобходимое на <a href="https://aliexpress.ru" target="_blank">aliexpress.ru</a>',
      link: "https://aliexpress.ru",
      id: "aliexpress",
    },
  ].sort(() => 0.5 - Math.random()),
};
