/* DISCLAMER
 * deadline was only 6 hours... so, sorry for this piece of shit
 */
import Sprite from "./sprite";
import {
  pointer,
  isHolding,
  immunity,
  gameView,
  preloadIndicator,
  replayCount,
} from "../store";
import { BASE_URL, GAME_VIEWS, IS_WIDGET } from "../store/constants";

import {
  VIRUS_RADIUS,
  VIRUS_TOUCH_RADIUS,
  INITIAL_VIRUS_VELOCITY,
  VIRUS_VELOCITY_CHANGE,
  RELEASE_VIRUS_VELOCITY_CHANGE,
  VIRUS_VELOCITY_REPLAY_VALUE,
  VIRUS_PROTECTED_TIME,
} from "../store/constants";

import { get } from "svelte/store";
import { sendMetrik } from "../helpers";

let requestAnimationFrameId = null;
let ctx = null;
let canvas = null;

let isVurusProtected = false;
const unprotectVirus = () => {
  if (!isVurusProtected) return;
  if (Date.now() - isVurusProtected >= VIRUS_PROTECTED_TIME)
    isVurusProtected = false;
};

const getBase64Image = async (url) => {
  let blob = null;
  for (let i = 1; i < 5; i++) {
    try {
      blob = await fetch(url).then((resp) => {
        if (!resp.ok) {
          throw Error(`is not ok: ` + resp.status);
        } else {
          preloadIndicator.assetLoaded();
        }
        return resp.blob();
      });
    } catch (err) {
      preloadIndicator.assetNotLoaded(url, err.toString());
    }
    if (blob) break;
  }
  return await new Promise((resolve) => {
    const reader = new FileReader();
    reader.onload = function () {
      resolve(this.result);
    };
    reader.readAsDataURL(blob);
  });
};

const virusImage = new Image();
const backgroundCirusImage = new Image();
const virusBoomImage = new Image();

const getImages = () => {
  const baseFileurl = IS_WIDGET
    ? `${BASE_URL}/assets/?file=`
    : `${BASE_URL}/assets/`;
  getBase64Image(`${baseFileurl}virusAll512.png`).then(
    (base64) => (virusImage.src = base64)
  );
  getBase64Image(`${baseFileurl}virus200.png`).then(
    (base64) => (backgroundCirusImage.src = base64)
  );
  getBase64Image(`${baseFileurl}boom512.png`).then(
    (base64) => (virusBoomImage.src = base64)
  );
};
getImages();

let backgroundViruses = [];
let virusSprite = null;
let boomSprite = null;

gameView.subscribe((state) => {
  if (!boomSprite) return;
  if (state === GAME_VIEWS.QUIZ_INTRO) {
    boomSprite.position.x.set(get(virusSprite.position.x) - 70);
    boomSprite.position.y.set(get(virusSprite.position.y) - 70);
  }
  if (state === GAME_VIEWS.GAME) {
    virusSprite.velocity = INITIAL_VIRUS_VELOCITY;
    setVirusImage(false);
  }
});
const setVirusImage = (isVirusHolding) => {
  virusSprite.yShift = isVirusHolding ? 512 : 0;
};
export const start = (canvasElement) => {
  canvas = canvasElement;
  ctx = canvas.getContext("2d");

  virusSprite = new Sprite({
    ctx: canvas.getContext("2d"),
    image: virusImage,
    width: 8192,
    height: 512,
    ticksPerFrame: 2,
    radius: VIRUS_RADIUS,
    velocity: 10,
    isGameObject: true,
  });

  boomSprite = new Sprite({
    ctx: canvas.getContext("2d"),
    image: virusBoomImage,
    width: 8192,
    height: 512,
    ticksPerFrame: 2,
    radius: 200,
    velocity: 0,
    repeat: false,
  });

  setVirusImage(false);
  backgroundViruses = new Array(40).fill(0).map(
    (i) =>
      new Sprite({
        ctx: canvas.getContext("2d"),
        image: backgroundCirusImage,
        width: 3200,
        height: 200,
        ticksPerFrame: 2,
      })
  );
  cancelAnimationFrame(requestAnimationFrameId);
  gameLoop();
};

let runAwayTimeout = null;
let lastHoldTime = null;
const checkCollision = () => {
  const { x, y } = pointer;
  const distance =
    Math.pow(get(virusSprite.position.x) + VIRUS_TOUCH_RADIUS - x, 2) +
    Math.pow(get(virusSprite.position.y) + VIRUS_TOUCH_RADIUS - y, 2);

  const userIsHoldingVirus =
    (x === 0 && y === 0) || isVurusProtected
      ? false
      : distance < Math.pow(VIRUS_TOUCH_RADIUS, 2);
  isHolding.update((state) => {
    if (state === userIsHoldingVirus) return state;

    setVirusImage(userIsHoldingVirus);
    if (userIsHoldingVirus) {
      sendMetrik("Pressed");
      (lastHoldTime = Date.now()),
        backgroundViruses.forEach((item) => item.updateVelocity(true));
      clearTimeout(runAwayTimeout);
      virusSprite.velocity =
        INITIAL_VIRUS_VELOCITY *
        VIRUS_VELOCITY_CHANGE *
        Math.pow(VIRUS_VELOCITY_REPLAY_VALUE, get(replayCount));
      virusSprite.setNewTargetPosition(true);
    } else {
      sendMetrik("Released", {
        value: Math.round((Date.now() - lastHoldTime) / 100) / 10,
      });
      lastHoldTime = null;
      backgroundViruses.forEach((item) => item.updateVelocity());
      isVurusProtected = Date.now();
      virusSprite.velocity =
        INITIAL_VIRUS_VELOCITY * RELEASE_VIRUS_VELOCITY_CHANGE;
      virusSprite.setNewTargetPosition();
      runAwayTimeout = setTimeout(
        () => (virusSprite.velocity = INITIAL_VIRUS_VELOCITY),
        2000
      );
    }
    return userIsHoldingVirus;
  });
};

const gameLoop = () => {
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  unprotectVirus();
  if (get(immunity) >= 100) {
    boomSprite.render();
  } else {
    boomSprite.frameIndex = 0;

    backgroundViruses
      .slice(
        0,
        Math.round(((100 - get(immunity)) / 100) * backgroundViruses.length)
      )
      .forEach((item) => item.render());
    checkCollision();
    virusSprite.render();
  }

  requestAnimationFrameId = requestAnimationFrame(gameLoop);
};
