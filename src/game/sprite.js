import { tweened } from "svelte/motion";
import { get } from "svelte/store";
import {isHolding, gameWidth, gameHeight} from '../store'

export default class Sprite {
  constructor({
    ctx,
    image,
    ticksPerFrame = 0,
    numberOfFrames = 16,
    width,
    height,
    velocity = Math.floor(Math.random() * 5) + 1,
    radius = Math.floor(Math.random() * 15) + 10,
    position = {
      x: tweened(Math.random() * get(gameWidth), { duration: 0 }),
      y: tweened(Math.random() * get(gameHeight), { duration: 0 })
    },
    isGameObject = false,
    repeat = true
  }) {
    this.ctx = ctx;
    this.image = image;
    this.frameIndex = 0;
    this.tickCount = 0;
    this.ticksPerFrame = ticksPerFrame;
    this.numberOfFrames = numberOfFrames;
    this.width = width;
    this.height = height;
    this.direction = 1;
    this.position = position;
    this.velocity = velocity;
    this.radius = radius;
    this.isGameObject = isGameObject;
    this.targetPosition = { x: 0, y: 0 };
    this.repeat = repeat;
    this.yShift = 0;
    this.setNewTargetPosition();
  }

  updateVelocity(force=false){
    const min = force ? 5 : 1;
    const max = force ? 10 : 5;
    this.velocity = Math.floor(Math.random() * max) + min
    this.setNewTargetPosition(true)
  }

  updateSprite({image, width, height}){
    this.image = image;
    this.width = width;
    this.height  = height;
    // this.numberOfFrames = numberOfFrames;
    this.frameIndex = 0;
    this.direction=1
    this.updateAnimation();
  }

  setNewTargetPosition(changeDirection = true) {
      const diametr = this.radius * 1.5;
      const isHoldingVirus = get(isHolding)
      const minX = this.isGameObject && isHoldingVirus ? 0 : -diametr;
      const maxX = this.isGameObject && isHoldingVirus ? get(gameWidth) - diametr : get(gameWidth);
      const minY = this.isGameObject && isHoldingVirus ? 0 : -diametr;
      const maxY = this.isGameObject && isHoldingVirus ? get(gameHeight) - diametr : get(gameHeight);
      
      
    if (changeDirection) {
      this.targetPosition.x =
        Math.random() * maxX + minX;
      this.targetPosition.y =
        Math.random() * maxY + minY;
    }
    const length = Math.sqrt(
      Math.pow(this.targetPosition.x - get(this.position.x), 2) +
        Math.pow(this.targetPosition.y - get(this.position.y), 2)
    );
    const duration = (length / this.velocity) * 100;
    this.position.x.set(this.targetPosition.x, { duration });
    this.position.y.set(this.targetPosition.y, { duration });
  }

  updatePosition() {
    if (this.targetPosition.x === get(this.position.x))
      this.setNewTargetPosition();
  }

  updateAnimation() {
    this.tickCount++;
    if (this.tickCount > this.ticksPerFrame) {
      this.tickCount = 0;
      this.frameIndex = this.frameIndex + this.direction;
      if(!this.repeat) return
      if (this.frameIndex === this.numberOfFrames - 1 || this.frameIndex === 0)
        this.direction = -this.direction;
    }
  }

  render() {
    this.updatePosition();
    this.updateAnimation();
    this.ctx.drawImage(
      this.image,
      (this.frameIndex * this.width) / this.numberOfFrames,
      this.yShift,
      this.width / this.numberOfFrames,
      this.height,
      get(this.position.x),
      get(this.position.y),
      this.radius*2,
      this.radius*2
      //   this.width / this.numberOfFrames,
      //   this.height
    );

  }
}
