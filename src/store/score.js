export const sendVkScore = (gameTime, vkParams) => {
    fetch("https://quarantinegame.azurewebsites.net/api/score", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        Line: vkParams.line,
        Key: vkParams.key,
        Score: gameTime,
        FirstName: vkParams.firstName,
        LastName: vkParams.lastName,
        Id : vkParams.id,
        Platform: "VK",
        Referer: vkParams.referer
      }),
    })
      .then((res) => res.text())
      .then((res) => console.log(res));
  };

  export const sendVkReferer = (vkParams) => {
    fetch("https://quarantinegame.azurewebsites.net/api/score", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        Line: vkParams.line,
        Key: vkParams.key,
        Score: 0,
        FirstName: vkParams.firstName,
        LastName: vkParams.lastName,
        Id : vkParams.id,
        Platform: "VK",
        Referer: vkParams.referer
      }),
    })
      .then((res) => res.text())
      .then((res) => console.log(res));
  };