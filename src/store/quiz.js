import { writable, derived, get } from "svelte/store";
import quizDict from "./quizDict";
import { QUESTION_COUNT, QUIZ_TIME, GAME_VIEWS } from "../store/constants";
import { gameView } from "../store";
import { sendMetrik } from "../helpers";
import { playTick, stopTick, playRightQuiz, playWrongQuiz } from "./audio";
import { lang } from "../helpers/i18";

export const activeQuestionIndex = writable(0);
export const selectedAnswer = writable(null);

export const quizTimer = (() => {
  const { set, subscribe, update } = writable(QUIZ_TIME);
  let timeoutId = null;
  let startTime = 0;
  return {
    subscribe,
    start: () => {
      playTick();
      const updateTimer = () => {
        update(() => {
          const newTime = QUIZ_TIME - (Date.now() - startTime);
          if (newTime <= 0) {
            stopTick();
            sendMetrik("QuizFail", { reason: "timout" });
            gameView.set(GAME_VIEWS.LOSE);
            return 0;
          }
          timeoutId = requestAnimationFrame(updateTimer);
          return newTime;
        });
      };
      startTime = Date.now();
      timeoutId = requestAnimationFrame(updateTimer);
    },
    stop:()=>{
      cancelAnimationFrame(timeoutId);
    },
    reset: () => {
      stopTick();
      cancelAnimationFrame(timeoutId);
      set(QUIZ_TIME);
    },
  };
})();

export const quizQuestions = (() => {
  const { subscribe, set } = writable([]);
  return {
    subscribe,
    init: () =>
      set(
        quizDict[lang]
          .sort(() => 0.5 - Math.random())
          .slice(0, QUESTION_COUNT)
          .map((item) => ({
            ...item,
            answers: item.answers.sort(() => 0.5 - Math.random()),
          }))
      ),
  };
})();
quizQuestions.init();

export const activeQuestion = derived(
  [activeQuestionIndex, quizQuestions],
  ([$activeQuestionIndex, $quizQuestions]) =>
    $quizQuestions[$activeQuestionIndex]
);

export const isAnswerCorrect = derived(
  [activeQuestion, selectedAnswer],
  ([$activeQuestion, $selectedAnswer]) => {
    if ($selectedAnswer === null) return null;
    return $activeQuestion.answer === $selectedAnswer;
  }
);
isAnswerCorrect.subscribe((state) => {
  if (state === false) {
    quizTimer.reset();
    playWrongQuiz();
    sendMetrik("QuizFail", {
      reason: "wrongAnswer",
      question: get(activeQuestion),
      userAnwser: get(selectedAnswer),
    });
  } else if (state === true) {
    playRightQuiz();
    if (get(activeQuestionIndex) === QUESTION_COUNT - 1) {
      quizTimer.stop();
    }
  }
});

export const nextQuestion = () => {
  if (!get(isAnswerCorrect)) {
    gameView.set(GAME_VIEWS.LOSE);
    return;
  }
  selectedAnswer.set(null);
  if (get(activeQuestionIndex) === QUESTION_COUNT - 1) {
    sendMetrik("QuizWin");
    gameView.set(GAME_VIEWS.WIN);
  } else {
    activeQuestionIndex.update((state) => state + 1);
  }
};

const startQuiz = () => {
  activeQuestionIndex.set(0);
  quizTimer.reset();
  quizTimer.start();
  selectedAnswer.set(null);
  quizQuestions.init();
};

gameView.subscribe((state) => {
  if (state === GAME_VIEWS.QUIZ) {
    startQuiz();
  }
  if (state === GAME_VIEWS.LOSE || state === GAME_VIEWS.WIN) {
    quizTimer.reset();
  }
});
