import { lang } from "../helpers/i18";
export const GAME_VIEWS = {
  INTRO: "INTRO",
  GAME: "GAME",
  AGE: "AGE",
  LOSE: "LOSE",
  WIN: "WIN",
  QUIZ: "QUIZ",
  QUIZ_INTRO: "QUIZ_INTRO",
};
export const IS_TEST = /(localhost|192\.168|az67128.gitlab.io)/.test(
  window.location.href
);

export const IS_WIDGET = !/(localhost|192\.168|screenlife.tech|az67128.gitlab.io)/.test(
  window.location.href
);
export const IS_VK = /vk_app_id/.test(window.location.href);
export const IS_MOBILE_VK = /vk_platform\=mobile/.test(window.location.href);

export const BASE_URL = !IS_WIDGET
  ? "."
  : // : "https://az67128.gitlab.io/svelte-covid";
    "https://screenlife.tech/quarantine";

export const VIRUS_RADIUS = 160;
export const VIRUS_TOUCH_RADIUS = 130;
export const INITIAL_VIRUS_VELOCITY = 17;
export const VIRUS_VELOCITY_CHANGE = 1.2; //1.5
export const VIRUS_VELOCITY_REPLAY_VALUE = 0.8;
export const RELEASE_VIRUS_VELOCITY_CHANGE = 7;
export const INITIAL_IMMUNITY = 20;
export const TICK_TIMEOUT = 100;
export const ADD_IMMUNITY_VALUE = 0.1; //0.1
export const SUB_IMMUNITY_VALUE = -0.1;
export const LOST_SUB_IMMUNITY_VALUE = -5;
export const VIRUS_PROTECTED_TIME = 2000;

export const QUESTION_COUNT = 5; // 5
export const QUIZ_TIME = 20000; //miliseconds 20000

export const SAMPLES = {
  SMS: [{ file: "SMS", }],
  SOUNDTRACK: [{ file: "elki", loop: true, value: 0.2 }],
  EXPLSOION: ["explosion"],
  PIG: [{ file: "pig", loop: 1, value: 0.1 }],
  PRESSING: [{ file: "pressing", value: 0.1 }],
  VIRUS_RUNAWAY: [{ file: "runaway", value: 0.1 }],
  SCREAM: [{ file: "scream", value: 0.1 }],
  TICK: [{ file: "tick", loop: true, value: 0.5 }],
  BUBLE: ["bubble"],
  GAME_START: lang === "ru" ? ["A-1-RU"] : [{ file: "A-1-EN", value: 2 }],
  INFO:
    lang === "ru"
      ? [{ file: "B-1-RU", value: 1 }]
      : [{ file: "B-1-EN", value: 2 }],
  GAME_RULES:
    lang === "ru"
      ? [{ file: "C-1-RU", value: 1 }]
      : [{ file: "C-1-EN", value: 2 }],
  CATCH:
    lang === "ru"
      ? [
          { file: "D-1-RU", value: 0.8 },
          { file: "D-2-RU", value: 0.8 },
          { file: "D-3-RU", value: 0.8 },
          { file: "D-4-RU", value: 0.8 },
          { file: "D-5-RU", value: 0.8 },
          { file: "D-6-RU", value: 0.8 },
          { file: "D-7-RU", value: 0.8 },
        ]
      : [
          { file: "D-1-EN", value: 2 },
          { file: "D-2-EN", value: 2 },
          { file: "D-3-EN", value: 2 },
          { file: "D-4-EN", value: 2 },
          { file: "D-5-EN", value: 2 },
          { file: "D-6-EN", value: 2 },
          { file: "D-7-EN", value: 2 },
          { file: "D-8-EN", value: 2 },
        ],
  RUNAWAY:
    lang === "ru"
      ? [
          { file: "E-1-RU", value: 1 },
          { file: "E-2-RU", value: 1 },
          { file: "E-3-RU", value: 1 },
          { file: "E-4-RU", value: 1 },
          { file: "E-5-RU", value: 1 },
          { file: "E-6-RU", value: 1 },
        ]
      : [
          { file: "E-1-EN", value: 2 },
          { file: "E-2-EN", value: 2 },
          { file: "E-3-EN", value: 2 },
          { file: "E-4-EN", value: 2 },
          { file: "E-5-EN", value: 2 },
        ],
  LISTEN_TO_ME:
    lang === "ru"
      ? [
          { file: "F-1-RU", value: 1.5 },
          { file: "F-2-RU", value: 1.5 },
          { file: "F-3-RU", value: 1.5 },
          { file: "F-4-RU", value: 1.5 },
          { file: "F-5-RU", value: 1.5 },
        ]
      : ["F-1-EN", "F-2-EN", "F-3-EN", "F-4-EN"],
  RULES:
    lang === "ru"
      ? [
          { file: "G-1-RU-F", value: 1.7 },
          { file: "G-2-RU-F", value: 1.7 },
          { file: "G-3-RU-F", value: 1.7 },
          { file: "G-4-RU-F", value: 1.7 },
          { file: "G-5-RU-F", value: 1.7 },
          { file: "G-6-RU-F", value: 1.7 },
          { file: "G-7-RU-F", value: 1.7 },
          { file: "G-8-RU-F", value: 1.7 },
          { file: "G-9-RU-F", value: 1.7 },
          { file: "G-10-RU-F", value: 1.7 },
          { file: "G-11-RU-F", value: 1.7 },
          { file: "G-12-RU-F", value: 1.7 },
          { file: "G-13-RU-F", value: 1.7 },
          { file: "G-14-RU-F", value: 1.7 },
        ]
      : [
          { file: "G-1-EN", value: 2 },
          { file: "G-2-EN", value: 2 },
          { file: "G-3-EN", value: 2 },
          { file: "G-4-EN", value: 2 },
          { file: "G-5-EN", value: 2 },
          { file: "G-6-EN", value: 2 },
          { file: "G-7-EN", value: 2 },
          { file: "G-8-EN", value: 2 },
          { file: "G-9-EN", value: 2 },
          { file: "G-10-EN", value: 2 },
          { file: "G-11-EN", value: 2 },
          { file: "G-12-EN", value: 2 },
          { file: "G-13-EN", value: 2 },
          { file: "G-14-EN", value: 2 },
          { file: "G-15-EN", value: 2 },
          { file: "G-16-EN", value: 2 },
          { file: "G-17-EN", value: 2 },
          { file: "G-18-EN", value: 2 },
          { file: "G-19-EN", value: 2 },
        ],
  QUIZ_INTRO:
    lang === "ru"
      ? [
          { file: "H-1-RU", value: 0.8 },
          { file: "H-2-RU", value: 0.8 },
          { file: "H-3-RU", value: 0.8 },
        ]
      : [
          { file: "H-1-EN", value: 2 },
          { file: "H-2-EN", value: 2 },
          { file: "H-3-EN", value: 2 },
        ],
  GAME_LOSE:
    lang === "ru"
      ? [
          { file: "J-1-RU", value: 1.5 },
          { file: "J-2-RU", value: 1.5 },
          { file: "J-3-RU", value: 1.5 },
          { file: "J-4-RU", value: 1.5 },
        ]
      : [
          { file: "J-1-EN", value: 2 },
          { file: "J-2-EN", value: 2 },
          { file: "J-3-EN", value: 2 },
        ],
  RIGHT_ANSWER:
    lang === "ru"
      ? ["K-1-RU", "K-2-RU", "K-3-RU"]
      : [
          { file: "K-1-EN", value: 2 },
          { file: "K-2-EN", value: 2 },
        ],
  WRONG_ANWSER:
    lang === "ru"
      ? ["L-1-RU", "L-2-RU", "L-3-RU"]
      : [
          { file: "L-1-EN", value: 2 },
          { file: "L-2-EN", value: 2 },
          { file: "L-3-EN", value: 2 },
        ],
  GAME_WIN:
    lang === "ru"
      ? ["M-1-RU", "M-2-RU"]
      : [
          { file: "M-1-EN", value: 2 },
          { file: "M-2-EN", value: 2 },
        ],
};


