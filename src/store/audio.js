import BufferLoader from "../helpers/bufferLoader";
import { BASE_URL, SAMPLES, IS_WIDGET } from "../store/constants";
import { preloadIndicator } from "../store";

window.AudioContext = window.AudioContext || window.webkitAudioContext;
const audioContext = new AudioContext();
const audioElements = {};
const audioBuffers = {};
const samples = {};
window.audioBuffers = audioBuffers;
window.audioElements = audioElements;
export const initAudio = () => {
  const loadAudio = (item, key) => new Promise(resolve =>{
    const file = typeof item === "string" ? item : item.file;
    const url = IS_WIDGET ? `${BASE_URL}/assets/?file=audio/${file}.mp3` : `${BASE_URL}/assets/audio/${file}.mp3`
    
    const bufferLoader = new BufferLoader(audioContext, [url], (bufferList) => {
      if (!audioBuffers[key]) audioBuffers[key] = [];
      const buffer = {
        buffer: bufferList[0],
        loop: Boolean(item.loop),
        value: item.value || 1
      }
      audioBuffers[key].push(buffer);
      preloadIndicator.assetLoaded();
      resolve();
    });
    bufferLoader.load();
  })

  const promiseArray = []
  Object.keys(SAMPLES).forEach((key) => {
    SAMPLES[key].forEach(item=>{
      promiseArray.push(loadAudio(item, key));
    })
  });
  Promise.all(promiseArray).then(()=>{
    Object.keys(audioBuffers).forEach(key=>{
      audioBuffers[key] = audioBuffers[key].sort(() => 0.5 - Math.random())
    })
  })
};
initAudio();

export const makeAudioLooped = (type) => {
  audioElements[type].loop = true;
};

export const stopAudio = (type) => {
  if (audioElements[type]) {
    audioElements[type].source.onended = () => {};
    audioElements[type].source.stop();
    audioElements[type].gainNode.disconnect(audioContext.destination);
    delete audioElements[type];
  }
};

export const playAudio = async (type, index, callback) => {
  stopAudio(type);

  let gainNode = audioContext.createGain();
  let source = audioContext.createBufferSource();
  source = audioContext.createBufferSource();
  source.buffer = audioBuffers[type][index].buffer;
  source.loop = audioBuffers[type][index].loop;
  if (callback) source.onended = callback;
  source.connect(gainNode);
  gainNode.connect(audioContext.destination);
  source.start(0);
  gainNode.gain.value = audioBuffers[type][index].value;
  audioElements[type] = { gainNode, source };
};

export const stopAllAudio = (keepSoundtrack = false) => {
  Object.keys(SAMPLES).forEach((key) => {
    if (keepSoundtrack && key === "SOUNDTRACK") return;
    stopAudio(key);
  });
};

export const playSample = async (type, callback) => {
  if (!audioBuffers[type]) return;
  if (document.visibilityState === "hidden") return;
  stopSample(type);
  if (samples[type] === undefined) samples[type] = -1;

  samples[type] += 1;
  if (samples[type] === audioBuffers[type].length) samples[type] = 0;
  await playAudio(type, samples[type], callback);
};

export const stopSample = (type) => {
  stopAudio(type);
};

export const playCatch = async () => {
  const rulesCallback = () => playSample("RULES", rulesCallback);
  if (audioElements.SOUNDTRACK)
    audioElements.SOUNDTRACK.gainNode.gain.value = SAMPLES.SOUNDTRACK[0].value;
  stopSample("GAME_RULES");
  stopSample("GAME_START");
  stopSample("GAME_LOSE");
  playSample("SCREAM");
  playSample("PIG");
  playSample("CATCH", () => {
    playSample("LISTEN_TO_ME", rulesCallback);
  });
};

export const playRelease = async () => {
  if (audioElements.SOUNDTRACK)
    audioElements.SOUNDTRACK.gainNode.gain.value = 1;
  playSample("VIRUS_RUNAWAY");
  playSample("RUNAWAY");

  stopSample("SCREAM");
  stopSample("PIG");
  stopSample("CATCH");
  stopSample("LISTEN_TO_ME");
  stopSample("RULES");
};

export const playBoom = () => {
  stopSample("SCREAM");
  stopSample("PIG");
  playSample("EXPLSOION");
};

export const playTick = () => {
  playSample("TICK");
};

export const stopTick = () => {
  stopSample("TICK");
};

export const playGameStart = () => {
  stopSample("GAME_START");
  stopSample("GAME_LOSE");
  playSample("GAME_RULES");
};

export const playLose = () => {
  stopSample("WRONG_ANWSER");
  stopSample("RUNAWAY");
  playSample("GAME_LOSE");
};

export const playWin = () => {
  stopSample("RIGHT_ANSWER");
  playSample("GAME_WIN");
};
export const playRightQuiz = () => {
  stopSample("QUIZ_INTRO");
  playSample("RIGHT_ANSWER");
};
export const playWrongQuiz = () => {
  stopSample("QUIZ_INTRO");
  playSample("WRONG_ANWSER");
};
export const playQuizIntro = () => {
  stopSample("CATCH");
  stopSample("LISTEN_TO_ME");
  stopSample("RULES");
  playSample("QUIZ_INTRO");
};
