import bridge from "@vkontakte/vk-bridge";
import { age, vkParams, close } from "./index";
import { sendMetrik } from "../helpers";
import {sendVkScore} from './score';

const signVkParams = () => {
  const searchParams = new URLSearchParams(window.location.search);
  vkParams.key = searchParams.get("sign");
  vkParams.id = searchParams.get("vk_user_id");
  vkParams.appId = searchParams.get("vk_app_id");
  vkParams.referer = window.location.hash.replace('#','');
  let vkQueryarams = {};
  for (let [key, value] of searchParams) {
    if (key.startsWith("vk_")) vkQueryarams[key] = value;
  }
  const line = Object.keys(vkQueryarams)
    .sort()
    .map((key) => `${key}=${vkQueryarams[key]}`)
    .join("&");
  
  vkParams.line = line;
};
export const initVk = () => {
  bridge.subscribe((event) => {
    if (event.detail && event.detail.type === "VKWebAppViewHide") close();
  });
  bridge
    .send("VKWebAppInit", {})
    .then(() => {
      return bridge.send("VKWebAppGetUserInfo", {});
    })
    .then((res) => {
      vkParams.firstName = res.first_name;
      vkParams.lastName = res.last_name;
      sendVkScore(0, vkParams)
      sendVkReferer(vkParams);
      if (!res.bdate || res.bdate.length < 10) return;
      const birthday = new Date(res.bdate.split(".").reverse().join("-"));
      const now = new Date();
      const userAge = now.getFullYear() - birthday.getFullYear();
      age.set(userAge);
      sendMetrik("AgeSubmit", { age: userAge });
    }).catch(err=>{
      console.log('VKWebAppInit', err);
    });
  signVkParams();
};

