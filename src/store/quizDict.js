export default {
  ru: [
    {
      question: "Что является одним из основных симптомов?",
      answers: ["Одышка", "Головная боль"],
      answer: "Одышка",
    },
    {
      question: "Что является одним из основных симптомов?",
      answers: ["Высокая температура", "Заложенность грудной клетки"],
      answer: "Высокая температура",
    },
    {
      question: "Что является одним из основных симптомов?",
      answers: ["Утомляемость", "Кровохарканье"],
      answer: "Утомляемость",
    },
    {
      question: "Что является одним из основных симптомов?",
      answers: ["Утомляемость", "Диарея"],
      answer: "Утомляемость",
    },
    {
      question: "Что является одним из основных симптомов?",
      answers: ["Кашель", "Тошнота"],
      answer: "Кашель",
    },
    {
      question: "Что является одним из основных симптомов?",
      answers: ["Боль в мышцах", "Головная боль"],
      answer: "Боль в мышцах",
    },
    {
      question: "Что делать, если обнаружил симптомы",
      answers: ["Вызвать скорую", "Пойти в поликлиннику"],
      answer: "Вызвать скорую",
    },
    {
      question: "Нужно ли проветривать помещение",
      answers: ["Нужно часто", "Нет, полная изоляция"],
      answer: "Нужно часто",
    },
    {
      question: "Кто может заразиться?",
      answers: ["Люди всех возрастов", "Люди старше 65"],
      answer: "Люди всех возрастов",
    },
    {
      question: "Кто в группе риска?",
      answers: ["Люди старше 65", "Дети до 18"],
      answer: "Люди старше 65",
    },
    {
      question: "Инкубационный период у гриппа",
      answers: ["3 дня", "5-6 дней"],
      answer: "3 дня",
    },
    {
      question: "Инкубационный период у COVID-19",
      answers: ["До 14 дней", "3 дня"],
      answer: "До 14 дней",
    },
    {
      question: "Срок изоляции при карантине",
      answers: ["15 дней", "3 дня"],
      answer: "15 дней",
    },
    {
      question: "Стоимость оказания помощи с COVID-19",
      answers: ["Бесплатно", "1 000 000"],
      answer: "Бесплатно",
    },
    {
      question: "Могут ли родственники посещать больного",
      answers: ["Нет", "Да"],
      answer: "Нет",
    },
    {
      question: "Можно ли заразиться через мобильный телефон",
      answers: ["Да", "Нет"],
      answer: "Да",
    },
    {
      question: "Передаётся ли COVID-19 воздушно-капельным путём?",
      answers: ["Да", "Нет"],
      answer: "Да",
    },
    {
      question: "Передаётся ли COVID-19 контактным путём",
      answers: ["Да", "Нет"],
      answer: "Да",
    },
    {
      question: "Можно ли заразиться через поручни в транспорте?",
      answers: ["Да", "Нет"],
      answer: "Да",
    },
    {
      question: "Для чего можно выходить из дома?",
      answers: ["В экстренных случаях", "Для занятий спортом"],
      answer: "В экстренных случаях",
    },
    {
      question: "Почему нельзя касаться рта, носа, глаз?",
      answers: ["Можно заразиться", "Вредная привычка"],
      answer: "Можно заразиться",
    },
    {
      question: "Какую дистанцию нужно соблюдать при общении?",
      answers: ["1.5 метра", "0.5 метра"],
      answer: "1.5 метра",
    },
    {
      question: "Как лечиться?",
      answers: ["Обратиться к врачу", "Самолечение"],
      answer: "Обратиться к врачу",
    },
    {
      question: "Можно ли пользоваться личным транспортом?",
      answers: ["В случае необходимости", "Поехать на экскурсию"],
      answer: "В случае необходимости",
    },
  ],
  en: [
    {
      question: "What is one of the main symptoms?",
      answers: ["Shortness of breath", "Headache"],
      answer: "Shortness of breath",
    },
    {
      question: "What is one of the main symptoms?",
      answers: ["High temperature", "Chest tightness"],
      answer: "High temperature",
    },
    {
      question: "What is one of the main symptoms?",
      answers: ["Fatigue", "Coughing up blood"],
      answer: "Fatigue",
    },
    {
      question: "What is one of the main symptoms?",
      answers: ["Fatigue", "Diarrhea"],
      answer: "Fatigue",
    },
    {
      question: "What is one of the main symptoms?",
      answers: ["Coughing", "Nausea"],
      answer: "Coughing",
    },
    {
      question: "What is one of the main symptoms?",
      answers: ["Muscular pain", "Headache"],
      answer: "Muscular pain",
    },
    {
      question: "What should you do if you have symptoms?",
      answers: ["Call an ambulance", "Go to hospital"],
      answer: "Call an ambulance",
    },
    {
      question: "Should you ventilate the room?",
      answers: ["Yes, often", "No, complete isolation"],
      answer: "Yes, often",
    },
    {
      question: "Who can become infected?",
      answers: ["People of all ages", "People over 65"],
      answer: "People of all ages",
    },
    {
      question: "Who is in the risk group?",
      answers: ["People over 65", "Children under 18"],
      answer: "People over 65",
    },
    {
      question: "Incubation period for flu",
      answers: ["3 days", "5-6 days"],
      answer: "3 days",
    },
    {
      question: "Incubation period for COVID-19",
      answers: ["Up to 14 days", "3 days"],
      answer: "Up to 14 days",
    },
    {
      question: "Time spent in isolation when under quarantine",
      answers: ["15 days", "3 days"],
      answer: "15 days",
    },
    {
      question: "Cost of care for COVID-19",
      answers: ["Free of charge", "1,000,000"],
      answer: "Free of charge",
    },
    {
      question: "Can relatives visit a patient?",
      answers: ["No", "Yes"],
      answer: "No",
    },
    {
      question: "Can you get infected from your mobile phone?",
      answers: ["Yes", "No"],
      answer: "Yes",
    },
    {
      question: "Is COVID-19 an airborne infection?",
      answers: ["Yes", "No"],
      answer: "Yes",
    },
    {
      question: "Is COVID-19 spread through close contact?",
      answers: ["Yes", "No"],
      answer: "Yes",
    },
    {
      question: "Can you get infected from handrails on public transport?",
      answers: ["Yes", "No"],
      answer: "Yes",
    },
    {
      question: "Under what circumstances can you go outside?",
      answers: ["In an emergency", "To do sport"],
      answer: "In an emergency",
    },
    {
      question: "Why must you not touch your mouth, nose, or eyes?",
      answers: ["You can get infected", "It's a bad habit"],
      answer: "You can get infected",
    },
    {
      question: "What social distance should you maintain?",
      answers: ["1.5 metres", "0.5 metres"],
      answer: "1.5 metres",
    },
    {
      question: "What should you do for treatment? ",
      answers: ["See a doctor", "Self-treatment"],
      answer: "See a doctor",
    },
    {
      question: "Can you use your own vehicle?",
      answers: ["If necessary", "To go on a trip"],
      answer: "If necessary",
    },
  ],
};
