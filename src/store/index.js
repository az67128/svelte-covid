import { writable, get } from "svelte/store";
import {
  playCatch,
  playRelease,
  playBoom,
  stopAllAudio,
  playGameStart,
  playSample,
  playLose,
  playWin,
  playQuizIntro,
} from "./audio";
import { tweened } from "svelte/motion";
import { cubicOut } from "svelte/easing";
import { sendVkScore } from "./score";

import {
  GAME_VIEWS,
  INITIAL_IMMUNITY,
  TICK_TIMEOUT,
  ADD_IMMUNITY_VALUE,
  SUB_IMMUNITY_VALUE,
  LOST_SUB_IMMUNITY_VALUE,
  SAMPLES,
  IS_VK,
} from "./constants";
import { sendMetrik } from "../helpers";

let isPointerDown = false;
let gameStartTime = null;
let gameTime = null;

export const vkParams = {};

export const showIntro = writable(true);

export const gameWidth = writable(0);
export const gameHeight = writable(0);

export const gameState = writable(null);
export const gameView = writable(null);
// export const gameState = writable("loaded");
// export const gameView = writable(GAME_VIEWS.LOSE);

export const pointer = { x: 0, y: 0 };

export const age = writable("");

export const replayCount = writable(0);
export const startPointer = (e) => {
  showIntro.set(false);
  isPointerDown = true;
  const { clientX: x, clientY: y } = e.touches ? e.touches[0] : e;
  pointer.x = x;
  pointer.y = y;
};
export const setPointer = (e) => {
  if (!isPointerDown) return;
  const { clientX: x, clientY: y } = e.touches ? e.touches[0] : e;
  pointer.x = x;
  pointer.y = y;
};
export const resetPointer = () => {
  isPointerDown = false;
  pointer.x = 0;
  pointer.y = 0;
};

export const isHolding = (() => {
  const store = writable(false);
  return {
    subscribe: store.subscribe,
    update: (f) => {
      store.update((state) => {
        const newValue = f(state);

        if (!get(gameState) || get(gameView) !== GAME_VIEWS.GAME)
          return newValue;
        if (state === false && newValue === true) {
          playCatch();
        }
        if (state === true && newValue === false) {
          playRelease();
        }
        return newValue;
      });
    },
  };
})();
export const immunity = writable(INITIAL_IMMUNITY);

let immunityTimeoutId = null;
let prevIsHoldingVirusState = false;
const resetImmunityTimeout = () => clearTimeout(immunityTimeoutId);
const updateImmunity = () => {
  const isHoldingVirus = get(isHolding);
  let immunityValue = get(immunity);
  const isLostVirus = prevIsHoldingVirusState && !isHoldingVirus;
  immunityValue += isHoldingVirus
    ? ADD_IMMUNITY_VALUE
    : isLostVirus
    ? LOST_SUB_IMMUNITY_VALUE
    : SUB_IMMUNITY_VALUE;

  // immunity.set(immunityValue, { duration: isLostVirus ? 1000 : 1 });
  immunity.update(() => immunityValue);
  if (immunityValue <= 0) {
    resetPointer();
    sendMetrik("ArcadeFail", {
      gameTimeLose: Math.round((Date.now() - gameStartTime) / 100) / 10,
    });
    gameView.set(GAME_VIEWS.LOSE);
  } else if (immunityValue >= 100) {
    playBoom();
    resetPointer();

    gameView.set(GAME_VIEWS.QUIZ_INTRO);
    isHolding.update(() => false);
    playQuizIntro();
  } else {
    immunityTimeoutId = setTimeout(
      updateImmunity,
      TICK_TIMEOUT + isLostVirus ? 100 : 0
    );
  }
  prevIsHoldingVirusState = isHoldingVirus;
};

const startImmunityCount = () => {
  immunity.set(INITIAL_IMMUNITY);
  immunityTimeoutId = setTimeout(updateImmunity, TICK_TIMEOUT);
};

gameView.subscribe((state) => {
  if (state === GAME_VIEWS.GAME) {
    gameStartTime = Date.now();
    playSample("SOUNDTRACK");
    playGameStart();
    prevIsHoldingVirusState = false;
    isHolding.update(() => false);
    startImmunityCount();
  } else {
    resetImmunityTimeout();
  }
  if (state === GAME_VIEWS.LOSE) {
    playLose();
  }
  if (state === GAME_VIEWS.WIN) {
    if (IS_VK) sendVkScore(gameTime, vkParams);
    playWin();
  }
  if (state === GAME_VIEWS.QUIZ_INTRO) {
    gameTime = Math.round((Date.now() - gameStartTime) / 100) / 10;
    sendMetrik("ArcadeWin", {
      gameTime: gameTime,
    });
  }
});

export const preloadIndicator = (() => {
  const startTime = Date.now();
  const assetsCount =
    Object.keys(SAMPLES).reduce((memo, key) => memo + SAMPLES[key].length, 0) +
    3; //3 - count of sprites
  let loadedAssestsCount = 0;
  const { subscribe, set } = tweened(0, {
    duration: 500,
    easing: cubicOut,
  });
  return {
    subscribe,
    assetLoaded: () => {
      loadedAssestsCount += 1;
      if (loadedAssestsCount === assetsCount && get(gameState) === null) {
        sendMetrik("ResoursesLoad", {
          time: Math.round((Date.now() - startTime) / 100) / 10,
        });
        // playSample("GAME_START");
      }
      set(loadedAssestsCount / assetsCount);
    },
    assetNotLoaded: (asset, err) => {
      sendMetrik("ResoursesLoad", {
        failedAsset: asset,
        err,
      });
    },
  };
})();

export const startWidget = async () => {
  gameView.set(GAME_VIEWS.GAME);
  gameState.set("loaded");
  sendMetrik("Start");
};

export const submitAge = (event) => {
  event.preventDefault();
  if (!get(age)) return;

  sendMetrik("AgeSubmit", { age: get(age) });
  gameView.set(GAME_VIEWS.GAME);
};

export const proceedToQuiz = () => {
  gameView.set(GAME_VIEWS.QUIZ);
  sendMetrik("GameContinue");
};

export const replay = () => {
  replayCount.update((count) => count + 1);
  gameView.set(GAME_VIEWS.GAME);
  sendMetrik("GameReplay", { replayCount: get(replayCount) });
};

export const close = () => {
  resetPointer();
  resetImmunityTimeout();
  sendMetrik("GameClose", { view: get(gameView) });
  gameView.set(null);
  gameState.set(null);
  stopAllAudio();
};

export const onVisibilityChange = () => {
  if (document.visibilityState === "hidden") {
    stopAllAudio();
  }
  if (
    get(gameView) === GAME_VIEWS.GAME &&
    get(gameState) &&
    document.visibilityState === "visible"
  ) {
    playSample("SOUNDTRACK");
  }
};
