
(function(l, r) { if (l.getElementById('livereloadscript')) return; r = l.createElement('script'); r.async = 1; r.src = '//' + (window.location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1'; r.id = 'livereloadscript'; l.head.appendChild(r) })(window.document);
var app = (function () {
    'use strict';

    function noop() { }
    const identity = x => x;
    function assign(tar, src) {
        // @ts-ignore
        for (const k in src)
            tar[k] = src[k];
        return tar;
    }
    function add_location(element, file, line, column, char) {
        element.__svelte_meta = {
            loc: { file, line, column, char }
        };
    }
    function run(fn) {
        return fn();
    }
    function blank_object() {
        return Object.create(null);
    }
    function run_all(fns) {
        fns.forEach(run);
    }
    function is_function(thing) {
        return typeof thing === 'function';
    }
    function safe_not_equal(a, b) {
        return a != a ? b == b : a !== b || ((a && typeof a === 'object') || typeof a === 'function');
    }
    function validate_store(store, name) {
        if (store != null && typeof store.subscribe !== 'function') {
            throw new Error(`'${name}' is not a store with a 'subscribe' method`);
        }
    }
    function subscribe(store, ...callbacks) {
        if (store == null) {
            return noop;
        }
        const unsub = store.subscribe(...callbacks);
        return unsub.unsubscribe ? () => unsub.unsubscribe() : unsub;
    }
    function get_store_value(store) {
        let value;
        subscribe(store, _ => value = _)();
        return value;
    }
    function component_subscribe(component, store, callback) {
        component.$$.on_destroy.push(subscribe(store, callback));
    }
    function create_slot(definition, ctx, $$scope, fn) {
        if (definition) {
            const slot_ctx = get_slot_context(definition, ctx, $$scope, fn);
            return definition[0](slot_ctx);
        }
    }
    function get_slot_context(definition, ctx, $$scope, fn) {
        return definition[1] && fn
            ? assign($$scope.ctx.slice(), definition[1](fn(ctx)))
            : $$scope.ctx;
    }
    function get_slot_changes(definition, $$scope, dirty, fn) {
        if (definition[2] && fn) {
            const lets = definition[2](fn(dirty));
            if ($$scope.dirty === undefined) {
                return lets;
            }
            if (typeof lets === 'object') {
                const merged = [];
                const len = Math.max($$scope.dirty.length, lets.length);
                for (let i = 0; i < len; i += 1) {
                    merged[i] = $$scope.dirty[i] | lets[i];
                }
                return merged;
            }
            return $$scope.dirty | lets;
        }
        return $$scope.dirty;
    }

    const is_client = typeof window !== 'undefined';
    let now = is_client
        ? () => window.performance.now()
        : () => Date.now();
    let raf = is_client ? cb => requestAnimationFrame(cb) : noop;

    const tasks = new Set();
    function run_tasks(now) {
        tasks.forEach(task => {
            if (!task.c(now)) {
                tasks.delete(task);
                task.f();
            }
        });
        if (tasks.size !== 0)
            raf(run_tasks);
    }
    /**
     * Creates a new task that runs on each raf frame
     * until it returns a falsy value or is aborted
     */
    function loop(callback) {
        let task;
        if (tasks.size === 0)
            raf(run_tasks);
        return {
            promise: new Promise(fulfill => {
                tasks.add(task = { c: callback, f: fulfill });
            }),
            abort() {
                tasks.delete(task);
            }
        };
    }

    function append(target, node) {
        target.appendChild(node);
    }
    function insert(target, node, anchor) {
        target.insertBefore(node, anchor || null);
    }
    function detach(node) {
        node.parentNode.removeChild(node);
    }
    function destroy_each(iterations, detaching) {
        for (let i = 0; i < iterations.length; i += 1) {
            if (iterations[i])
                iterations[i].d(detaching);
        }
    }
    function element(name) {
        return document.createElement(name);
    }
    function svg_element(name) {
        return document.createElementNS('http://www.w3.org/2000/svg', name);
    }
    function text(data) {
        return document.createTextNode(data);
    }
    function space() {
        return text(' ');
    }
    function empty() {
        return text('');
    }
    function listen(node, event, handler, options) {
        node.addEventListener(event, handler, options);
        return () => node.removeEventListener(event, handler, options);
    }
    function attr(node, attribute, value) {
        if (value == null)
            node.removeAttribute(attribute);
        else if (node.getAttribute(attribute) !== value)
            node.setAttribute(attribute, value);
    }
    function to_number(value) {
        return value === '' ? undefined : +value;
    }
    function children(element) {
        return Array.from(element.childNodes);
    }
    function set_input_value(input, value) {
        if (value != null || input.value) {
            input.value = value;
        }
    }
    function set_style(node, key, value, important) {
        node.style.setProperty(key, value, important ? 'important' : '');
    }
    function toggle_class(element, name, toggle) {
        element.classList[toggle ? 'add' : 'remove'](name);
    }
    function custom_event(type, detail) {
        const e = document.createEvent('CustomEvent');
        e.initCustomEvent(type, false, false, detail);
        return e;
    }

    const active_docs = new Set();
    let active = 0;
    // https://github.com/darkskyapp/string-hash/blob/master/index.js
    function hash(str) {
        let hash = 5381;
        let i = str.length;
        while (i--)
            hash = ((hash << 5) - hash) ^ str.charCodeAt(i);
        return hash >>> 0;
    }
    function create_rule(node, a, b, duration, delay, ease, fn, uid = 0) {
        const step = 16.666 / duration;
        let keyframes = '{\n';
        for (let p = 0; p <= 1; p += step) {
            const t = a + (b - a) * ease(p);
            keyframes += p * 100 + `%{${fn(t, 1 - t)}}\n`;
        }
        const rule = keyframes + `100% {${fn(b, 1 - b)}}\n}`;
        const name = `__svelte_${hash(rule)}_${uid}`;
        const doc = node.ownerDocument;
        active_docs.add(doc);
        const stylesheet = doc.__svelte_stylesheet || (doc.__svelte_stylesheet = doc.head.appendChild(element('style')).sheet);
        const current_rules = doc.__svelte_rules || (doc.__svelte_rules = {});
        if (!current_rules[name]) {
            current_rules[name] = true;
            stylesheet.insertRule(`@keyframes ${name} ${rule}`, stylesheet.cssRules.length);
        }
        const animation = node.style.animation || '';
        node.style.animation = `${animation ? `${animation}, ` : ``}${name} ${duration}ms linear ${delay}ms 1 both`;
        active += 1;
        return name;
    }
    function delete_rule(node, name) {
        const previous = (node.style.animation || '').split(', ');
        const next = previous.filter(name
            ? anim => anim.indexOf(name) < 0 // remove specific animation
            : anim => anim.indexOf('__svelte') === -1 // remove all Svelte animations
        );
        const deleted = previous.length - next.length;
        if (deleted) {
            node.style.animation = next.join(', ');
            active -= deleted;
            if (!active)
                clear_rules();
        }
    }
    function clear_rules() {
        raf(() => {
            if (active)
                return;
            active_docs.forEach(doc => {
                const stylesheet = doc.__svelte_stylesheet;
                let i = stylesheet.cssRules.length;
                while (i--)
                    stylesheet.deleteRule(i);
                doc.__svelte_rules = {};
            });
            active_docs.clear();
        });
    }

    let current_component;
    function set_current_component(component) {
        current_component = component;
    }
    function get_current_component() {
        if (!current_component)
            throw new Error(`Function called outside component initialization`);
        return current_component;
    }
    function onMount(fn) {
        get_current_component().$$.on_mount.push(fn);
    }
    function onDestroy(fn) {
        get_current_component().$$.on_destroy.push(fn);
    }
    // TODO figure out if we still want to support
    // shorthand events, or if we want to implement
    // a real bubbling mechanism
    function bubble(component, event) {
        const callbacks = component.$$.callbacks[event.type];
        if (callbacks) {
            callbacks.slice().forEach(fn => fn(event));
        }
    }

    const dirty_components = [];
    const binding_callbacks = [];
    const render_callbacks = [];
    const flush_callbacks = [];
    const resolved_promise = Promise.resolve();
    let update_scheduled = false;
    function schedule_update() {
        if (!update_scheduled) {
            update_scheduled = true;
            resolved_promise.then(flush);
        }
    }
    function add_render_callback(fn) {
        render_callbacks.push(fn);
    }
    let flushing = false;
    const seen_callbacks = new Set();
    function flush() {
        if (flushing)
            return;
        flushing = true;
        do {
            // first, call beforeUpdate functions
            // and update components
            for (let i = 0; i < dirty_components.length; i += 1) {
                const component = dirty_components[i];
                set_current_component(component);
                update(component.$$);
            }
            dirty_components.length = 0;
            while (binding_callbacks.length)
                binding_callbacks.pop()();
            // then, once components are updated, call
            // afterUpdate functions. This may cause
            // subsequent updates...
            for (let i = 0; i < render_callbacks.length; i += 1) {
                const callback = render_callbacks[i];
                if (!seen_callbacks.has(callback)) {
                    // ...so guard against infinite loops
                    seen_callbacks.add(callback);
                    callback();
                }
            }
            render_callbacks.length = 0;
        } while (dirty_components.length);
        while (flush_callbacks.length) {
            flush_callbacks.pop()();
        }
        update_scheduled = false;
        flushing = false;
        seen_callbacks.clear();
    }
    function update($$) {
        if ($$.fragment !== null) {
            $$.update();
            run_all($$.before_update);
            const dirty = $$.dirty;
            $$.dirty = [-1];
            $$.fragment && $$.fragment.p($$.ctx, dirty);
            $$.after_update.forEach(add_render_callback);
        }
    }

    let promise;
    function wait() {
        if (!promise) {
            promise = Promise.resolve();
            promise.then(() => {
                promise = null;
            });
        }
        return promise;
    }
    function dispatch(node, direction, kind) {
        node.dispatchEvent(custom_event(`${direction ? 'intro' : 'outro'}${kind}`));
    }
    const outroing = new Set();
    let outros;
    function group_outros() {
        outros = {
            r: 0,
            c: [],
            p: outros // parent group
        };
    }
    function check_outros() {
        if (!outros.r) {
            run_all(outros.c);
        }
        outros = outros.p;
    }
    function transition_in(block, local) {
        if (block && block.i) {
            outroing.delete(block);
            block.i(local);
        }
    }
    function transition_out(block, local, detach, callback) {
        if (block && block.o) {
            if (outroing.has(block))
                return;
            outroing.add(block);
            outros.c.push(() => {
                outroing.delete(block);
                if (callback) {
                    if (detach)
                        block.d(1);
                    callback();
                }
            });
            block.o(local);
        }
    }
    const null_transition = { duration: 0 };
    function create_in_transition(node, fn, params) {
        let config = fn(node, params);
        let running = false;
        let animation_name;
        let task;
        let uid = 0;
        function cleanup() {
            if (animation_name)
                delete_rule(node, animation_name);
        }
        function go() {
            const { delay = 0, duration = 300, easing = identity, tick = noop, css } = config || null_transition;
            if (css)
                animation_name = create_rule(node, 0, 1, duration, delay, easing, css, uid++);
            tick(0, 1);
            const start_time = now() + delay;
            const end_time = start_time + duration;
            if (task)
                task.abort();
            running = true;
            add_render_callback(() => dispatch(node, true, 'start'));
            task = loop(now => {
                if (running) {
                    if (now >= end_time) {
                        tick(1, 0);
                        dispatch(node, true, 'end');
                        cleanup();
                        return running = false;
                    }
                    if (now >= start_time) {
                        const t = easing((now - start_time) / duration);
                        tick(t, 1 - t);
                    }
                }
                return running;
            });
        }
        let started = false;
        return {
            start() {
                if (started)
                    return;
                delete_rule(node);
                if (is_function(config)) {
                    config = config();
                    wait().then(go);
                }
                else {
                    go();
                }
            },
            invalidate() {
                started = false;
            },
            end() {
                if (running) {
                    cleanup();
                    running = false;
                }
            }
        };
    }
    function create_out_transition(node, fn, params) {
        let config = fn(node, params);
        let running = true;
        let animation_name;
        const group = outros;
        group.r += 1;
        function go() {
            const { delay = 0, duration = 300, easing = identity, tick = noop, css } = config || null_transition;
            if (css)
                animation_name = create_rule(node, 1, 0, duration, delay, easing, css);
            const start_time = now() + delay;
            const end_time = start_time + duration;
            add_render_callback(() => dispatch(node, false, 'start'));
            loop(now => {
                if (running) {
                    if (now >= end_time) {
                        tick(0, 1);
                        dispatch(node, false, 'end');
                        if (!--group.r) {
                            // this will result in `end()` being called,
                            // so we don't need to clean up here
                            run_all(group.c);
                        }
                        return false;
                    }
                    if (now >= start_time) {
                        const t = easing((now - start_time) / duration);
                        tick(1 - t, t);
                    }
                }
                return running;
            });
        }
        if (is_function(config)) {
            wait().then(() => {
                // @ts-ignore
                config = config();
                go();
            });
        }
        else {
            go();
        }
        return {
            end(reset) {
                if (reset && config.tick) {
                    config.tick(1, 0);
                }
                if (running) {
                    if (animation_name)
                        delete_rule(node, animation_name);
                    running = false;
                }
            }
        };
    }
    function create_bidirectional_transition(node, fn, params, intro) {
        let config = fn(node, params);
        let t = intro ? 0 : 1;
        let running_program = null;
        let pending_program = null;
        let animation_name = null;
        function clear_animation() {
            if (animation_name)
                delete_rule(node, animation_name);
        }
        function init(program, duration) {
            const d = program.b - t;
            duration *= Math.abs(d);
            return {
                a: t,
                b: program.b,
                d,
                duration,
                start: program.start,
                end: program.start + duration,
                group: program.group
            };
        }
        function go(b) {
            const { delay = 0, duration = 300, easing = identity, tick = noop, css } = config || null_transition;
            const program = {
                start: now() + delay,
                b
            };
            if (!b) {
                // @ts-ignore todo: improve typings
                program.group = outros;
                outros.r += 1;
            }
            if (running_program) {
                pending_program = program;
            }
            else {
                // if this is an intro, and there's a delay, we need to do
                // an initial tick and/or apply CSS animation immediately
                if (css) {
                    clear_animation();
                    animation_name = create_rule(node, t, b, duration, delay, easing, css);
                }
                if (b)
                    tick(0, 1);
                running_program = init(program, duration);
                add_render_callback(() => dispatch(node, b, 'start'));
                loop(now => {
                    if (pending_program && now > pending_program.start) {
                        running_program = init(pending_program, duration);
                        pending_program = null;
                        dispatch(node, running_program.b, 'start');
                        if (css) {
                            clear_animation();
                            animation_name = create_rule(node, t, running_program.b, running_program.duration, 0, easing, config.css);
                        }
                    }
                    if (running_program) {
                        if (now >= running_program.end) {
                            tick(t = running_program.b, 1 - t);
                            dispatch(node, running_program.b, 'end');
                            if (!pending_program) {
                                // we're done
                                if (running_program.b) {
                                    // intro — we can tidy up immediately
                                    clear_animation();
                                }
                                else {
                                    // outro — needs to be coordinated
                                    if (!--running_program.group.r)
                                        run_all(running_program.group.c);
                                }
                            }
                            running_program = null;
                        }
                        else if (now >= running_program.start) {
                            const p = now - running_program.start;
                            t = running_program.a + running_program.d * easing(p / running_program.duration);
                            tick(t, 1 - t);
                        }
                    }
                    return !!(running_program || pending_program);
                });
            }
        }
        return {
            run(b) {
                if (is_function(config)) {
                    wait().then(() => {
                        // @ts-ignore
                        config = config();
                        go(b);
                    });
                }
                else {
                    go(b);
                }
            },
            end() {
                clear_animation();
                running_program = pending_program = null;
            }
        };
    }

    const globals = (typeof window !== 'undefined' ? window : global);
    function create_component(block) {
        block && block.c();
    }
    function mount_component(component, target, anchor) {
        const { fragment, on_mount, on_destroy, after_update } = component.$$;
        fragment && fragment.m(target, anchor);
        // onMount happens before the initial afterUpdate
        add_render_callback(() => {
            const new_on_destroy = on_mount.map(run).filter(is_function);
            if (on_destroy) {
                on_destroy.push(...new_on_destroy);
            }
            else {
                // Edge case - component was destroyed immediately,
                // most likely as a result of a binding initialising
                run_all(new_on_destroy);
            }
            component.$$.on_mount = [];
        });
        after_update.forEach(add_render_callback);
    }
    function destroy_component(component, detaching) {
        const $$ = component.$$;
        if ($$.fragment !== null) {
            run_all($$.on_destroy);
            $$.fragment && $$.fragment.d(detaching);
            // TODO null out other refs, including component.$$ (but need to
            // preserve final state?)
            $$.on_destroy = $$.fragment = null;
            $$.ctx = [];
        }
    }
    function make_dirty(component, i) {
        if (component.$$.dirty[0] === -1) {
            dirty_components.push(component);
            schedule_update();
            component.$$.dirty.fill(0);
        }
        component.$$.dirty[(i / 31) | 0] |= (1 << (i % 31));
    }
    function init(component, options, instance, create_fragment, not_equal, props, dirty = [-1]) {
        const parent_component = current_component;
        set_current_component(component);
        const prop_values = options.props || {};
        const $$ = component.$$ = {
            fragment: null,
            ctx: null,
            // state
            props,
            update: noop,
            not_equal,
            bound: blank_object(),
            // lifecycle
            on_mount: [],
            on_destroy: [],
            before_update: [],
            after_update: [],
            context: new Map(parent_component ? parent_component.$$.context : []),
            // everything else
            callbacks: blank_object(),
            dirty
        };
        let ready = false;
        $$.ctx = instance
            ? instance(component, prop_values, (i, ret, ...rest) => {
                const value = rest.length ? rest[0] : ret;
                if ($$.ctx && not_equal($$.ctx[i], $$.ctx[i] = value)) {
                    if ($$.bound[i])
                        $$.bound[i](value);
                    if (ready)
                        make_dirty(component, i);
                }
                return ret;
            })
            : [];
        $$.update();
        ready = true;
        run_all($$.before_update);
        // `false` as a special case of no DOM component
        $$.fragment = create_fragment ? create_fragment($$.ctx) : false;
        if (options.target) {
            if (options.hydrate) {
                const nodes = children(options.target);
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.l(nodes);
                nodes.forEach(detach);
            }
            else {
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.c();
            }
            if (options.intro)
                transition_in(component.$$.fragment);
            mount_component(component, options.target, options.anchor);
            flush();
        }
        set_current_component(parent_component);
    }
    class SvelteComponent {
        $destroy() {
            destroy_component(this, 1);
            this.$destroy = noop;
        }
        $on(type, callback) {
            const callbacks = (this.$$.callbacks[type] || (this.$$.callbacks[type] = []));
            callbacks.push(callback);
            return () => {
                const index = callbacks.indexOf(callback);
                if (index !== -1)
                    callbacks.splice(index, 1);
            };
        }
        $set() {
            // overridden by instance, if it has props
        }
    }

    function dispatch_dev(type, detail) {
        document.dispatchEvent(custom_event(type, Object.assign({ version: '3.20.1' }, detail)));
    }
    function append_dev(target, node) {
        dispatch_dev("SvelteDOMInsert", { target, node });
        append(target, node);
    }
    function insert_dev(target, node, anchor) {
        dispatch_dev("SvelteDOMInsert", { target, node, anchor });
        insert(target, node, anchor);
    }
    function detach_dev(node) {
        dispatch_dev("SvelteDOMRemove", { node });
        detach(node);
    }
    function listen_dev(node, event, handler, options, has_prevent_default, has_stop_propagation) {
        const modifiers = options === true ? ["capture"] : options ? Array.from(Object.keys(options)) : [];
        if (has_prevent_default)
            modifiers.push('preventDefault');
        if (has_stop_propagation)
            modifiers.push('stopPropagation');
        dispatch_dev("SvelteDOMAddEventListener", { node, event, handler, modifiers });
        const dispose = listen(node, event, handler, options);
        return () => {
            dispatch_dev("SvelteDOMRemoveEventListener", { node, event, handler, modifiers });
            dispose();
        };
    }
    function attr_dev(node, attribute, value) {
        attr(node, attribute, value);
        if (value == null)
            dispatch_dev("SvelteDOMRemoveAttribute", { node, attribute });
        else
            dispatch_dev("SvelteDOMSetAttribute", { node, attribute, value });
    }
    function prop_dev(node, property, value) {
        node[property] = value;
        dispatch_dev("SvelteDOMSetProperty", { node, property, value });
    }
    function set_data_dev(text, data) {
        data = '' + data;
        if (text.data === data)
            return;
        dispatch_dev("SvelteDOMSetData", { node: text, data });
        text.data = data;
    }
    function validate_each_argument(arg) {
        if (typeof arg !== 'string' && !(arg && typeof arg === 'object' && 'length' in arg)) {
            let msg = '{#each} only iterates over array-like objects.';
            if (typeof Symbol === 'function' && arg && Symbol.iterator in arg) {
                msg += ' You can use a spread to convert this iterable into an array.';
            }
            throw new Error(msg);
        }
    }
    function validate_slots(name, slot, keys) {
        for (const slot_key of Object.keys(slot)) {
            if (!~keys.indexOf(slot_key)) {
                console.warn(`<${name}> received an unexpected slot "${slot_key}".`);
            }
        }
    }
    class SvelteComponentDev extends SvelteComponent {
        constructor(options) {
            if (!options || (!options.target && !options.$$inline)) {
                throw new Error(`'target' is a required option`);
            }
            super();
        }
        $destroy() {
            super.$destroy();
            this.$destroy = () => {
                console.warn(`Component was already destroyed`); // eslint-disable-line no-console
            };
        }
        $capture_state() { }
        $inject_state() { }
    }

    function cubicOut(t) {
        const f = t - 1.0;
        return f * f * f + 1.0;
    }

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */

    function __rest(s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }
    function fade(node, { delay = 0, duration = 400, easing = identity }) {
        const o = +getComputedStyle(node).opacity;
        return {
            delay,
            duration,
            easing,
            css: t => `opacity: ${t * o}`
        };
    }
    function scale(node, { delay = 0, duration = 400, easing = cubicOut, start = 0, opacity = 0 }) {
        const style = getComputedStyle(node);
        const target_opacity = +style.opacity;
        const transform = style.transform === 'none' ? '' : style.transform;
        const sd = 1 - start;
        const od = target_opacity * (1 - opacity);
        return {
            delay,
            duration,
            easing,
            css: (_t, u) => `
			transform: ${transform} scale(${1 - (sd * u)});
			opacity: ${target_opacity - (od * u)}
		`
        };
    }
    function crossfade(_a) {
        var { fallback } = _a, defaults = __rest(_a, ["fallback"]);
        const to_receive = new Map();
        const to_send = new Map();
        function crossfade(from, node, params) {
            const { delay = 0, duration = d => Math.sqrt(d) * 30, easing = cubicOut } = assign(assign({}, defaults), params);
            const to = node.getBoundingClientRect();
            const dx = from.left - to.left;
            const dy = from.top - to.top;
            const dw = from.width / to.width;
            const dh = from.height / to.height;
            const d = Math.sqrt(dx * dx + dy * dy);
            const style = getComputedStyle(node);
            const transform = style.transform === 'none' ? '' : style.transform;
            const opacity = +style.opacity;
            return {
                delay,
                duration: is_function(duration) ? duration(d) : duration,
                easing,
                css: (t, u) => `
				opacity: ${t * opacity};
				transform-origin: top left;
				transform: ${transform} translate(${u * dx}px,${u * dy}px) scale(${t + (1 - t) * dw}, ${t + (1 - t) * dh});
			`
            };
        }
        function transition(items, counterparts, intro) {
            return (node, params) => {
                items.set(params.key, {
                    rect: node.getBoundingClientRect()
                });
                return () => {
                    if (counterparts.has(params.key)) {
                        const { rect } = counterparts.get(params.key);
                        counterparts.delete(params.key);
                        return crossfade(rect, node, params);
                    }
                    // if the node is disappearing altogether
                    // (i.e. wasn't claimed by the other list)
                    // then we need to supply an outro
                    items.delete(params.key);
                    return fallback && fallback(node, params, intro);
                };
            };
        }
        return [
            transition(to_send, to_receive, false),
            transition(to_receive, to_send, true)
        ];
    }

    var commonjsGlobal = typeof globalThis !== 'undefined' ? globalThis : typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

    function unwrapExports (x) {
    	return x && x.__esModule && Object.prototype.hasOwnProperty.call(x, 'default') ? x['default'] : x;
    }

    function createCommonjsModule(fn, module) {
    	return module = { exports: {} }, fn(module, module.exports), module.exports;
    }

    var index_umd = createCommonjsModule(function (module, exports) {
    !function(e,n){n(exports);}(commonjsGlobal,function(e){var a=function(){return (a=Object.assign||function(e){for(var n,t=1,o=arguments.length;t<o;t++)for(var r in n=arguments[t])Object.prototype.hasOwnProperty.call(n,r)&&(e[r]=n[r]);return e}).apply(this,arguments)};function u(){for(var e=0,n=0,t=arguments.length;n<t;n++)e+=arguments[n].length;var o=Array(e),r=0;for(n=0;n<t;n++)for(var i=arguments[n],p=0,a=i.length;p<a;p++,r++)o[r]=i[p];return o}function o(i,e){var o,r,p=(o={current:0,next:function(){return ++this.current}},r={},{add:function(e,n){var t=null!=n?n:o.next();return r[t]=e,t},resolve:function(e,n,t){var o=r[e];o&&(t(n)?o.resolve(n):o.reject(n),r[e]=null);}});return e(function(e){if(e.detail&&e.detail.data&&"request_id"in e.detail.data){var n=e.detail.data,t=n.request_id,o=function(e,n){var t={};for(var o in e)Object.prototype.hasOwnProperty.call(e,o)&&n.indexOf(o)<0&&(t[o]=e[o]);if(null!=e&&"function"==typeof Object.getOwnPropertySymbols){var r=0;for(o=Object.getOwnPropertySymbols(e);r<o.length;r++)n.indexOf(o[r])<0&&Object.prototype.propertyIsEnumerable.call(e,o[r])&&(t[o[r]]=e[o[r]]);}return t}(n,["request_id"]);t&&p.resolve(t,o,function(e){return !("error_type"in e)});}}),function(o,r){return void 0===r&&(r={}),new Promise(function(e,n){var t=p.add({resolve:e,reject:n},r.request_id);i(o,a(a({},r),{request_id:t}));})}}var n="undefined"!=typeof window,d=Boolean(n&&window.AndroidBridge),s=Boolean(n&&window.webkit&&window.webkit.messageHandlers&&window.webkit.messageHandlers.VKWebAppClose),f=n&&!d&&!s,t=f&&"mobile_web"===new URLSearchParams(location.search).get("vk_platform"),r=f?"message":"VKWebAppEvent",l=u(["VKWebAppInit","VKWebAppGetCommunityAuthToken","VKWebAppAddToCommunity","VKWebAppGetUserInfo","VKWebAppSetLocation","VKWebAppGetClientVersion","VKWebAppGetPhoneNumber","VKWebAppGetEmail","VKWebAppGetGeodata","VKWebAppSetTitle","VKWebAppGetAuthToken","VKWebAppCallAPIMethod","VKWebAppJoinGroup","VKWebAppAllowMessagesFromGroup","VKWebAppDenyNotifications","VKWebAppAllowNotifications","VKWebAppOpenPayForm","VKWebAppOpenApp","VKWebAppShare","VKWebAppShowWallPostBox","VKWebAppScroll","VKWebAppResizeWindow","VKWebAppShowOrderBox","VKWebAppShowLeaderBoardBox","VKWebAppShowInviteBox","VKWebAppShowRequestBox","VKWebAppAddToFavorites","VKWebAppShowCommunityWidgetPreviewBox"],f&&!t?["VKWebAppShowStoryBox"]:[]),c=n?window.AndroidBridge:void 0,b=s?window.webkit.messageHandlers:void 0;function i(e,n){var t=n||{bubbles:!1,cancelable:!1,detail:void 0},o=document.createEvent("CustomEvent");return o.initCustomEvent(e,!!t.bubbles,!!t.cancelable,t.detail),o}"undefined"==typeof window||window.CustomEvent||(window.CustomEvent=(i.prototype=Event.prototype,i));var p=function(t){var i=void 0,p=[];function e(e){p.push(e);}"undefined"!=typeof window&&"addEventListener"in window&&window.addEventListener(r,function(n){if(s||d)return u(p).map(function(e){return e.call(null,n)});if(f&&n&&n.data){var e=n.data,t=e.type,o=e.data,r=e.frameId;t&&"VKWebAppSettings"===t?i=r:u(p).map(function(e){return e({detail:{type:t,data:o}})});}});var n=o(function(e,n){c&&c[e]?c[e](JSON.stringify(n)):b&&b[e]&&"function"==typeof b[e].postMessage?b[e].postMessage(n):f&&parent.postMessage({handler:e,params:n,type:"vk-connect",webFrameId:i,connectVersion:t},"*");},e);return {send:n,sendPromise:n,subscribe:e,unsubscribe:function(e){var n=p.indexOf(e);-1<n&&p.splice(n,1);},supports:function(e){return d?!(!c||"function"!=typeof c[e]):s?!(!b||!b[e]||"function"!=typeof b[e].postMessage):f&&-1<l.indexOf(e)},isWebView:function(){return s||d}}}("2.1.6");e.applyMiddleware=function e(){for(var o=[],n=0;n<arguments.length;n++)o[n]=arguments[n];return o.includes(void 0)||o.includes(null)?e.apply(void 0,o.filter(function(e){return "function"==typeof e})):function(t){if(0===o.length)return t;var e,n={subscribe:t.subscribe,send:function(){for(var e=[],n=0;n<arguments.length;n++)e[n]=arguments[n];return t.send.apply(t,e)}};return e=o.filter(function(e){return "function"==typeof e}).map(function(e){return e(n)}).reduce(function(n,t){return function(e){return n(t(e))}})(t.send),a(a({},t),{send:e})}},e.default=p,Object.defineProperty(e,"__esModule",{value:!0});});
    });

    var bridge = unwrapExports(index_umd);

    const subscriber_queue = [];
    /**
     * Creates a `Readable` store that allows reading by subscription.
     * @param value initial value
     * @param {StartStopNotifier}start start and stop notifications for subscriptions
     */
    function readable(value, start) {
        return {
            subscribe: writable(value, start).subscribe,
        };
    }
    /**
     * Create a `Writable` store that allows both updating and reading by subscription.
     * @param {*=}value initial value
     * @param {StartStopNotifier=}start start and stop notifications for subscriptions
     */
    function writable(value, start = noop) {
        let stop;
        const subscribers = [];
        function set(new_value) {
            if (safe_not_equal(value, new_value)) {
                value = new_value;
                if (stop) { // store is ready
                    const run_queue = !subscriber_queue.length;
                    for (let i = 0; i < subscribers.length; i += 1) {
                        const s = subscribers[i];
                        s[1]();
                        subscriber_queue.push(s, value);
                    }
                    if (run_queue) {
                        for (let i = 0; i < subscriber_queue.length; i += 2) {
                            subscriber_queue[i][0](subscriber_queue[i + 1]);
                        }
                        subscriber_queue.length = 0;
                    }
                }
            }
        }
        function update(fn) {
            set(fn(value));
        }
        function subscribe(run, invalidate = noop) {
            const subscriber = [run, invalidate];
            subscribers.push(subscriber);
            if (subscribers.length === 1) {
                stop = start(set) || noop;
            }
            run(value);
            return () => {
                const index = subscribers.indexOf(subscriber);
                if (index !== -1) {
                    subscribers.splice(index, 1);
                }
                if (subscribers.length === 0) {
                    stop();
                    stop = null;
                }
            };
        }
        return { set, update, subscribe };
    }
    function derived(stores, fn, initial_value) {
        const single = !Array.isArray(stores);
        const stores_array = single
            ? [stores]
            : stores;
        const auto = fn.length < 2;
        return readable(initial_value, (set) => {
            let inited = false;
            const values = [];
            let pending = 0;
            let cleanup = noop;
            const sync = () => {
                if (pending) {
                    return;
                }
                cleanup();
                const result = fn(single ? values[0] : values, set);
                if (auto) {
                    set(result);
                }
                else {
                    cleanup = is_function(result) ? result : noop;
                }
            };
            const unsubscribers = stores_array.map((store, i) => subscribe(store, (value) => {
                values[i] = value;
                pending &= ~(1 << i);
                if (inited) {
                    sync();
                }
            }, () => {
                pending |= (1 << i);
            }));
            inited = true;
            sync();
            return function stop() {
                run_all(unsubscribers);
                cleanup();
            };
        });
    }

    function BufferLoader(context, urlList, callback) {
        this.context = context;
        this.urlList = urlList;
        this.onload = callback;
        this.bufferList = new Array();
        this.loadCount = 0;
      }
      
      BufferLoader.prototype.loadBuffer = function(url, index) {
        // Load buffer asynchronously
        var request = new XMLHttpRequest();
        request.open("GET", url, true);
        request.responseType = "arraybuffer";
      
        var loader = this;
      
        request.onload = function() {
          // Asynchronously decode the audio file data in request.response
          loader.context.decodeAudioData(
            request.response,
            function(buffer) {
              if (!buffer) {
                console.log('error decoding file data: ' + url);
                return;
              }
              loader.bufferList[index] = buffer;
              if (++loader.loadCount == loader.urlList.length)
                loader.onload(loader.bufferList);
            },
            function(error) {
              console.error('decodeAudioData error', error);
            }
          );
        };
      
        request.onerror = function(err) {
            preloadIndicator.assetNotLoaded(urlList[0], err);
          
        };
      
        request.send();
      };
      
      BufferLoader.prototype.load = function() {
        for (var i = 0; i < this.urlList.length; ++i)
        this.loadBuffer(this.urlList[i], i);
      };

    var translation = {
      IMMUNITY: {
        ru: "Иммунитет",
        en: "Immunity",
      },
      CATCH_VIRUSES: {
        ru: "Давите вирусы",
        en: "Catch the viruses",
      },
      ANSWER_QUESTIONS_WIDGET: {
        ru: "Отвечайте на вопросы",
        en: "Answer the questions",
      },
      START: {
        ru: "Начать",
        en: "Start",
      },
      ENTER_YOUR_AGE: {
        ru: "Введите ваш возраст",
        en: "Submit your age",
      },
      MAY_AGE_IS: {
        ru: "Мой возраст",
        en: "My age is",
      },
      SUBMIT: {
        ru: "Подтвердить",
        en: "Submit",
      },
      IMMUNITY_VALUE: {
        ru: "ИММУНИТЕТ",
        en: "IMMUNITY",
      },
      VIRUS_WIN: {
        ru: "Вирус победил",
        en: "The virus has spread",
      },
      BUT_YOU_CAN_CHANGE: {
        ru: "Но ты можешь исправить ситуацию",
        en: "But you can still change things",
      },
      REPLAY: {
        ru: "Играть еще раз",
        en: "Play again",
      },
      SHARE: {
        ru: "Рассказать друзьям",
        en: "Share with friends",
      },
      TWITTER_TEXT: {
        ru: "Останови COVID-19",
        en: "Stop COVID-19 spread",
      },
      LEARN_MORE: {
        ru: "Узнать больше о коронавирусе",
        en: "Learn more about coronavirus",
      },
      CONGRATULATIONS: {
        ru: "Поздравляем!",
        en: "Excellent! You did it.",
      },
      YOU_DID_IT: {
        ru: "Вы смогли победить вирус!",
        en: "The best way to fight the virus is to stay at home! We can help you.",
      },
      VIRUS_DEFEATED: {
        ru: "Ура! Вирус побежден",
        en: "Virus defeated!",
      },
      TEST_KNOWLEDGE: {
        ru: "Теперь проверьте свои знания, ответив на пару вопросов",
        en: "Now test your knowledge by answering a few questions",
      },
      ANSWER_QUESTIONS: {
        ru: "Ответить на вопросы!",
        en: "Answer the questions!",
      },
      PROCEED: {
        ru: "Продолжить",
        en: "Proceed",
      },
      PRESS_AND_HOLD: {
        ru: "Нажми и удерживай!",
        en: "Press and hold!",
      },
      MORE_INFO: {
        ru: "Больше информации на",
        en: "More info at",
      },
      PLAY_WITH_SOUND:{
        ru:"Рекомендуем\nиграть\nсо звуком",
        en:'Recommended\nto play\nwith sound'
      },
      PRIZE_DRAW:{
        ru:'Вы участвуете в розыгрыше призов! Повтори свой успех завтра',
        en:'You are participating in a prize draw! Repeat you success tomorrow'
      },
      REPEAT_YOUR_SUCESS:{
        ru:'Повтори свой успех во Вконтакте и участвуй в ежедневном розыгрыше призов',
        en:'Repeat your success on Vkontakte and participate in the daily prize draw'
      },
      WIN_TO_GET_PRIZE:{
        ru:'Выигрывай и участвуй в ежедневном розыгрыше призов',
        en:'Win and participate in the daily prize draw'
      },
      PLAY_IN_VK_TO_WIN:{
        ru:'Играй во Вконтакте и участвуй в ежедневном розыгрыше призов',
        en:'Play on Vkontakte and participate in the daily prize draw'
      },
      INTRO_PHRASES: {
        ru: [
          "Вирус передаётся при ĸашле, чихании, разговоре, с пылевыми частицами в воздухе, через руĸопожатия, предметы обихода",
          "Симптомы ĸоронавируса: Высоĸая температура, ĸашель, отдышĸа, боль в мышцах, утомляемость",
          "Иногда при ĸоронавирусе возниĸает головная боль, заложенность в груди, диарея, тошнота",
          "Симптомы могут проявится в течении 14 дней после ĸонтаĸта с инфеĸционным больным.",
          "Воздержитесь от посещения общественных мест",
          "Не ĸасайтесь грязными руĸами глаз, лица и рта",
          "Избегайте близĸих ĸонтаĸтов с людьми, имеющими признаĸи ОРВИ",
          "Мойте руĸи с мылом и водой.",
          "Дезиинфицируйте гаджеты, оргтехниĸу и поверхности ĸ ĸоторым приĸасаетесь.",
          "Ограничьте по возможности при приветствии тесные объятия и руĸопожатия",
          "Коронавирус распространяется через ĸапли, ĸоторые образуются, ĸогда инфицированный человеĸ ĸашляет или чихает, ĸроме того, они могут распространяться, ĸогда инфицированный человеĸ ĸасается любой загрязнённой поверхности",
          "Часто проветриваете помещение и по возможности соблюдайте дистанцию не менее 1 м при общении с родными, друзьями и близĸими",
          "Носите с собой одноразовые салфетĸи и всегда приĸрывайте нос и рот, ĸогда вы ĸашляете",
          "Держите руĸи в чистоте, часто мойте их водой с мылом или используйте дезинфицирующее средство, старайтесь не ĸасаться рта, носа или глаз немытыми руĸами",
          "Если у вас есть симптомы, обратитесь ĸ врачу. Не посещайте работу в течение 14 дней.",
          "Держите руĸи в чистоте, часто мойте их водой с мылом или используйте дезинфицирующее средство.",
          "Старайтесь не ĸасаться рта, носа или глаз немытыми руĸами (обычно таĸие приĸосновения неосознанно",
          "На работе регулярно очищайте поверхности и устройства, ĸ ĸоторым вы приĸасаетесь",
          "Носите с собой одноразовые салфетĸи и всегда приĸрывайте нос и рот, ĸогда вы ĸашляете или чихаете.",
          "Не ешьте еду из общих упаĸовоĸ или посуды, если другие люди погружали в них свои пальцы.",
          "Часто проветривайте помещения.",
          "По возможности, соблюдайте дистанцию не менее 1 метра при общении с ĸоллегами, друзьями, родными и близĸими",
          "Лечение назначает врач в зависимости от симптомов в соответствии с российсĸими и международными реĸомендациями. Самолечение противопоĸазано.",
          "Медицинсĸая помощь всем пациентам и лицам с подозрением на COVID-19 оĸазывается на бесплатной основе.",
        ],
        en: [
          "Coronavirus can be transmitted through small droplets from the nose or mouth which are spread when an infected person coughs or exhales",
          "Wash hands often with soap and water. Or use sanitizer gel. Avoid touching your mouth, nose, with unwashed hands",
          "Cough and sneeze into a disposable tissue",
          "Always ventilate your room and keep a huge social distance - more than three feet away from a person",
          "Coronavirus can be transmitted through small droplets from the nose or mouth which are spread when an infected person coughs or exhales, or by touching a surface on which droplets have landed.",
          "Symptoms may include: fever, cough, shortness of breath, body akes, tiredness",
          "It may take up to 14 days to start showing the symptoms",
          "If you have the symptoms like the coronavirus, stay home and seek medical advice",
          "Good girl! But no matter how hard you try, if your friends don’t know the rules, they might get infected. Share the game, not the virus",
          "Score! But to make America great again, you stay home! Don’t get bored – check out Screen lifer",
          "The best way to fight the virus is to stay at home. Go check Screen Lifer right now",
          "Coronavirus is transmitted when we cough sneeze and talk, with dust particles in the air, through handshakes and household items",
          "Headaches, chest tightness, diarrhea and nausea can be signs of the coronavirus",
          "Avoid public gatherings",
          "Don’t touch your face and mouth with unwashed hands",
          "Avoid direct contact with people who have flu-like symptoms",
          "Wash hands with soap and water",
          "Disinfect gadgets, office equipment and the surfaces you touch",
          "Stop handshakes, hugs or kisses",
          "At work, regularly clean surfaces and things you touch.",
          "Do not eat food from common packages or with utensils if other people touched them",
          "Ventilate your room often",
          "Keep a huge social distance - more than three feet away from a person",
          "Follow your doctor instructions and treatments",
        ],
      },
    };

    const FALLBACK_LANG = "en";
    const detectLang = () => {
      if (localStorage.getItem("covidlang"))
        return localStorage.getItem("covidlang");
      if (window.navigator.languages)
        return window.navigator.languages.some(
          (item) => item === "ru" || item === "ru-RU"
        )
          ? "ru"
          : FALLBACK_LANG;
      const language = window.navigator.userLanguage || window.navigator.language;
      return language === "ru" || language === "ru-RU" ? "ru" : FALLBACK_LANG;
    };

    const lang = detectLang();

    const text$1 = (phrase) => {
      return translation[phrase] ? translation[phrase][lang] || phrase : phrase;
    };

    const GAME_VIEWS = {
      INTRO: "INTRO",
      GAME: "GAME",
      AGE: "AGE",
      LOSE: "LOSE",
      WIN: "WIN",
      QUIZ: "QUIZ",
      QUIZ_INTRO: "QUIZ_INTRO",
    };
    const IS_TEST = /(localhost|192\.168|az67128.gitlab.io)/.test(
      window.location.href
    );

    const IS_WIDGET = !/(localhost|192\.168|screenlife.tech|az67128.gitlab.io)/.test(
      window.location.href
    );
    const IS_VK = /vk_app_id/.test(window.location.href);
    const IS_MOBILE_VK = /vk_platform\=mobile/.test(window.location.href);

    const BASE_URL = !IS_WIDGET
      ? "."
      : // : "https://az67128.gitlab.io/svelte-covid";
        "https://screenlife.tech/quarantine";

    const VIRUS_RADIUS = 160;
    const VIRUS_TOUCH_RADIUS = 130;
    const INITIAL_VIRUS_VELOCITY = 17;
    const VIRUS_VELOCITY_CHANGE = 1.2; //1.5
    const VIRUS_VELOCITY_REPLAY_VALUE = 0.8;
    const RELEASE_VIRUS_VELOCITY_CHANGE = 7;
    const INITIAL_IMMUNITY = 20;
    const TICK_TIMEOUT = 100;
    const ADD_IMMUNITY_VALUE = 0.1; //0.1
    const SUB_IMMUNITY_VALUE = -0.1;
    const LOST_SUB_IMMUNITY_VALUE = -5;
    const VIRUS_PROTECTED_TIME = 2000;

    const QUESTION_COUNT = 5; // 5
    const QUIZ_TIME = 20000; //miliseconds 20000

    const SAMPLES = {
      SMS: [{ file: "SMS", }],
      SOUNDTRACK: [{ file: "elki", loop: true, value: 0.2 }],
      EXPLSOION: ["explosion"],
      PIG: [{ file: "pig", loop: 1, value: 0.1 }],
      PRESSING: [{ file: "pressing", value: 0.1 }],
      VIRUS_RUNAWAY: [{ file: "runaway", value: 0.1 }],
      SCREAM: [{ file: "scream", value: 0.1 }],
      TICK: [{ file: "tick", loop: true, value: 0.5 }],
      BUBLE: ["bubble"],
      GAME_START: lang === "ru" ? ["A-1-RU"] : [{ file: "A-1-EN", value: 2 }],
      INFO:
        lang === "ru"
          ? [{ file: "B-1-RU", value: 1 }]
          : [{ file: "B-1-EN", value: 2 }],
      GAME_RULES:
        lang === "ru"
          ? [{ file: "C-1-RU", value: 1 }]
          : [{ file: "C-1-EN", value: 2 }],
      CATCH:
        lang === "ru"
          ? [
              { file: "D-1-RU", value: 0.8 },
              { file: "D-2-RU", value: 0.8 },
              { file: "D-3-RU", value: 0.8 },
              { file: "D-4-RU", value: 0.8 },
              { file: "D-5-RU", value: 0.8 },
              { file: "D-6-RU", value: 0.8 },
              { file: "D-7-RU", value: 0.8 },
            ]
          : [
              { file: "D-1-EN", value: 2 },
              { file: "D-2-EN", value: 2 },
              { file: "D-3-EN", value: 2 },
              { file: "D-4-EN", value: 2 },
              { file: "D-5-EN", value: 2 },
              { file: "D-6-EN", value: 2 },
              { file: "D-7-EN", value: 2 },
              { file: "D-8-EN", value: 2 },
            ],
      RUNAWAY:
        lang === "ru"
          ? [
              { file: "E-1-RU", value: 1 },
              { file: "E-2-RU", value: 1 },
              { file: "E-3-RU", value: 1 },
              { file: "E-4-RU", value: 1 },
              { file: "E-5-RU", value: 1 },
              { file: "E-6-RU", value: 1 },
            ]
          : [
              { file: "E-1-EN", value: 2 },
              { file: "E-2-EN", value: 2 },
              { file: "E-3-EN", value: 2 },
              { file: "E-4-EN", value: 2 },
              { file: "E-5-EN", value: 2 },
            ],
      LISTEN_TO_ME:
        lang === "ru"
          ? [
              { file: "F-1-RU", value: 1.5 },
              { file: "F-2-RU", value: 1.5 },
              { file: "F-3-RU", value: 1.5 },
              { file: "F-4-RU", value: 1.5 },
              { file: "F-5-RU", value: 1.5 },
            ]
          : ["F-1-EN", "F-2-EN", "F-3-EN", "F-4-EN"],
      RULES:
        lang === "ru"
          ? [
              { file: "G-1-RU-F", value: 1.7 },
              { file: "G-2-RU-F", value: 1.7 },
              { file: "G-3-RU-F", value: 1.7 },
              { file: "G-4-RU-F", value: 1.7 },
              { file: "G-5-RU-F", value: 1.7 },
              { file: "G-6-RU-F", value: 1.7 },
              { file: "G-7-RU-F", value: 1.7 },
              { file: "G-8-RU-F", value: 1.7 },
              { file: "G-9-RU-F", value: 1.7 },
              { file: "G-10-RU-F", value: 1.7 },
              { file: "G-11-RU-F", value: 1.7 },
              { file: "G-12-RU-F", value: 1.7 },
              { file: "G-13-RU-F", value: 1.7 },
              { file: "G-14-RU-F", value: 1.7 },
            ]
          : [
              { file: "G-1-EN", value: 2 },
              { file: "G-2-EN", value: 2 },
              { file: "G-3-EN", value: 2 },
              { file: "G-4-EN", value: 2 },
              { file: "G-5-EN", value: 2 },
              { file: "G-6-EN", value: 2 },
              { file: "G-7-EN", value: 2 },
              { file: "G-8-EN", value: 2 },
              { file: "G-9-EN", value: 2 },
              { file: "G-10-EN", value: 2 },
              { file: "G-11-EN", value: 2 },
              { file: "G-12-EN", value: 2 },
              { file: "G-13-EN", value: 2 },
              { file: "G-14-EN", value: 2 },
              { file: "G-15-EN", value: 2 },
              { file: "G-16-EN", value: 2 },
              { file: "G-17-EN", value: 2 },
              { file: "G-18-EN", value: 2 },
              { file: "G-19-EN", value: 2 },
            ],
      QUIZ_INTRO:
        lang === "ru"
          ? [
              { file: "H-1-RU", value: 0.8 },
              { file: "H-2-RU", value: 0.8 },
              { file: "H-3-RU", value: 0.8 },
            ]
          : [
              { file: "H-1-EN", value: 2 },
              { file: "H-2-EN", value: 2 },
              { file: "H-3-EN", value: 2 },
            ],
      GAME_LOSE:
        lang === "ru"
          ? [
              { file: "J-1-RU", value: 1.5 },
              { file: "J-2-RU", value: 1.5 },
              { file: "J-3-RU", value: 1.5 },
              { file: "J-4-RU", value: 1.5 },
            ]
          : [
              { file: "J-1-EN", value: 2 },
              { file: "J-2-EN", value: 2 },
              { file: "J-3-EN", value: 2 },
            ],
      RIGHT_ANSWER:
        lang === "ru"
          ? ["K-1-RU", "K-2-RU", "K-3-RU"]
          : [
              { file: "K-1-EN", value: 2 },
              { file: "K-2-EN", value: 2 },
            ],
      WRONG_ANWSER:
        lang === "ru"
          ? ["L-1-RU", "L-2-RU", "L-3-RU"]
          : [
              { file: "L-1-EN", value: 2 },
              { file: "L-2-EN", value: 2 },
              { file: "L-3-EN", value: 2 },
            ],
      GAME_WIN:
        lang === "ru"
          ? ["M-1-RU", "M-2-RU"]
          : [
              { file: "M-1-EN", value: 2 },
              { file: "M-2-EN", value: 2 },
            ],
    };

    window.AudioContext = window.AudioContext || window.webkitAudioContext;
    const audioContext = new AudioContext();
    const audioElements = {};
    const audioBuffers = {};
    const samples = {};
    window.audioBuffers = audioBuffers;
    window.audioElements = audioElements;
    const initAudio = () => {
      const loadAudio = (item, key) => new Promise(resolve =>{
        const file = typeof item === "string" ? item : item.file;
        const url = IS_WIDGET ? `${BASE_URL}/assets/?file=audio/${file}.mp3` : `${BASE_URL}/assets/audio/${file}.mp3`;
        
        const bufferLoader = new BufferLoader(audioContext, [url], (bufferList) => {
          if (!audioBuffers[key]) audioBuffers[key] = [];
          const buffer = {
            buffer: bufferList[0],
            loop: Boolean(item.loop),
            value: item.value || 1
          };
          audioBuffers[key].push(buffer);
          preloadIndicator.assetLoaded();
          resolve();
        });
        bufferLoader.load();
      });

      const promiseArray = [];
      Object.keys(SAMPLES).forEach((key) => {
        SAMPLES[key].forEach(item=>{
          promiseArray.push(loadAudio(item, key));
        });
      });
      Promise.all(promiseArray).then(()=>{
        Object.keys(audioBuffers).forEach(key=>{
          audioBuffers[key] = audioBuffers[key].sort(() => 0.5 - Math.random());
        });
      });
    };
    initAudio();

    const stopAudio = (type) => {
      if (audioElements[type]) {
        audioElements[type].source.onended = () => {};
        audioElements[type].source.stop();
        audioElements[type].gainNode.disconnect(audioContext.destination);
        delete audioElements[type];
      }
    };

    const playAudio = async (type, index, callback) => {
      stopAudio(type);

      let gainNode = audioContext.createGain();
      let source = audioContext.createBufferSource();
      source = audioContext.createBufferSource();
      source.buffer = audioBuffers[type][index].buffer;
      source.loop = audioBuffers[type][index].loop;
      if (callback) source.onended = callback;
      source.connect(gainNode);
      gainNode.connect(audioContext.destination);
      source.start(0);
      gainNode.gain.value = audioBuffers[type][index].value;
      audioElements[type] = { gainNode, source };
    };

    const stopAllAudio = (keepSoundtrack = false) => {
      Object.keys(SAMPLES).forEach((key) => {
        if (keepSoundtrack && key === "SOUNDTRACK") return;
        stopAudio(key);
      });
    };

    const playSample = async (type, callback) => {
      if (!audioBuffers[type]) return;
      if (document.visibilityState === "hidden") return;
      stopSample(type);
      if (samples[type] === undefined) samples[type] = -1;

      samples[type] += 1;
      if (samples[type] === audioBuffers[type].length) samples[type] = 0;
      await playAudio(type, samples[type], callback);
    };

    const stopSample = (type) => {
      stopAudio(type);
    };

    const playCatch = async () => {
      const rulesCallback = () => playSample("RULES", rulesCallback);
      if (audioElements.SOUNDTRACK)
        audioElements.SOUNDTRACK.gainNode.gain.value = SAMPLES.SOUNDTRACK[0].value;
      stopSample("GAME_RULES");
      stopSample("GAME_START");
      stopSample("GAME_LOSE");
      playSample("SCREAM");
      playSample("PIG");
      playSample("CATCH", () => {
        playSample("LISTEN_TO_ME", rulesCallback);
      });
    };

    const playRelease = async () => {
      if (audioElements.SOUNDTRACK)
        audioElements.SOUNDTRACK.gainNode.gain.value = 1;
      playSample("VIRUS_RUNAWAY");
      playSample("RUNAWAY");

      stopSample("SCREAM");
      stopSample("PIG");
      stopSample("CATCH");
      stopSample("LISTEN_TO_ME");
      stopSample("RULES");
    };

    const playBoom = () => {
      stopSample("SCREAM");
      stopSample("PIG");
      playSample("EXPLSOION");
    };

    const playTick = () => {
      playSample("TICK");
    };

    const stopTick = () => {
      stopSample("TICK");
    };

    const playGameStart = () => {
      stopSample("GAME_START");
      stopSample("GAME_LOSE");
      playSample("GAME_RULES");
    };

    const playLose = () => {
      stopSample("WRONG_ANWSER");
      stopSample("RUNAWAY");
      playSample("GAME_LOSE");
    };

    const playWin = () => {
      stopSample("RIGHT_ANSWER");
      playSample("GAME_WIN");
    };
    const playRightQuiz = () => {
      stopSample("QUIZ_INTRO");
      playSample("RIGHT_ANSWER");
    };
    const playWrongQuiz = () => {
      stopSample("QUIZ_INTRO");
      playSample("WRONG_ANWSER");
    };
    const playQuizIntro = () => {
      stopSample("CATCH");
      stopSample("LISTEN_TO_ME");
      stopSample("RULES");
      playSample("QUIZ_INTRO");
    };

    function is_date(obj) {
        return Object.prototype.toString.call(obj) === '[object Date]';
    }

    function get_interpolator(a, b) {
        if (a === b || a !== a)
            return () => a;
        const type = typeof a;
        if (type !== typeof b || Array.isArray(a) !== Array.isArray(b)) {
            throw new Error('Cannot interpolate values of different type');
        }
        if (Array.isArray(a)) {
            const arr = b.map((bi, i) => {
                return get_interpolator(a[i], bi);
            });
            return t => arr.map(fn => fn(t));
        }
        if (type === 'object') {
            if (!a || !b)
                throw new Error('Object cannot be null');
            if (is_date(a) && is_date(b)) {
                a = a.getTime();
                b = b.getTime();
                const delta = b - a;
                return t => new Date(a + t * delta);
            }
            const keys = Object.keys(b);
            const interpolators = {};
            keys.forEach(key => {
                interpolators[key] = get_interpolator(a[key], b[key]);
            });
            return t => {
                const result = {};
                keys.forEach(key => {
                    result[key] = interpolators[key](t);
                });
                return result;
            };
        }
        if (type === 'number') {
            const delta = b - a;
            return t => a + t * delta;
        }
        throw new Error(`Cannot interpolate ${type} values`);
    }
    function tweened(value, defaults = {}) {
        const store = writable(value);
        let task;
        let target_value = value;
        function set(new_value, opts) {
            if (value == null) {
                store.set(value = new_value);
                return Promise.resolve();
            }
            target_value = new_value;
            let previous_task = task;
            let started = false;
            let { delay = 0, duration = 400, easing = identity, interpolate = get_interpolator } = assign(assign({}, defaults), opts);
            const start = now() + delay;
            let fn;
            task = loop(now => {
                if (now < start)
                    return true;
                if (!started) {
                    fn = interpolate(value, new_value);
                    if (typeof duration === 'function')
                        duration = duration(value, new_value);
                    started = true;
                }
                if (previous_task) {
                    previous_task.abort();
                    previous_task = null;
                }
                const elapsed = now - start;
                if (elapsed > duration) {
                    store.set(value = new_value);
                    return false;
                }
                // @ts-ignore
                store.set(value = fn(easing(elapsed / duration)));
                return true;
            });
            return task.promise;
        }
        return {
            set,
            update: (fn, opts) => set(fn(target_value, value), opts),
            subscribe: store.subscribe
        };
    }

    const sendVkScore = (gameTime, vkParams) => {
        fetch("https://quarantinegame.azurewebsites.net/api/score", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            Line: vkParams.line,
            Key: vkParams.key,
            Score: gameTime,
            FirstName: vkParams.firstName,
            LastName: vkParams.lastName,
            Id : vkParams.id,
            Platform: "VK",
            Referer: vkParams.referer
          }),
        })
          .then((res) => res.text())
          .then((res) => console.log(res));
      };

    const sendMetrik = (name, payload) => {
      const consoleMetrik = (id, type, name, payload) => console.log(name, payload);
      const ym = IS_TEST ? consoleMetrik : window.ym || consoleMetrik;
      ym(53183122, "reachGoal", `Virus.${name}`, payload);
    };

    let isPointerDown = false;
    let gameStartTime = null;
    let gameTime = null;

    const vkParams = {};

    const showIntro = writable(true);

    const gameWidth = writable(0);
    const gameHeight = writable(0);

    const gameState = writable(null);
    const gameView = writable(null);
    // export const gameState = writable("loaded");
    // export const gameView = writable(GAME_VIEWS.LOSE);

    const pointer = { x: 0, y: 0 };

    const age = writable("");

    const replayCount = writable(0);
    const startPointer = (e) => {
      showIntro.set(false);
      isPointerDown = true;
      const { clientX: x, clientY: y } = e.touches ? e.touches[0] : e;
      pointer.x = x;
      pointer.y = y;
    };
    const setPointer = (e) => {
      if (!isPointerDown) return;
      const { clientX: x, clientY: y } = e.touches ? e.touches[0] : e;
      pointer.x = x;
      pointer.y = y;
    };
    const resetPointer = () => {
      isPointerDown = false;
      pointer.x = 0;
      pointer.y = 0;
    };

    const isHolding = (() => {
      const store = writable(false);
      return {
        subscribe: store.subscribe,
        update: (f) => {
          store.update((state) => {
            const newValue = f(state);

            if (!get_store_value(gameState) || get_store_value(gameView) !== GAME_VIEWS.GAME)
              return newValue;
            if (state === false && newValue === true) {
              playCatch();
            }
            if (state === true && newValue === false) {
              playRelease();
            }
            return newValue;
          });
        },
      };
    })();
    const immunity = writable(INITIAL_IMMUNITY);

    let immunityTimeoutId = null;
    let prevIsHoldingVirusState = false;
    const resetImmunityTimeout = () => clearTimeout(immunityTimeoutId);
    const updateImmunity = () => {
      const isHoldingVirus = get_store_value(isHolding);
      let immunityValue = get_store_value(immunity);
      const isLostVirus = prevIsHoldingVirusState && !isHoldingVirus;
      immunityValue += isHoldingVirus
        ? ADD_IMMUNITY_VALUE
        : isLostVirus
        ? LOST_SUB_IMMUNITY_VALUE
        : SUB_IMMUNITY_VALUE;

      // immunity.set(immunityValue, { duration: isLostVirus ? 1000 : 1 });
      immunity.update(() => immunityValue);
      if (immunityValue <= 0) {
        resetPointer();
        sendMetrik("ArcadeFail", {
          gameTimeLose: Math.round((Date.now() - gameStartTime) / 100) / 10,
        });
        gameView.set(GAME_VIEWS.LOSE);
      } else if (immunityValue >= 100) {
        playBoom();
        resetPointer();

        gameView.set(GAME_VIEWS.QUIZ_INTRO);
        isHolding.update(() => false);
        playQuizIntro();
      } else {
        immunityTimeoutId = setTimeout(
          updateImmunity,
          TICK_TIMEOUT + isLostVirus ? 100 : 0
        );
      }
      prevIsHoldingVirusState = isHoldingVirus;
    };

    const startImmunityCount = () => {
      immunity.set(INITIAL_IMMUNITY);
      immunityTimeoutId = setTimeout(updateImmunity, TICK_TIMEOUT);
    };

    gameView.subscribe((state) => {
      if (state === GAME_VIEWS.GAME) {
        gameStartTime = Date.now();
        playSample("SOUNDTRACK");
        playGameStart();
        prevIsHoldingVirusState = false;
        isHolding.update(() => false);
        startImmunityCount();
      } else {
        resetImmunityTimeout();
      }
      if (state === GAME_VIEWS.LOSE) {
        playLose();
      }
      if (state === GAME_VIEWS.WIN) {
        if (IS_VK) sendVkScore(gameTime, vkParams);
        playWin();
      }
      if (state === GAME_VIEWS.QUIZ_INTRO) {
        gameTime = Math.round((Date.now() - gameStartTime) / 100) / 10;
        sendMetrik("ArcadeWin", {
          gameTime: gameTime,
        });
      }
    });

    const preloadIndicator = (() => {
      const startTime = Date.now();
      const assetsCount =
        Object.keys(SAMPLES).reduce((memo, key) => memo + SAMPLES[key].length, 0) +
        3; //3 - count of sprites
      let loadedAssestsCount = 0;
      const { subscribe, set } = tweened(0, {
        duration: 500,
        easing: cubicOut,
      });
      return {
        subscribe,
        assetLoaded: () => {
          loadedAssestsCount += 1;
          if (loadedAssestsCount === assetsCount && get_store_value(gameState) === null) {
            sendMetrik("ResoursesLoad", {
              time: Math.round((Date.now() - startTime) / 100) / 10,
            });
            // playSample("GAME_START");
          }
          set(loadedAssestsCount / assetsCount);
        },
        assetNotLoaded: (asset, err) => {
          sendMetrik("ResoursesLoad", {
            failedAsset: asset,
            err,
          });
        },
      };
    })();

    const startWidget = async () => {
      gameView.set(GAME_VIEWS.GAME);
      gameState.set("loaded");
      sendMetrik("Start");
    };

    const submitAge = (event) => {
      event.preventDefault();
      if (!get_store_value(age)) return;

      sendMetrik("AgeSubmit", { age: get_store_value(age) });
      gameView.set(GAME_VIEWS.GAME);
    };

    const proceedToQuiz = () => {
      gameView.set(GAME_VIEWS.QUIZ);
      sendMetrik("GameContinue");
    };

    const replay = () => {
      replayCount.update((count) => count + 1);
      gameView.set(GAME_VIEWS.GAME);
      sendMetrik("GameReplay", { replayCount: get_store_value(replayCount) });
    };

    const close = () => {
      resetPointer();
      resetImmunityTimeout();
      sendMetrik("GameClose", { view: get_store_value(gameView) });
      gameView.set(null);
      gameState.set(null);
      stopAllAudio();
    };

    const onVisibilityChange = () => {
      if (document.visibilityState === "hidden") {
        stopAllAudio();
      }
      if (
        get_store_value(gameView) === GAME_VIEWS.GAME &&
        get_store_value(gameState) &&
        document.visibilityState === "visible"
      ) {
        playSample("SOUNDTRACK");
      }
    };

    const signVkParams = () => {
      const searchParams = new URLSearchParams(window.location.search);
      vkParams.key = searchParams.get("sign");
      vkParams.id = searchParams.get("vk_user_id");
      vkParams.appId = searchParams.get("vk_app_id");
      vkParams.referer = window.location.hash.replace('#','');
      let vkQueryarams = {};
      for (let [key, value] of searchParams) {
        if (key.startsWith("vk_")) vkQueryarams[key] = value;
      }
      const line = Object.keys(vkQueryarams)
        .sort()
        .map((key) => `${key}=${vkQueryarams[key]}`)
        .join("&");
      
      vkParams.line = line;
    };
    const initVk = () => {
      bridge.subscribe((event) => {
        if (event.detail && event.detail.type === "VKWebAppViewHide") close();
      });
      bridge
        .send("VKWebAppInit", {})
        .then(() => {
          return bridge.send("VKWebAppGetUserInfo", {});
        })
        .then((res) => {
          vkParams.firstName = res.first_name;
          vkParams.lastName = res.last_name;
          sendVkScore(0, vkParams);
          sendVkReferer(vkParams);
          if (!res.bdate || res.bdate.length < 10) return;
          const birthday = new Date(res.bdate.split(".").reverse().join("-"));
          const now = new Date();
          const userAge = now.getFullYear() - birthday.getFullYear();
          age.set(userAge);
          sendMetrik("AgeSubmit", { age: userAge });
        }).catch(err=>{
          console.log('VKWebAppInit', err);
        });
      signVkParams();
    };

    /* src/assets/Human.svelte generated by Svelte v3.20.1 */

    const file = "src/assets/Human.svelte";

    function create_fragment(ctx) {
    	let svg;
    	let path0;
    	let path0_style_value;
    	let path1;
    	let path1_style_value;

    	const block = {
    		c: function create() {
    			svg = svg_element("svg");
    			path0 = svg_element("path");
    			path1 = svg_element("path");
    			attr_dev(path0, "d", "M12 96L53.5 86.5L41.5 55V32.5L59 10.5H100.5L117.5 27L122 48L115 71.5L105\n    86.5L148 96V230H117.5V390H45V230H12V96Z");
    			attr_dev(path0, "style", path0_style_value = `fill: #64c816; opacity:${/*opacity*/ ctx[0]}`);
    			add_location(path0, file, 6, 2, 77);
    			attr_dev(path1, "d", "M120 80H115.68C123.312 71.496 128 60.304 128 48C128 21.528 106.472 0 80\n    0C53.528 0 32 21.528 32 48C32 60.304 36.688 71.496 44.32 80H40C17.944 80 0\n    97.944 0 120V232C0 236.424 3.584 240 8 240H32V392C32 396.424 35.584 400 40\n    400H120C124.416 400 128 396.424 128 392V240H152C156.416 240 160 236.424 160\n    232V120C160 97.944 142.056 80 120 80ZM144 224H128V120H120H112V384H88V240C88\n    235.576 84.416 232 80 232C75.584 232 72 235.576 72\n    240V384H48V120H40H32V224H16V120C16 106.768 26.768 96 40 96H120C133.232 96\n    144 106.768 144 120V224ZM48 48C48 30.352 62.352 16 80 16C97.648 16 112\n    30.352 112 48C112 65.648 97.648 80 80 80C62.352 80 48 65.648 48 48Z");
    			attr_dev(path1, "style", path1_style_value = `fill: ${/*opacity*/ ctx[0] >= 1 ? "green" : "var(--color1)"}`);
    			add_location(path1, file, 10, 2, 261);
    			attr_dev(svg, "viewBox", "0 0 160 400");
    			add_location(svg, file, 4, 0, 46);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, svg, anchor);
    			append_dev(svg, path0);
    			append_dev(svg, path1);
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*opacity*/ 1 && path0_style_value !== (path0_style_value = `fill: #64c816; opacity:${/*opacity*/ ctx[0]}`)) {
    				attr_dev(path0, "style", path0_style_value);
    			}

    			if (dirty & /*opacity*/ 1 && path1_style_value !== (path1_style_value = `fill: ${/*opacity*/ ctx[0] >= 1 ? "green" : "var(--color1)"}`)) {
    				attr_dev(path1, "style", path1_style_value);
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(svg);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance($$self, $$props, $$invalidate) {
    	let { opacity = 1 } = $$props;
    	const writable_props = ["opacity"];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<Human> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("Human", $$slots, []);

    	$$self.$set = $$props => {
    		if ("opacity" in $$props) $$invalidate(0, opacity = $$props.opacity);
    	};

    	$$self.$capture_state = () => ({ opacity });

    	$$self.$inject_state = $$props => {
    		if ("opacity" in $$props) $$invalidate(0, opacity = $$props.opacity);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [opacity];
    }

    class Human extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance, create_fragment, safe_not_equal, { opacity: 0 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Human",
    			options,
    			id: create_fragment.name
    		});
    	}

    	get opacity() {
    		throw new Error("<Human>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set opacity(value) {
    		throw new Error("<Human>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/components/Immunity.svelte generated by Svelte v3.20.1 */
    const file$1 = "src/components/Immunity.svelte";

    function get_each_context(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[2] = list[i];
    	child_ctx[4] = i;
    	return child_ctx;
    }

    // (10:2) {#each arr as item, i}
    function create_each_block(ctx) {
    	let div;
    	let t;
    	let current;

    	const human = new Human({
    			props: {
    				opacity: /*$immunity*/ ctx[0] / 10 - /*i*/ ctx[4]
    			},
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			div = element("div");
    			create_component(human.$$.fragment);
    			t = space();
    			attr_dev(div, "class", "humanWrapper svelte-19x5ite");
    			add_location(div, file$1, 10, 4, 264);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			mount_component(human, div, null);
    			append_dev(div, t);
    			current = true;
    		},
    		p: function update(ctx, dirty) {
    			const human_changes = {};
    			if (dirty & /*$immunity*/ 1) human_changes.opacity = /*$immunity*/ ctx[0] / 10 - /*i*/ ctx[4];
    			human.$set(human_changes);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(human.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(human.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			destroy_component(human);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block.name,
    		type: "each",
    		source: "(10:2) {#each arr as item, i}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$1(ctx) {
    	let div;
    	let current;
    	let each_value = /*arr*/ ctx[1];
    	validate_each_argument(each_value);
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block(get_each_context(ctx, each_value, i));
    	}

    	const out = i => transition_out(each_blocks[i], 1, 1, () => {
    		each_blocks[i] = null;
    	});

    	const block = {
    		c: function create() {
    			div = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr_dev(div, "class", "immunity svelte-19x5ite");
    			toggle_class(div, "isWidget", IS_VK);
    			add_location(div, file$1, 7, 0, 188);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div, null);
    			}

    			current = true;
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*$immunity*/ 1) {
    				each_value = /*arr*/ ctx[1];
    				validate_each_argument(each_value);
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    						transition_in(each_blocks[i], 1);
    					} else {
    						each_blocks[i] = create_each_block(child_ctx);
    						each_blocks[i].c();
    						transition_in(each_blocks[i], 1);
    						each_blocks[i].m(div, null);
    					}
    				}

    				group_outros();

    				for (i = each_value.length; i < each_blocks.length; i += 1) {
    					out(i);
    				}

    				check_outros();
    			}

    			if (dirty & /*IS_VK*/ 0) {
    				toggle_class(div, "isWidget", IS_VK);
    			}
    		},
    		i: function intro(local) {
    			if (current) return;

    			for (let i = 0; i < each_value.length; i += 1) {
    				transition_in(each_blocks[i]);
    			}

    			current = true;
    		},
    		o: function outro(local) {
    			each_blocks = each_blocks.filter(Boolean);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				transition_out(each_blocks[i]);
    			}

    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			destroy_each(each_blocks, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$1.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$1($$self, $$props, $$invalidate) {
    	let $immunity;
    	validate_store(immunity, "immunity");
    	component_subscribe($$self, immunity, $$value => $$invalidate(0, $immunity = $$value));
    	const arr = new Array(10).fill(0);
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<Immunity> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("Immunity", $$slots, []);
    	$$self.$capture_state = () => ({ immunity, IS_VK, Human, arr, $immunity });
    	return [$immunity, arr];
    }

    class Immunity extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$1, create_fragment$1, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Immunity",
    			options,
    			id: create_fragment$1.name
    		});
    	}
    }

    /* src/views/Hud.svelte generated by Svelte v3.20.1 */
    const file$2 = "src/views/Hud.svelte";

    // (15:0) {#if $opacity > 0}
    function create_if_block(ctx) {
    	let div1;
    	let div0;
    	let t_value = text$1("PRESS_AND_HOLD") + "";
    	let t;
    	let div0_style_value;
    	let dispose;

    	const block = {
    		c: function create() {
    			div1 = element("div");
    			div0 = element("div");
    			t = text(t_value);
    			attr_dev(div0, "class", "pressAndHold svelte-r59qj5");
    			attr_dev(div0, "style", div0_style_value = `opacity: ${/*$opacity*/ ctx[0]}`);
    			add_location(div0, file$2, 16, 4, 442);
    			attr_dev(div1, "class", "immunity svelte-r59qj5");
    			add_location(div1, file$2, 15, 2, 415);
    		},
    		m: function mount(target, anchor, remount) {
    			insert_dev(target, div1, anchor);
    			append_dev(div1, div0);
    			append_dev(div0, t);
    			if (remount) run_all(dispose);

    			dispose = [
    				listen_dev(div0, "mousedown", /*forceOpacity*/ ctx[2], false, false, false),
    				listen_dev(div0, "touchstart", /*forceOpacity*/ ctx[2], { passive: true }, false, false)
    			];
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*$opacity*/ 1 && div0_style_value !== (div0_style_value = `opacity: ${/*$opacity*/ ctx[0]}`)) {
    				attr_dev(div0, "style", div0_style_value);
    			}
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div1);
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block.name,
    		type: "if",
    		source: "(15:0) {#if $opacity > 0}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$2(ctx) {
    	let t;
    	let if_block_anchor;
    	let current;
    	const immunity_1 = new Immunity({ $$inline: true });
    	let if_block = /*$opacity*/ ctx[0] > 0 && create_if_block(ctx);

    	const block = {
    		c: function create() {
    			create_component(immunity_1.$$.fragment);
    			t = space();
    			if (if_block) if_block.c();
    			if_block_anchor = empty();
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			mount_component(immunity_1, target, anchor);
    			insert_dev(target, t, anchor);
    			if (if_block) if_block.m(target, anchor);
    			insert_dev(target, if_block_anchor, anchor);
    			current = true;
    		},
    		p: function update(ctx, [dirty]) {
    			if (/*$opacity*/ ctx[0] > 0) {
    				if (if_block) {
    					if_block.p(ctx, dirty);
    				} else {
    					if_block = create_if_block(ctx);
    					if_block.c();
    					if_block.m(if_block_anchor.parentNode, if_block_anchor);
    				}
    			} else if (if_block) {
    				if_block.d(1);
    				if_block = null;
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(immunity_1.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(immunity_1.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(immunity_1, detaching);
    			if (detaching) detach_dev(t);
    			if (if_block) if_block.d(detaching);
    			if (detaching) detach_dev(if_block_anchor);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$2.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$2($$self, $$props, $$invalidate) {
    	let $opacity;
    	const opacity = tweened(1, { duration: 3000 });
    	validate_store(opacity, "opacity");
    	component_subscribe($$self, opacity, value => $$invalidate(0, $opacity = value));
    	opacity.set(0);
    	const forceOpacity = () => opacity.set(0, { duration: 200 });
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<Hud> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("Hud", $$slots, []);

    	$$self.$capture_state = () => ({
    		Immunity,
    		fade,
    		tweened,
    		immunity,
    		text: text$1,
    		opacity,
    		forceOpacity,
    		$opacity
    	});

    	return [$opacity, opacity, forceOpacity];
    }

    class Hud extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$2, create_fragment$2, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Hud",
    			options,
    			id: create_fragment$2.name
    		});
    	}
    }

    /* src/views/Intro.svelte generated by Svelte v3.20.1 */

    function create_fragment$3(ctx) {
    	const block = {
    		c: noop,
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: noop,
    		p: noop,
    		i: noop,
    		o: noop,
    		d: noop
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$3.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$3($$self, $$props, $$invalidate) {
    	setTimeout(() => gameView.set(GAME_VIEWS.AGE), 500);
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<Intro> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("Intro", $$slots, []);
    	$$self.$capture_state = () => ({ gameView, GAME_VIEWS });
    	return [];
    }

    class Intro extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$3, create_fragment$3, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Intro",
    			options,
    			id: create_fragment$3.name
    		});
    	}
    }

    /* src/views/Age.svelte generated by Svelte v3.20.1 */
    const file$3 = "src/views/Age.svelte";

    function create_fragment$4(ctx) {
    	let div2;
    	let div0;
    	let t1;
    	let div1;
    	let t4;
    	let form;
    	let input;
    	let input_updating = false;
    	let t5;
    	let div3;
    	let t6;
    	let div4;
    	let button;
    	let t7_value = text$1("SUBMIT") + "";
    	let t7;
    	let button_disabled_value;
    	let dispose;

    	function input_input_handler() {
    		input_updating = true;
    		/*input_input_handler*/ ctx[6].call(input);
    	}

    	const block = {
    		c: function create() {
    			div2 = element("div");
    			div0 = element("div");
    			div0.textContent = `${text$1("ENTER_YOUR_AGE")}`;
    			t1 = space();
    			div1 = element("div");

    			div1.textContent = `${text$1("MAY_AGE_IS")}:
  `;

    			t4 = space();
    			form = element("form");
    			input = element("input");
    			t5 = space();
    			div3 = element("div");
    			t6 = space();
    			div4 = element("div");
    			button = element("button");
    			t7 = text(t7_value);
    			attr_dev(div0, "class", "header svelte-9jsg8w");
    			add_location(div0, file$3, 15, 2, 457);
    			attr_dev(div1, "class", "ageTitle svelte-9jsg8w");
    			toggle_class(div1, "isMobileSafari", /*isMobileSafari*/ ctx[2] && /*focus*/ ctx[1]);
    			add_location(div1, file$3, 16, 2, 510);
    			attr_dev(input, "type", "number");
    			attr_dev(input, "class", "age svelte-9jsg8w");
    			add_location(input, file$3, 20, 4, 663);
    			attr_dev(form, "class", "form");
    			add_location(form, file$3, 19, 2, 617);
    			attr_dev(div2, "class", "message svelte-9jsg8w");
    			toggle_class(div2, "isWidget", IS_MOBILE_VK);
    			add_location(div2, file$3, 14, 0, 403);
    			attr_dev(div3, "class", "ageContainer");
    			add_location(div3, file$3, 30, 0, 856);
    			attr_dev(button, "class", "button svelte-9jsg8w");
    			button.disabled = button_disabled_value = !/*$age*/ ctx[3];
    			add_location(button, file$3, 34, 2, 960);
    			attr_dev(div4, "class", "buttons svelte-9jsg8w");
    			toggle_class(div4, "hideWhenFocus", /*focus*/ ctx[1] && /*$gameHeight*/ ctx[4] < 400);
    			add_location(div4, file$3, 32, 0, 886);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor, remount) {
    			insert_dev(target, div2, anchor);
    			append_dev(div2, div0);
    			append_dev(div2, t1);
    			append_dev(div2, div1);
    			append_dev(div2, t4);
    			append_dev(div2, form);
    			append_dev(form, input);
    			/*input_binding*/ ctx[5](input);
    			set_input_value(input, /*$age*/ ctx[3]);
    			insert_dev(target, t5, anchor);
    			insert_dev(target, div3, anchor);
    			insert_dev(target, t6, anchor);
    			insert_dev(target, div4, anchor);
    			append_dev(div4, button);
    			append_dev(button, t7);
    			if (remount) run_all(dispose);

    			dispose = [
    				listen_dev(input, "input", input_input_handler),
    				listen_dev(input, "focus", /*focus_handler*/ ctx[7], false, false, false),
    				listen_dev(input, "blur", /*blur_handler*/ ctx[8], false, false, false),
    				listen_dev(form, "submit", submitAge, false, false, false),
    				listen_dev(button, "click", submitAge, false, false, false)
    			];
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*isMobileSafari, focus*/ 6) {
    				toggle_class(div1, "isMobileSafari", /*isMobileSafari*/ ctx[2] && /*focus*/ ctx[1]);
    			}

    			if (!input_updating && dirty & /*$age*/ 8) {
    				set_input_value(input, /*$age*/ ctx[3]);
    			}

    			input_updating = false;

    			if (dirty & /*IS_MOBILE_VK*/ 0) {
    				toggle_class(div2, "isWidget", IS_MOBILE_VK);
    			}

    			if (dirty & /*$age*/ 8 && button_disabled_value !== (button_disabled_value = !/*$age*/ ctx[3])) {
    				prop_dev(button, "disabled", button_disabled_value);
    			}

    			if (dirty & /*focus, $gameHeight*/ 18) {
    				toggle_class(div4, "hideWhenFocus", /*focus*/ ctx[1] && /*$gameHeight*/ ctx[4] < 400);
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div2);
    			/*input_binding*/ ctx[5](null);
    			if (detaching) detach_dev(t5);
    			if (detaching) detach_dev(div3);
    			if (detaching) detach_dev(t6);
    			if (detaching) detach_dev(div4);
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$4.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$4($$self, $$props, $$invalidate) {
    	let $age;
    	let $gameHeight;
    	validate_store(age, "age");
    	component_subscribe($$self, age, $$value => $$invalidate(3, $age = $$value));
    	validate_store(gameHeight, "gameHeight");
    	component_subscribe($$self, gameHeight, $$value => $$invalidate(4, $gameHeight = $$value));
    	let inputRef = null;

    	onMount(() => {
    		setTimeout(() => inputRef.focus(), 100);
    	});

    	let focus = true;
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<Age> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("Age", $$slots, []);

    	function input_binding($$value) {
    		binding_callbacks[$$value ? "unshift" : "push"](() => {
    			$$invalidate(0, inputRef = $$value);
    		});
    	}

    	function input_input_handler() {
    		$age = to_number(this.value);
    		age.set($age);
    	}

    	const focus_handler = () => $$invalidate(1, focus = true);
    	const blur_handler = () => $$invalidate(1, focus = false);

    	$$self.$capture_state = () => ({
    		onMount,
    		age,
    		submitAge,
    		gameHeight,
    		GAME_VIEWS,
    		IS_MOBILE_VK,
    		text: text$1,
    		inputRef,
    		focus,
    		isMobileSafari,
    		$age,
    		$gameHeight
    	});

    	$$self.$inject_state = $$props => {
    		if ("inputRef" in $$props) $$invalidate(0, inputRef = $$props.inputRef);
    		if ("focus" in $$props) $$invalidate(1, focus = $$props.focus);
    		if ("isMobileSafari" in $$props) $$invalidate(2, isMobileSafari = $$props.isMobileSafari);
    	};

    	let isMobileSafari;

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	 $$invalidate(2, isMobileSafari = navigator.userAgent.match(/(iPod|iPhone|iPad)/));

    	return [
    		inputRef,
    		focus,
    		isMobileSafari,
    		$age,
    		$gameHeight,
    		input_binding,
    		input_input_handler,
    		focus_handler,
    		blur_handler
    	];
    }

    class Age extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$4, create_fragment$4, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Age",
    			options,
    			id: create_fragment$4.name
    		});
    	}
    }

    /* src/assets/Vk.svelte generated by Svelte v3.20.1 */

    const file$4 = "src/assets/Vk.svelte";

    function create_fragment$5(ctx) {
    	let svg;
    	let path;

    	const block = {
    		c: function create() {
    			svg = svg_element("svg");
    			path = svg_element("path");
    			attr_dev(path, "d", "m409.898438 153.902344 101.796874-153.902344h-156.117187l-2.914063\n    11.292969c-7.480468 28.992187-21.519531 68.285156-45.914062\n    90.839843-3.015625 2.785157-5.871094 5.023438-8.5\n    6.816407v-108.949219h-135.972656v30.101562h12.703125v68.222657c-15.242188-13.40625-34.394531-38.988281-39.410157-84.90625l-1.464843-13.417969h-134.105469l4.605469\n    18.65625c16.558593 67.09375 41.398437 122.058594 73.835937 163.367188\n    26.480469 33.726562 58.019532 58.488281 93.738282 73.59375 31.898437\n    13.492187 61.269531 16.800781 82.402343 16.800781 19.4375 0\n    31.902344-2.800781 32.949219-3.042969l11.464844-2.671875.59375-41.351563\n    51.058594 44.417969h161.355468zm-139.484376 6.175781-1.164062\n    81.546875c-16.808594 1.496094-50.109375\n    1.554688-86.859375-14.378906-30.691406-13.308594-57.953125-35.101563-81.027344-64.777344-26.425781-33.984375-47.449219-78.449219-62.613281-132.367188h68.777344c14.886718\n    86.335938 74.78125 108.675782 77.449218 109.625l20.105469\n    7.171876v-116.796876h63.070313v116.945313l15.835937-.828125c1.925781-.101562\n    19.414063-1.496094 40.066407-19.195312 23.464843-20.113282\n    41.828124-52.683594 54.664062-96.921876h76.976562l-83.617187 126.410157\n    73.277344 83.152343h-83.449219zm0 0");
    			attr_dev(path, "data-original", "#000000");
    			attr_dev(path, "class", "active-path svelte-1jd7m96");
    			add_location(path, file$4, 2, 2, 67);
    			attr_dev(svg, "height", "32px");
    			attr_dev(svg, "viewBox", "0 -120 512.00201 512");
    			attr_dev(svg, "width", "32px");
    			add_location(svg, file$4, 0, 0, 0);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, svg, anchor);
    			append_dev(svg, path);
    		},
    		p: noop,
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(svg);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$5.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$5($$self, $$props) {
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<Vk> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("Vk", $$slots, []);
    	return [];
    }

    class Vk extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$5, create_fragment$5, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Vk",
    			options,
    			id: create_fragment$5.name
    		});
    	}
    }

    /* src/assets/Facebook.svelte generated by Svelte v3.20.1 */

    const file$5 = "src/assets/Facebook.svelte";

    function create_fragment$6(ctx) {
    	let svg;
    	let path;

    	const block = {
    		c: function create() {
    			svg = svg_element("svg");
    			path = svg_element("path");
    			attr_dev(path, "d", "m75 512h167v-182h-60v-60h60v-75c0-41.355469 33.644531-75\n    75-75h75v60h-60c-16.542969 0-30 13.457031-30 30v60h87.292969l-10\n    60h-77.292969v182h135c41.355469 0 75-33.644531\n    75-75v-362c0-41.355469-33.644531-75-75-75h-362c-41.355469 0-75 33.644531-75\n    75v362c0 41.355469 33.644531 75 75 75zm-45-437c0-24.8125 20.1875-45\n    45-45h362c24.8125 0 45 20.1875 45 45v362c0 24.8125-20.1875 45-45\n    45h-105v-122h72.707031l20-120h-92.707031v-30h90v-120h-105c-57.898438 0-105\n    47.101562-105 105v45h-60v120h60v122h-137c-24.8125 0-45-20.1875-45-45zm0 0");
    			attr_dev(path, "data-original", "#000000");
    			attr_dev(path, "class", "active-path svelte-aejmer");
    			add_location(path, file$5, 1, 2, 57);
    			attr_dev(svg, "viewBox", "0 0 512 512");
    			attr_dev(svg, "width", "32px");
    			attr_dev(svg, "height", "32px");
    			add_location(svg, file$5, 0, 0, 0);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, svg, anchor);
    			append_dev(svg, path);
    		},
    		p: noop,
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(svg);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$6.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$6($$self, $$props) {
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<Facebook> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("Facebook", $$slots, []);
    	return [];
    }

    class Facebook extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$6, create_fragment$6, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Facebook",
    			options,
    			id: create_fragment$6.name
    		});
    	}
    }

    /* src/assets/Twitter.svelte generated by Svelte v3.20.1 */

    const file$6 = "src/assets/Twitter.svelte";

    function create_fragment$7(ctx) {
    	let svg;
    	let path;

    	const block = {
    		c: function create() {
    			svg = svg_element("svg");
    			path = svg_element("path");
    			attr_dev(path, "d", "m512 55.964844c-32.207031 1.484375-31.503906 1.363281-35.144531\n    1.667968l19.074219-54.472656s-59.539063 21.902344-74.632813\n    25.820313c-39.640625-35.628907-98.5625-37.203125-140.6875-11.3125-34.496094\n    21.207031-53.011719 57.625-46.835937\n    100.191406-67.136719-9.316406-123.703126-41.140625-168.363282-94.789063l-14.125-16.964843-10.554687\n    19.382812c-13.339844 24.492188-17.769531 52.496094-12.476563 78.851563\n    2.171875 10.8125 5.863282 21.125 10.976563\n    30.78125l-12.117188-4.695313-1.4375 20.246094c-1.457031 20.566406 5.390625\n    44.574219 18.320313 64.214844 3.640625 5.53125 8.328125 11.605469 14.269531\n    17.597656l-6.261719-.960937 7.640625 23.199218c10.042969 30.480469 30.902344\n    54.0625 57.972657 67.171875-27.035157 11.472657-48.875 18.792969-84.773438\n    30.601563l-32.84375 10.796875 30.335938 16.585937c11.566406 6.324219 52.4375\n    27.445313 92.820312 33.78125 89.765625 14.078125 190.832031 2.613282\n    258.871094-58.664062 57.308594-51.613282 76.113281-125.03125\n    72.207031-201.433594-.589844-11.566406 2.578125-22.605469 8.921875-31.078125\n    12.707031-16.964844 48.765625-66.40625 48.84375-66.519531zm-72.832031\n    48.550781c-10.535157 14.066406-15.8125 32.03125-14.867188 50.578125 3.941407\n    77.066406-17.027343 136.832031-62.328125 177.628906-52.917968\n    47.660156-138.273437 66.367188-234.171875\n    51.324219-17.367187-2.722656-35.316406-8.820313-50.171875-14.910156\n    30.097656-10.355469 53.339844-19.585938\n    90.875-37.351563l52.398438-24.800781-57.851563-3.703125c-27.710937-1.773438-50.785156-15.203125-64.96875-37.007812\n    7.53125-.4375 14.792969-1.65625\n    22.023438-3.671876l55.175781-15.367187-55.636719-13.625c-27.035156-6.621094-42.445312-22.796875-50.613281-35.203125-5.363281-8.152344-8.867188-16.503906-10.96875-24.203125\n    5.578125 1.496094 12.082031 2.5625 22.570312 3.601563l51.496094\n    5.09375-40.800781-31.828126c-29.398437-22.929687-41.179687-57.378906-32.542969-90.496093\n    91.75 95.164062 199.476563 88.011719 210.320313\n    90.527343-2.386719-23.183593-2.449219-23.238281-3.074219-25.445312-13.886719-49.089844\n    16.546875-74.015625 30.273438-82.453125 28.671874-17.621094\n    74.183593-20.277344 105.707031 8.753906 6.808593 6.265625 16.015625 8.730469\n    24.632812 6.589844 7.734375-1.921875 14.082031-3.957031\n    20.296875-6.171875l-12.9375 36.945312 16.515625.011719c-3.117187\n    4.179688-6.855469 9.183594-11.351562 15.183594zm0 0");
    			attr_dev(path, "data-original", "#000000");
    			attr_dev(path, "class", "active-path svelte-aejmer");
    			add_location(path, file$6, 1, 2, 65);
    			attr_dev(svg, "viewBox", "0 -47 512.00004 512");
    			attr_dev(svg, "width", "32px");
    			attr_dev(svg, "height", "32px");
    			add_location(svg, file$6, 0, 0, 0);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, svg, anchor);
    			append_dev(svg, path);
    		},
    		p: noop,
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(svg);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$7.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$7($$self, $$props) {
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<Twitter> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("Twitter", $$slots, []);
    	return [];
    }

    class Twitter extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$7, create_fragment$7, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Twitter",
    			options,
    			id: create_fragment$7.name
    		});
    	}
    }

    /* src/assets/Ok.svelte generated by Svelte v3.20.1 */

    const file$7 = "src/assets/Ok.svelte";

    function create_fragment$8(ctx) {
    	let svg;
    	let path0;
    	let path1;
    	let path2;

    	const block = {
    		c: function create() {
    			svg = svg_element("svg");
    			path0 = svg_element("path");
    			path1 = svg_element("path");
    			path2 = svg_element("path");
    			attr_dev(path0, "d", "m19.223 12.484c-.606-1.183-2.081-1.632-3.525-.497-.014.011-1.444\n    1.108-3.695\n    1.108s-3.681-1.098-3.695-1.108c-1.438-1.136-2.925-.691-3.527.5-.002.003-.003.006-.005.01-.895\n    1.831.583 2.785 1.772 3.552.735.473 1.675.83 2.804 1.066l-3.205 3.212c-1.935\n    1.931 1.097 4.978 3.052 3.067l2.814-2.828c1.083 1.085 2.092 2.098 2.82 2.839\n    1.93 1.895 4.982-1.099 3.058-3.074-2.295-2.293-2.789-2.788-3.223-3.215\n    2.396-.513 5.729-2.194 4.555-4.632zm-6.025 3.355c-.518-.249-1.206.697-.64\n    1.285l.006.006c.001.001.003.003.004.004.002.002.003.004.005.005.288.287\n    4.163 4.158 4.249 4.245.538.553-.41\n    1.467-.931.959-.93-.946-2.405-2.423-3.35-3.37-.292-.293-.769-.295-1.062\n    0l-3.338\n    3.354c-.529.517-1.472-.406-.936-.941l4.244-4.252c.002-.002.004-.004.005-.006.527-.518-.005-1.478-.625-1.295-1.477-.173-2.674-.534-3.471-1.046-1.54-.995-1.463-1.17-1.24-1.627.648-1.241\n    1.796 1.435 5.883 1.435 2.799 0 4.555-1.375 4.624-1.43.456-.358.991-.525\n    1.254-.014.223.463.306.636-1.238 1.639-1.054.671-2.544.942-3.443 1.049z");
    			attr_dev(path0, "data-original", "#000000");
    			attr_dev(path0, "class", "active-path svelte-aejmer");
    			add_location(path0, file$7, 2, 2, 56);
    			attr_dev(path1, "d", "m11.999 9.997c1.954 0 3.543-1.592 3.543-3.548\n    0-1.964-1.589-3.562-3.543-3.562-1.953 0-3.542 1.598-3.542 3.562-.001 1.956\n    1.588 3.548 3.542 3.548zm0-5.61c1.126 0 2.043.925 2.043 2.062 0 1.129-.917\n    2.048-2.043 2.048s-2.043-.919-2.043-2.049c0-1.136.916-2.061 2.043-2.061z");
    			attr_dev(path1, "data-original", "#000000");
    			attr_dev(path1, "class", "active-path svelte-aejmer");
    			add_location(path1, file$7, 20, 2, 1172);
    			attr_dev(path2, "d", "m11.999 12.883c3.549 0 6.437-2.887\n    6.437-6.435-.001-3.555-2.888-6.448-6.437-6.448s-6.437 2.893-6.437 6.448c0\n    3.548 2.887 6.435 6.437 6.435zm0-11.383c2.722 0 4.937 2.22 4.937 4.948 0\n    2.721-2.214 4.935-4.937 4.935s-4.937-2.214-4.937-4.935c0-2.728 2.214-4.948\n    4.937-4.948z");
    			attr_dev(path2, "data-original", "#000000");
    			attr_dev(path2, "class", "active-path svelte-aejmer");
    			add_location(path2, file$7, 28, 2, 1529);
    			attr_dev(svg, "height", "32px");
    			attr_dev(svg, "viewBox", "0 0 24 24");
    			attr_dev(svg, "width", "32px");
    			add_location(svg, file$7, 0, 0, 0);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, svg, anchor);
    			append_dev(svg, path0);
    			append_dev(svg, path1);
    			append_dev(svg, path2);
    		},
    		p: noop,
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(svg);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$8.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$8($$self, $$props) {
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<Ok> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("Ok", $$slots, []);
    	return [];
    }

    class Ok extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$8, create_fragment$8, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Ok",
    			options,
    			id: create_fragment$8.name
    		});
    	}
    }

    /* src/components/SocialShare.svelte generated by Svelte v3.20.1 */
    const file$8 = "src/components/SocialShare.svelte";

    // (21:2) {:else}
    function create_else_block(ctx) {
    	let div0;
    	let t1;
    	let div1;
    	let a0;
    	let a0_href_value;
    	let t2;
    	let a1;
    	let a1_href_value;
    	let t3;
    	let a2;
    	let a2_href_value;
    	let t4;
    	let a3;
    	let a3_href_value;
    	let current;
    	let dispose;
    	const vk = new Vk({ $$inline: true });
    	const ok = new Ok({ $$inline: true });
    	const facebook = new Facebook({ $$inline: true });
    	const twitter = new Twitter({ $$inline: true });

    	const block = {
    		c: function create() {
    			div0 = element("div");
    			div0.textContent = `${text$1("SHARE")}`;
    			t1 = space();
    			div1 = element("div");
    			a0 = element("a");
    			create_component(vk.$$.fragment);
    			t2 = space();
    			a1 = element("a");
    			create_component(ok.$$.fragment);
    			t3 = space();
    			a2 = element("a");
    			create_component(facebook.$$.fragment);
    			t4 = space();
    			a3 = element("a");
    			create_component(twitter.$$.fragment);
    			attr_dev(div0, "class", "text svelte-1bhdob6");
    			add_location(div0, file$8, 21, 4, 769);
    			attr_dev(a0, "href", a0_href_value = "https://vk.com/share.php?url=$" + /*url*/ ctx[0]);
    			attr_dev(a0, "target", "_blank");
    			attr_dev(a0, "rel", "noopener");
    			attr_dev(a0, "aria-label", "Share on vkontakte");
    			add_location(a0, file$8, 23, 6, 847);
    			attr_dev(a1, "href", a1_href_value = `https://connect.ok.ru/offer?url=${/*url*/ ctx[0]}&title=${encodeURIComponent(text$1("TWITTER_TEXT"))}`);
    			attr_dev(a1, "target", "_blank");
    			attr_dev(a1, "rel", "noopener");
    			attr_dev(a1, "aria-label", "Share on ok");
    			add_location(a1, file$8, 31, 6, 1090);
    			attr_dev(a2, "href", a2_href_value = `https://facebook.com/sharer/sharer.php?u=${/*url*/ ctx[0]}`);
    			attr_dev(a2, "target", "_blank");
    			attr_dev(a2, "rel", "noopener");
    			attr_dev(a2, "aria-label", "Share on Facebook");
    			add_location(a2, file$8, 39, 6, 1381);
    			attr_dev(a3, "class", "resp-sharing-button__link");
    			attr_dev(a3, "href", a3_href_value = `https://twitter.com/intent/tweet/?text=${encodeURIComponent(text$1("TWITTER_TEXT"))}&url=${/*url*/ ctx[0]}`);
    			attr_dev(a3, "target", "_blank");
    			attr_dev(a3, "rel", "noopener");
    			attr_dev(a3, "aria-label", "Share on Twitter");
    			add_location(a3, file$8, 48, 6, 1650);
    			attr_dev(div1, "class", "socialButtons svelte-1bhdob6");
    			add_location(div1, file$8, 22, 4, 813);
    		},
    		m: function mount(target, anchor, remount) {
    			insert_dev(target, div0, anchor);
    			insert_dev(target, t1, anchor);
    			insert_dev(target, div1, anchor);
    			append_dev(div1, a0);
    			mount_component(vk, a0, null);
    			append_dev(div1, t2);
    			append_dev(div1, a1);
    			mount_component(ok, a1, null);
    			append_dev(div1, t3);
    			append_dev(div1, a2);
    			mount_component(facebook, a2, null);
    			append_dev(div1, t4);
    			append_dev(div1, a3);
    			mount_component(twitter, a3, null);
    			current = true;
    			if (remount) run_all(dispose);

    			dispose = [
    				listen_dev(a0, "click", /*click_handler*/ ctx[2], false, false, false),
    				listen_dev(a1, "click", /*click_handler_1*/ ctx[3], false, false, false),
    				listen_dev(a2, "click", /*click_handler_2*/ ctx[4], false, false, false),
    				listen_dev(a3, "click", /*click_handler_3*/ ctx[5], false, false, false)
    			];
    		},
    		p: noop,
    		i: function intro(local) {
    			if (current) return;
    			transition_in(vk.$$.fragment, local);
    			transition_in(ok.$$.fragment, local);
    			transition_in(facebook.$$.fragment, local);
    			transition_in(twitter.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(vk.$$.fragment, local);
    			transition_out(ok.$$.fragment, local);
    			transition_out(facebook.$$.fragment, local);
    			transition_out(twitter.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div0);
    			if (detaching) detach_dev(t1);
    			if (detaching) detach_dev(div1);
    			destroy_component(vk);
    			destroy_component(ok);
    			destroy_component(facebook);
    			destroy_component(twitter);
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_else_block.name,
    		type: "else",
    		source: "(21:2) {:else}",
    		ctx
    	});

    	return block;
    }

    // (19:2) {#if IS_VK}
    function create_if_block$1(ctx) {
    	let button;
    	let dispose;

    	const block = {
    		c: function create() {
    			button = element("button");
    			button.textContent = `${text$1("SHARE")}`;
    			attr_dev(button, "class", "shareVkButton svelte-1bhdob6");
    			add_location(button, file$8, 19, 4, 681);
    		},
    		m: function mount(target, anchor, remount) {
    			insert_dev(target, button, anchor);
    			if (remount) dispose();
    			dispose = listen_dev(button, "click", /*shareVk*/ ctx[1], false, false, false);
    		},
    		p: noop,
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(button);
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$1.name,
    		type: "if",
    		source: "(19:2) {#if IS_VK}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$9(ctx) {
    	let div;
    	let current_block_type_index;
    	let if_block;
    	let current;
    	const if_block_creators = [create_if_block$1, create_else_block];
    	const if_blocks = [];

    	function select_block_type(ctx, dirty) {
    		if (IS_VK) return 0;
    		return 1;
    	}

    	current_block_type_index = select_block_type();
    	if_block = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);

    	const block = {
    		c: function create() {
    			div = element("div");
    			if_block.c();
    			attr_dev(div, "class", "social svelte-1bhdob6");
    			add_location(div, file$8, 17, 0, 642);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			if_blocks[current_block_type_index].m(div, null);
    			current = true;
    		},
    		p: function update(ctx, [dirty]) {
    			if_block.p(ctx, dirty);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(if_block);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(if_block);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			if_blocks[current_block_type_index].d();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$9.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$9($$self, $$props, $$invalidate) {
    	const url = encodeURIComponent(window.location.href);

    	const shareVk = () => {
    		sendMetrik("SocialShare", { target: "vk" });

    		bridge.send("VKWebAppShare", {
    			"link": `https://vk.com/app${vkParams.appId}#${vkParams.id}`
    		});
    	};

    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<SocialShare> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("SocialShare", $$slots, []);
    	const click_handler = () => sendMetrik("SocialShare", { target: "vk" });
    	const click_handler_1 = () => sendMetrik("SocialShare", { target: "ok" });
    	const click_handler_2 = () => sendMetrik("SocialShare", { target: "facebook" });
    	const click_handler_3 = () => sendMetrik("SocialShare", { target: "twitter" });

    	$$self.$capture_state = () => ({
    		sendMetrik,
    		vkParams,
    		bridge,
    		IS_VK,
    		text: text$1,
    		Vk,
    		Facebook,
    		Twitter,
    		Ok,
    		url,
    		shareVk
    	});

    	return [url, shareVk, click_handler, click_handler_1, click_handler_2, click_handler_3];
    }

    class SocialShare extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$9, create_fragment$9, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "SocialShare",
    			options,
    			id: create_fragment$9.name
    		});
    	}
    }

    /* src/components/LearnMore.svelte generated by Svelte v3.20.1 */
    const file$9 = "src/components/LearnMore.svelte";

    function create_fragment$a(ctx) {
    	let div3;
    	let div2;
    	let t0;
    	let div0;
    	let t2;
    	let a;
    	let img;
    	let img_src_value;
    	let t3;
    	let div1;
    	let a_href_value;
    	let current;
    	let dispose;
    	const default_slot_template = /*$$slots*/ ctx[1].default;
    	const default_slot = create_slot(default_slot_template, ctx, /*$$scope*/ ctx[0], null);

    	const block = {
    		c: function create() {
    			div3 = element("div");
    			div2 = element("div");
    			if (default_slot) default_slot.c();
    			t0 = space();
    			div0 = element("div");
    			div0.textContent = `${text$1("MORE_INFO")}`;
    			t2 = space();
    			a = element("a");
    			img = element("img");
    			t3 = space();
    			div1 = element("div");
    			div1.textContent = `${IS_VK ? "Screenlifer" : "Screenlifer.com"}`;
    			add_location(div0, file$9, 10, 4, 235);
    			attr_dev(img, "class", "screenliferLogo svelte-xu1z8t");
    			if (img.src !== (img_src_value = `${BASE_URL}/assets/logo-screenlife.png`)) attr_dev(img, "src", img_src_value);
    			attr_dev(img, "alt", "screenlife logo");
    			add_location(img, file$9, 17, 6, 475);
    			add_location(div1, file$9, 21, 6, 606);

    			attr_dev(a, "href", a_href_value = IS_VK
    			? "https://vk.com/screenlifer"
    			: "http://screenlifer.com/quarantine/");

    			attr_dev(a, "target", "_blank");
    			attr_dev(a, "class", "screenliferLink svelte-xu1z8t");
    			add_location(a, file$9, 12, 4, 271);
    			attr_dev(div2, "class", "screenliferBlock svelte-xu1z8t");
    			add_location(div2, file$9, 8, 2, 187);
    			attr_dev(div3, "class", "learnMore svelte-xu1z8t");
    			add_location(div3, file$9, 6, 0, 160);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor, remount) {
    			insert_dev(target, div3, anchor);
    			append_dev(div3, div2);

    			if (default_slot) {
    				default_slot.m(div2, null);
    			}

    			append_dev(div2, t0);
    			append_dev(div2, div0);
    			append_dev(div2, t2);
    			append_dev(div2, a);
    			append_dev(a, img);
    			append_dev(a, t3);
    			append_dev(a, div1);
    			current = true;
    			if (remount) dispose();
    			dispose = listen_dev(a, "click", /*click_handler*/ ctx[2], false, false, false);
    		},
    		p: function update(ctx, [dirty]) {
    			if (default_slot) {
    				if (default_slot.p && dirty & /*$$scope*/ 1) {
    					default_slot.p(get_slot_context(default_slot_template, ctx, /*$$scope*/ ctx[0], null), get_slot_changes(default_slot_template, /*$$scope*/ ctx[0], dirty, null));
    				}
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(default_slot, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(default_slot, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div3);
    			if (default_slot) default_slot.d(detaching);
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$a.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$a($$self, $$props, $$invalidate) {
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<LearnMore> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("LearnMore", $$slots, ['default']);
    	const click_handler = () => sendMetrik("ScreenliferLink");

    	$$self.$set = $$props => {
    		if ("$$scope" in $$props) $$invalidate(0, $$scope = $$props.$$scope);
    	};

    	$$self.$capture_state = () => ({ sendMetrik, text: text$1, BASE_URL, IS_VK });
    	return [$$scope, $$slots, click_handler];
    }

    class LearnMore extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$a, create_fragment$a, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "LearnMore",
    			options,
    			id: create_fragment$a.name
    		});
    	}
    }

    /* src/assets/Close.svelte generated by Svelte v3.20.1 */

    const file$a = "src/assets/Close.svelte";

    function create_fragment$b(ctx) {
    	let svg;
    	let path;

    	const block = {
    		c: function create() {
    			svg = svg_element("svg");
    			path = svg_element("path");
    			attr_dev(path, "d", "m194.800781 164.769531 128.210938-128.214843c8.34375-8.339844\n    8.34375-21.824219 0-30.164063-8.339844-8.339844-21.824219-8.339844-30.164063\n    0l-128.214844\n    128.214844-128.210937-128.214844c-8.34375-8.339844-21.824219-8.339844-30.164063\n    0-8.34375 8.339844-8.34375 21.824219 0 30.164063l128.210938\n    128.214843-128.210938 128.214844c-8.34375 8.339844-8.34375 21.824219 0\n    30.164063 4.15625 4.160156 9.621094 6.25 15.082032 6.25 5.460937 0\n    10.921875-2.089844 15.082031-6.25l128.210937-128.214844 128.214844\n    128.214844c4.160156 4.160156 9.621094 6.25 15.082032 6.25 5.460937 0\n    10.921874-2.089844 15.082031-6.25 8.34375-8.339844 8.34375-21.824219\n    0-30.164063zm0 0");
    			attr_dev(path, "data-original", "#000000");
    			attr_dev(path, "class", "active-path svelte-aejmer");
    			add_location(path, file$a, 2, 2, 64);
    			attr_dev(svg, "height", "16px");
    			attr_dev(svg, "viewBox", "0 0 329.26933 329");
    			attr_dev(svg, "width", "16px");
    			add_location(svg, file$a, 0, 0, 0);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, svg, anchor);
    			append_dev(svg, path);
    		},
    		p: noop,
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(svg);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$b.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$b($$self, $$props) {
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<Close> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("Close", $$slots, []);
    	return [];
    }

    class Close extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$b, create_fragment$b, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Close",
    			options,
    			id: create_fragment$b.name
    		});
    	}
    }

    /* src/components/Close.svelte generated by Svelte v3.20.1 */
    const file$b = "src/components/Close.svelte";

    // (8:0) {#if !IS_VK}
    function create_if_block$2(ctx) {
    	let div;
    	let button;
    	let current;
    	let dispose;
    	const close_1 = new Close({ $$inline: true });

    	const block = {
    		c: function create() {
    			div = element("div");
    			button = element("button");
    			create_component(close_1.$$.fragment);
    			attr_dev(button, "class", "button svelte-12r4zhg");
    			add_location(button, file$b, 9, 4, 239);
    			attr_dev(div, "class", "closeContainer svelte-12r4zhg");
    			add_location(div, file$b, 8, 2, 206);
    		},
    		m: function mount(target, anchor, remount) {
    			insert_dev(target, div, anchor);
    			append_dev(div, button);
    			mount_component(close_1, button, null);
    			current = true;
    			if (remount) dispose();
    			dispose = listen_dev(button, "click", close, false, false, false);
    		},
    		p: noop,
    		i: function intro(local) {
    			if (current) return;
    			transition_in(close_1.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(close_1.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			destroy_component(close_1);
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$2.name,
    		type: "if",
    		source: "(8:0) {#if !IS_VK}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$c(ctx) {
    	let if_block_anchor;
    	let current;
    	let if_block = !IS_VK && create_if_block$2(ctx);

    	const block = {
    		c: function create() {
    			if (if_block) if_block.c();
    			if_block_anchor = empty();
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			if (if_block) if_block.m(target, anchor);
    			insert_dev(target, if_block_anchor, anchor);
    			current = true;
    		},
    		p: function update(ctx, [dirty]) {
    			if (!IS_VK) if_block.p(ctx, dirty);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(if_block);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(if_block);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (if_block) if_block.d(detaching);
    			if (detaching) detach_dev(if_block_anchor);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$c.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$c($$self, $$props, $$invalidate) {
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<Close> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("Close", $$slots, []);
    	$$self.$capture_state = () => ({ close, sendMetrik, IS_VK, Close });
    	return [];
    }

    class Close_1 extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$c, create_fragment$c, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Close_1",
    			options,
    			id: create_fragment$c.name
    		});
    	}
    }

    /* src/views/Lose.svelte generated by Svelte v3.20.1 */
    const file$c = "src/views/Lose.svelte";

    // (20:4) {:else}
    function create_else_block$1(ctx) {
    	let div;

    	const block = {
    		c: function create() {
    			div = element("div");
    			div.textContent = `${text$1("PLAY_IN_VK_TO_WIN")}`;
    			attr_dev(div, "class", "prize svelte-zuds6i");
    			add_location(div, file$c, 20, 6, 590);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    		},
    		p: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_else_block$1.name,
    		type: "else",
    		source: "(20:4) {:else}",
    		ctx
    	});

    	return block;
    }

    // (18:4) {#if IS_VK}
    function create_if_block$3(ctx) {
    	let div;

    	const block = {
    		c: function create() {
    			div = element("div");
    			div.textContent = `${text$1("WIN_TO_GET_PRIZE")}`;
    			attr_dev(div, "class", "prize svelte-zuds6i");
    			add_location(div, file$c, 18, 6, 520);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    		},
    		p: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$3.name,
    		type: "if",
    		source: "(18:4) {#if IS_VK}",
    		ctx
    	});

    	return block;
    }

    // (17:2) <LearnMore>
    function create_default_slot(ctx) {
    	let if_block_anchor;

    	function select_block_type(ctx, dirty) {
    		if (IS_VK) return create_if_block$3;
    		return create_else_block$1;
    	}

    	let current_block_type = select_block_type();
    	let if_block = current_block_type(ctx);

    	const block = {
    		c: function create() {
    			if_block.c();
    			if_block_anchor = empty();
    		},
    		m: function mount(target, anchor) {
    			if_block.m(target, anchor);
    			insert_dev(target, if_block_anchor, anchor);
    		},
    		p: function update(ctx, dirty) {
    			if_block.p(ctx, dirty);
    		},
    		d: function destroy(detaching) {
    			if_block.d(detaching);
    			if (detaching) detach_dev(if_block_anchor);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_default_slot.name,
    		type: "slot",
    		source: "(17:2) <LearnMore>",
    		ctx
    	});

    	return block;
    }

    function create_fragment$d(ctx) {
    	let div3;
    	let div1;
    	let div0;
    	let t1;
    	let t2;
    	let div2;
    	let button;
    	let t4;
    	let t5;
    	let current;
    	let dispose;

    	const learnmore = new LearnMore({
    			props: {
    				$$slots: { default: [create_default_slot] },
    				$$scope: { ctx }
    			},
    			$$inline: true
    		});

    	const socialshare = new SocialShare({ $$inline: true });
    	const close = new Close_1({ $$inline: true });

    	const block = {
    		c: function create() {
    			div3 = element("div");
    			div1 = element("div");
    			div0 = element("div");
    			div0.textContent = `${text$1("VIRUS_WIN")}`;
    			t1 = space();
    			create_component(learnmore.$$.fragment);
    			t2 = space();
    			div2 = element("div");
    			button = element("button");
    			button.textContent = `${text$1("REPLAY")}`;
    			t4 = space();
    			create_component(socialshare.$$.fragment);
    			t5 = space();
    			create_component(close.$$.fragment);
    			attr_dev(div0, "class", "primary svelte-zuds6i");
    			add_location(div0, file$c, 12, 4, 423);
    			attr_dev(div1, "class", "message svelte-zuds6i");
    			toggle_class(div1, "isWidget", IS_MOBILE_VK);
    			add_location(div1, file$c, 11, 2, 367);
    			attr_dev(button, "class", "button svelte-zuds6i");
    			add_location(button, file$c, 25, 4, 697);
    			attr_dev(div2, "class", "buttons");
    			add_location(div2, file$c, 23, 2, 670);
    			attr_dev(div3, "class", "container svelte-zuds6i");
    			add_location(div3, file$c, 10, 0, 341);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor, remount) {
    			insert_dev(target, div3, anchor);
    			append_dev(div3, div1);
    			append_dev(div1, div0);
    			append_dev(div3, t1);
    			mount_component(learnmore, div3, null);
    			append_dev(div3, t2);
    			append_dev(div3, div2);
    			append_dev(div2, button);
    			append_dev(div2, t4);
    			mount_component(socialshare, div2, null);
    			insert_dev(target, t5, anchor);
    			mount_component(close, target, anchor);
    			current = true;
    			if (remount) dispose();
    			dispose = listen_dev(button, "click", replay, false, false, false);
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*IS_MOBILE_VK*/ 0) {
    				toggle_class(div1, "isWidget", IS_MOBILE_VK);
    			}

    			const learnmore_changes = {};

    			if (dirty & /*$$scope*/ 1) {
    				learnmore_changes.$$scope = { dirty, ctx };
    			}

    			learnmore.$set(learnmore_changes);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(learnmore.$$.fragment, local);
    			transition_in(socialshare.$$.fragment, local);
    			transition_in(close.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(learnmore.$$.fragment, local);
    			transition_out(socialshare.$$.fragment, local);
    			transition_out(close.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div3);
    			destroy_component(learnmore);
    			destroy_component(socialshare);
    			if (detaching) detach_dev(t5);
    			destroy_component(close, detaching);
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$d.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$d($$self, $$props, $$invalidate) {
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<Lose> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("Lose", $$slots, []);

    	$$self.$capture_state = () => ({
    		replay,
    		GAME_VIEWS,
    		IS_MOBILE_VK,
    		IS_VK,
    		SocialShare,
    		LearnMore,
    		Close: Close_1,
    		text: text$1
    	});

    	return [];
    }

    class Lose extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$d, create_fragment$d, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Lose",
    			options,
    			id: create_fragment$d.name
    		});
    	}
    }

    /* src/assets/Gift.svelte generated by Svelte v3.20.1 */

    const file$d = "src/assets/Gift.svelte";

    function create_fragment$e(ctx) {
    	let svg;
    	let g;
    	let path0;
    	let path1;
    	let path2;
    	let path3;
    	let path4;
    	let path5;
    	let path6;
    	let path7;
    	let path8;
    	let path9;
    	let path10;
    	let path11;

    	const block = {
    		c: function create() {
    			svg = svg_element("svg");
    			g = svg_element("g");
    			path0 = svg_element("path");
    			path1 = svg_element("path");
    			path2 = svg_element("path");
    			path3 = svg_element("path");
    			path4 = svg_element("path");
    			path5 = svg_element("path");
    			path6 = svg_element("path");
    			path7 = svg_element("path");
    			path8 = svg_element("path");
    			path9 = svg_element("path");
    			path10 = svg_element("path");
    			path11 = svg_element("path");
    			set_style(path0, "fill", "#FF3D43");
    			attr_dev(path0, "d", "M233.209,104.738c-2.967-3.757-73.296-91.913-128.816-84.252   c-19.686,2.718-34.847,16.554-45.06,41.123c-14.09,33.895-15.261,61.447-3.48,81.889c14.667,25.45,43.739,29.556,44.969,29.719   c0.325,0.043,0.653,0.064,0.981,0.064h125.519c4.142,0,7.5-3.358,7.5-7.5v-56.395C234.823,107.7,234.254,106.062,233.209,104.738z");
    			add_location(path0, file$d, 3, 1, 64);
    			set_style(path1, "fill", "#FF3D43");
    			attr_dev(path1, "d", "M451.706,61.609c-10.213-24.569-25.373-38.405-45.059-41.123   c-55.509-7.663-125.85,80.496-128.816,84.252c-1.045,1.324-1.614,2.962-1.614,4.648v56.395c0,4.142,3.358,7.5,7.5,7.5h125.519   c0.328,0,0.656-0.021,0.981-0.064c1.23-0.163,30.302-4.269,44.969-29.719C466.968,123.055,465.797,95.504,451.706,61.609z");
    			add_location(path1, file$d, 4, 1, 411);
    			set_style(path2, "fill", "#D62D2D");
    			attr_dev(path2, "d", "M227.323,138.781H101.804c-0.328,0-0.656-0.021-0.981-0.064c-1.23-0.163-30.302-4.268-44.969-29.719   c-2.828-4.907-4.901-10.228-6.237-15.943c-3.727,19.851-1.657,36.746,6.237,50.443c14.667,25.45,43.739,29.556,44.969,29.719   c0.325,0.043,0.653,0.064,0.981,0.064h125.519c4.142,0,7.5-3.358,7.5-7.5v-34.5C234.823,135.423,231.465,138.781,227.323,138.781z");
    			add_location(path2, file$d, 5, 1, 749);
    			set_style(path3, "fill", "#D62D2D");
    			attr_dev(path3, "d", "M455.186,108.998c-14.667,25.45-43.739,29.556-44.969,29.719c-0.325,0.043-0.653,0.064-0.981,0.064   H283.717c-4.142,0-7.5-3.358-7.5-7.5v34.5c0,4.142,3.358,7.5,7.5,7.5h125.519c0.328,0,0.656-0.021,0.981-0.064   c1.23-0.163,30.302-4.269,44.969-29.719c7.894-13.698,9.964-30.592,6.237-50.443C460.088,98.77,458.014,104.091,455.186,108.998z");
    			add_location(path3, file$d, 6, 1, 1132);
    			set_style(path4, "fill", "#D62D2D");
    			attr_dev(path4, "d", "M283.717,101.886h-56.395c-4.142,0-7.5,3.358-7.5,7.5v56.395c0,4.142,3.358,7.5,7.5,7.5h56.395   c4.142,0,7.5-3.358,7.5-7.5v-56.395C291.217,105.244,287.859,101.886,283.717,101.886z");
    			add_location(path4, file$d, 7, 1, 1499);
    			set_style(path5, "fill", "#9E1219");
    			attr_dev(path5, "d", "M219.823,138.781v27c0,4.142,3.358,7.5,7.5,7.5h56.395c4.142,0,7.5-3.358,7.5-7.5v-27H219.823z");
    			add_location(path5, file$d, 8, 1, 1712);
    			set_style(path6, "fill", "#FFD039");
    			attr_dev(path6, "d", "M465.65,232.366H45.39c-4.142,0-7.5,3.358-7.5,7.5v218.568c0,17.972,14.621,32.593,32.593,32.593   h370.074c17.972,0,32.593-14.621,32.593-32.593V239.866C473.15,235.723,469.792,232.366,465.65,232.366z");
    			add_location(path6, file$d, 9, 1, 1839);
    			set_style(path7, "fill", "#F4B70C");
    			attr_dev(path7, "d", "M465.65,232.366H45.39c-4.142,0-7.5,3.358-7.5,7.5v32.224h435.26v-32.224   C473.15,235.723,469.792,232.366,465.65,232.366z");
    			add_location(path7, file$d, 10, 1, 2071);
    			set_style(path8, "fill", "#FFDF65");
    			attr_dev(path8, "d", "M503.54,158.281H7.5c-4.142,0-7.5,3.358-7.5,7.5v74.084c0,4.142,3.358,7.5,7.5,7.5h496.04   c4.142,0,7.5-3.358,7.5-7.5v-74.084C511.04,161.639,507.682,158.281,503.54,158.281z");
    			add_location(path8, file$d, 11, 1, 2227);
    			set_style(path9, "fill", "#FFCD2C");
    			attr_dev(path9, "d", "M0,212.865v27c0,4.142,3.358,7.5,7.5,7.5h496.04c4.142,0,7.5-3.358,7.5-7.5v-27H0z");
    			add_location(path9, file$d, 12, 1, 2433);
    			set_style(path10, "fill", "#FF3D43");
    			attr_dev(path10, "d", "M292.603,158.281h-74.167c-4.142,0-7.5,3.358-7.5,7.5v317.745c0,4.142,3.358,7.5,7.5,7.5h74.167   c4.142,0,7.5-3.358,7.5-7.5V165.781C300.103,161.639,296.746,158.281,292.603,158.281z");
    			add_location(path10, file$d, 13, 1, 2548);
    			set_style(path11, "fill", "#D62D2D");
    			attr_dev(path11, "d", "M292.603,158.281h-27v332.745h27c4.142,0,7.5-3.358,7.5-7.5V165.781   C300.103,161.639,296.746,158.281,292.603,158.281z");
    			add_location(path11, file$d, 14, 1, 2762);
    			add_location(g, file$d, 2, 0, 59);
    			attr_dev(svg, "viewBox", "0 0 511.04 511.04");
    			attr_dev(svg, "width", "64");
    			attr_dev(svg, "height", "64");
    			add_location(svg, file$d, 1, 0, 1);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, svg, anchor);
    			append_dev(svg, g);
    			append_dev(g, path0);
    			append_dev(g, path1);
    			append_dev(g, path2);
    			append_dev(g, path3);
    			append_dev(g, path4);
    			append_dev(g, path5);
    			append_dev(g, path6);
    			append_dev(g, path7);
    			append_dev(g, path8);
    			append_dev(g, path9);
    			append_dev(g, path10);
    			append_dev(g, path11);
    		},
    		p: noop,
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(svg);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$e.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$e($$self, $$props) {
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<Gift> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("Gift", $$slots, []);
    	return [];
    }

    class Gift extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$e, create_fragment$e, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Gift",
    			options,
    			id: create_fragment$e.name
    		});
    	}
    }

    /* src/views/Win.svelte generated by Svelte v3.20.1 */
    const file$e = "src/views/Win.svelte";

    // (26:8) {:else}
    function create_else_block$2(ctx) {
    	let div;

    	const block = {
    		c: function create() {
    			div = element("div");
    			div.textContent = `${text$1("REPEAT_YOUR_SUCESS")}`;
    			attr_dev(div, "class", "notVkPrize svelte-1j0db8j");
    			add_location(div, file$e, 26, 8, 744);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    		},
    		p: noop,
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_else_block$2.name,
    		type: "else",
    		source: "(26:8) {:else}",
    		ctx
    	});

    	return block;
    }

    // (19:6) {#if IS_VK}
    function create_if_block$4(ctx) {
    	let div2;
    	let div0;
    	let t0;
    	let div1;
    	let current;
    	const gift = new Gift({ $$inline: true });

    	const block = {
    		c: function create() {
    			div2 = element("div");
    			div0 = element("div");
    			create_component(gift.$$.fragment);
    			t0 = space();
    			div1 = element("div");
    			div1.textContent = `${text$1("PRIZE_DRAW")}`;
    			attr_dev(div0, "class", "gift");
    			add_location(div0, file$e, 20, 10, 605);
    			add_location(div1, file$e, 23, 10, 672);
    			attr_dev(div2, "class", "prize svelte-1j0db8j");
    			add_location(div2, file$e, 19, 8, 575);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div2, anchor);
    			append_dev(div2, div0);
    			mount_component(gift, div0, null);
    			append_dev(div2, t0);
    			append_dev(div2, div1);
    			current = true;
    		},
    		p: noop,
    		i: function intro(local) {
    			if (current) return;
    			transition_in(gift.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(gift.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div2);
    			destroy_component(gift);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$4.name,
    		type: "if",
    		source: "(19:6) {#if IS_VK}",
    		ctx
    	});

    	return block;
    }

    // (18:2) <LearnMore>
    function create_default_slot$1(ctx) {
    	let current_block_type_index;
    	let if_block;
    	let if_block_anchor;
    	let current;
    	const if_block_creators = [create_if_block$4, create_else_block$2];
    	const if_blocks = [];

    	function select_block_type(ctx, dirty) {
    		if (IS_VK) return 0;
    		return 1;
    	}

    	current_block_type_index = select_block_type();
    	if_block = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);

    	const block = {
    		c: function create() {
    			if_block.c();
    			if_block_anchor = empty();
    		},
    		m: function mount(target, anchor) {
    			if_blocks[current_block_type_index].m(target, anchor);
    			insert_dev(target, if_block_anchor, anchor);
    			current = true;
    		},
    		p: function update(ctx, dirty) {
    			if_block.p(ctx, dirty);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(if_block);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(if_block);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if_blocks[current_block_type_index].d(detaching);
    			if (detaching) detach_dev(if_block_anchor);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_default_slot$1.name,
    		type: "slot",
    		source: "(18:2) <LearnMore>",
    		ctx
    	});

    	return block;
    }

    function create_fragment$f(ctx) {
    	let div3;
    	let div1;
    	let div0;
    	let t1;
    	let t2;
    	let div2;
    	let button;
    	let t4;
    	let t5;
    	let current;
    	let dispose;

    	const learnmore = new LearnMore({
    			props: {
    				$$slots: { default: [create_default_slot$1] },
    				$$scope: { ctx }
    			},
    			$$inline: true
    		});

    	const socialshare = new SocialShare({ $$inline: true });
    	const close = new Close_1({ $$inline: true });

    	const block = {
    		c: function create() {
    			div3 = element("div");
    			div1 = element("div");
    			div0 = element("div");
    			div0.textContent = `${text$1("CONGRATULATIONS")}`;
    			t1 = space();
    			create_component(learnmore.$$.fragment);
    			t2 = space();
    			div2 = element("div");
    			button = element("button");
    			button.textContent = `${text$1("REPLAY")}`;
    			t4 = space();
    			create_component(socialshare.$$.fragment);
    			t5 = space();
    			create_component(close.$$.fragment);
    			attr_dev(div0, "class", "primary svelte-1j0db8j");
    			add_location(div0, file$e, 13, 4, 467);
    			attr_dev(div1, "class", "message svelte-1j0db8j");
    			toggle_class(div1, "isWidget", IS_MOBILE_VK);
    			add_location(div1, file$e, 12, 2, 411);
    			attr_dev(button, "class", "button svelte-1j0db8j");
    			add_location(button, file$e, 33, 4, 873);
    			attr_dev(div2, "class", "buttons");
    			add_location(div2, file$e, 31, 2, 842);
    			attr_dev(div3, "class", "container svelte-1j0db8j");
    			add_location(div3, file$e, 11, 0, 385);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor, remount) {
    			insert_dev(target, div3, anchor);
    			append_dev(div3, div1);
    			append_dev(div1, div0);
    			append_dev(div3, t1);
    			mount_component(learnmore, div3, null);
    			append_dev(div3, t2);
    			append_dev(div3, div2);
    			append_dev(div2, button);
    			append_dev(div2, t4);
    			mount_component(socialshare, div2, null);
    			insert_dev(target, t5, anchor);
    			mount_component(close, target, anchor);
    			current = true;
    			if (remount) dispose();
    			dispose = listen_dev(button, "click", replay, false, false, false);
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*IS_MOBILE_VK*/ 0) {
    				toggle_class(div1, "isWidget", IS_MOBILE_VK);
    			}

    			const learnmore_changes = {};

    			if (dirty & /*$$scope*/ 1) {
    				learnmore_changes.$$scope = { dirty, ctx };
    			}

    			learnmore.$set(learnmore_changes);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(learnmore.$$.fragment, local);
    			transition_in(socialshare.$$.fragment, local);
    			transition_in(close.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(learnmore.$$.fragment, local);
    			transition_out(socialshare.$$.fragment, local);
    			transition_out(close.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div3);
    			destroy_component(learnmore);
    			destroy_component(socialshare);
    			if (detaching) detach_dev(t5);
    			destroy_component(close, detaching);
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$f.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$f($$self, $$props, $$invalidate) {
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<Win> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("Win", $$slots, []);

    	$$self.$capture_state = () => ({
    		replay,
    		GAME_VIEWS,
    		IS_MOBILE_VK,
    		IS_VK,
    		text: text$1,
    		Gift,
    		SocialShare,
    		LearnMore,
    		Close: Close_1
    	});

    	return [];
    }

    class Win extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$f, create_fragment$f, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Win",
    			options,
    			id: create_fragment$f.name
    		});
    	}
    }

    var quizDict = {
      ru: [
        {
          question: "Что является одним из основных симптомов?",
          answers: ["Одышка", "Головная боль"],
          answer: "Одышка",
        },
        {
          question: "Что является одним из основных симптомов?",
          answers: ["Высокая температура", "Заложенность грудной клетки"],
          answer: "Высокая температура",
        },
        {
          question: "Что является одним из основных симптомов?",
          answers: ["Утомляемость", "Кровохарканье"],
          answer: "Утомляемость",
        },
        {
          question: "Что является одним из основных симптомов?",
          answers: ["Утомляемость", "Диарея"],
          answer: "Утомляемость",
        },
        {
          question: "Что является одним из основных симптомов?",
          answers: ["Кашель", "Тошнота"],
          answer: "Кашель",
        },
        {
          question: "Что является одним из основных симптомов?",
          answers: ["Боль в мышцах", "Головная боль"],
          answer: "Боль в мышцах",
        },
        {
          question: "Что делать, если обнаружил симптомы",
          answers: ["Вызвать скорую", "Пойти в поликлиннику"],
          answer: "Вызвать скорую",
        },
        {
          question: "Нужно ли проветривать помещение",
          answers: ["Нужно часто", "Нет, полная изоляция"],
          answer: "Нужно часто",
        },
        {
          question: "Кто может заразиться?",
          answers: ["Люди всех возрастов", "Люди старше 65"],
          answer: "Люди всех возрастов",
        },
        {
          question: "Кто в группе риска?",
          answers: ["Люди старше 65", "Дети до 18"],
          answer: "Люди старше 65",
        },
        {
          question: "Инкубационный период у гриппа",
          answers: ["3 дня", "5-6 дней"],
          answer: "3 дня",
        },
        {
          question: "Инкубационный период у COVID-19",
          answers: ["До 14 дней", "3 дня"],
          answer: "До 14 дней",
        },
        {
          question: "Срок изоляции при карантине",
          answers: ["15 дней", "3 дня"],
          answer: "15 дней",
        },
        {
          question: "Стоимость оказания помощи с COVID-19",
          answers: ["Бесплатно", "1 000 000"],
          answer: "Бесплатно",
        },
        {
          question: "Могут ли родственники посещать больного",
          answers: ["Нет", "Да"],
          answer: "Нет",
        },
        {
          question: "Можно ли заразиться через мобильный телефон",
          answers: ["Да", "Нет"],
          answer: "Да",
        },
        {
          question: "Передаётся ли COVID-19 воздушно-капельным путём?",
          answers: ["Да", "Нет"],
          answer: "Да",
        },
        {
          question: "Передаётся ли COVID-19 контактным путём",
          answers: ["Да", "Нет"],
          answer: "Да",
        },
        {
          question: "Можно ли заразиться через поручни в транспорте?",
          answers: ["Да", "Нет"],
          answer: "Да",
        },
        {
          question: "Для чего можно выходить из дома?",
          answers: ["В экстренных случаях", "Для занятий спортом"],
          answer: "В экстренных случаях",
        },
        {
          question: "Почему нельзя касаться рта, носа, глаз?",
          answers: ["Можно заразиться", "Вредная привычка"],
          answer: "Можно заразиться",
        },
        {
          question: "Какую дистанцию нужно соблюдать при общении?",
          answers: ["1.5 метра", "0.5 метра"],
          answer: "1.5 метра",
        },
        {
          question: "Как лечиться?",
          answers: ["Обратиться к врачу", "Самолечение"],
          answer: "Обратиться к врачу",
        },
        {
          question: "Можно ли пользоваться личным транспортом?",
          answers: ["В случае необходимости", "Поехать на экскурсию"],
          answer: "В случае необходимости",
        },
      ],
      en: [
        {
          question: "What is one of the main symptoms?",
          answers: ["Shortness of breath", "Headache"],
          answer: "Shortness of breath",
        },
        {
          question: "What is one of the main symptoms?",
          answers: ["High temperature", "Chest tightness"],
          answer: "High temperature",
        },
        {
          question: "What is one of the main symptoms?",
          answers: ["Fatigue", "Coughing up blood"],
          answer: "Fatigue",
        },
        {
          question: "What is one of the main symptoms?",
          answers: ["Fatigue", "Diarrhea"],
          answer: "Fatigue",
        },
        {
          question: "What is one of the main symptoms?",
          answers: ["Coughing", "Nausea"],
          answer: "Coughing",
        },
        {
          question: "What is one of the main symptoms?",
          answers: ["Muscular pain", "Headache"],
          answer: "Muscular pain",
        },
        {
          question: "What should you do if you have symptoms?",
          answers: ["Call an ambulance", "Go to hospital"],
          answer: "Call an ambulance",
        },
        {
          question: "Should you ventilate the room?",
          answers: ["Yes, often", "No, complete isolation"],
          answer: "Yes, often",
        },
        {
          question: "Who can become infected?",
          answers: ["People of all ages", "People over 65"],
          answer: "People of all ages",
        },
        {
          question: "Who is in the risk group?",
          answers: ["People over 65", "Children under 18"],
          answer: "People over 65",
        },
        {
          question: "Incubation period for flu",
          answers: ["3 days", "5-6 days"],
          answer: "3 days",
        },
        {
          question: "Incubation period for COVID-19",
          answers: ["Up to 14 days", "3 days"],
          answer: "Up to 14 days",
        },
        {
          question: "Time spent in isolation when under quarantine",
          answers: ["15 days", "3 days"],
          answer: "15 days",
        },
        {
          question: "Cost of care for COVID-19",
          answers: ["Free of charge", "1,000,000"],
          answer: "Free of charge",
        },
        {
          question: "Can relatives visit a patient?",
          answers: ["No", "Yes"],
          answer: "No",
        },
        {
          question: "Can you get infected from your mobile phone?",
          answers: ["Yes", "No"],
          answer: "Yes",
        },
        {
          question: "Is COVID-19 an airborne infection?",
          answers: ["Yes", "No"],
          answer: "Yes",
        },
        {
          question: "Is COVID-19 spread through close contact?",
          answers: ["Yes", "No"],
          answer: "Yes",
        },
        {
          question: "Can you get infected from handrails on public transport?",
          answers: ["Yes", "No"],
          answer: "Yes",
        },
        {
          question: "Under what circumstances can you go outside?",
          answers: ["In an emergency", "To do sport"],
          answer: "In an emergency",
        },
        {
          question: "Why must you not touch your mouth, nose, or eyes?",
          answers: ["You can get infected", "It's a bad habit"],
          answer: "You can get infected",
        },
        {
          question: "What social distance should you maintain?",
          answers: ["1.5 metres", "0.5 metres"],
          answer: "1.5 metres",
        },
        {
          question: "What should you do for treatment? ",
          answers: ["See a doctor", "Self-treatment"],
          answer: "See a doctor",
        },
        {
          question: "Can you use your own vehicle?",
          answers: ["If necessary", "To go on a trip"],
          answer: "If necessary",
        },
      ],
    };

    const activeQuestionIndex = writable(0);
    const selectedAnswer = writable(null);

    const quizTimer = (() => {
      const { set, subscribe, update } = writable(QUIZ_TIME);
      let timeoutId = null;
      let startTime = 0;
      return {
        subscribe,
        start: () => {
          playTick();
          const updateTimer = () => {
            update(() => {
              const newTime = QUIZ_TIME - (Date.now() - startTime);
              if (newTime <= 0) {
                stopTick();
                sendMetrik("QuizFail", { reason: "timout" });
                gameView.set(GAME_VIEWS.LOSE);
                return 0;
              }
              timeoutId = requestAnimationFrame(updateTimer);
              return newTime;
            });
          };
          startTime = Date.now();
          timeoutId = requestAnimationFrame(updateTimer);
        },
        stop:()=>{
          cancelAnimationFrame(timeoutId);
        },
        reset: () => {
          stopTick();
          cancelAnimationFrame(timeoutId);
          set(QUIZ_TIME);
        },
      };
    })();

    const quizQuestions = (() => {
      const { subscribe, set } = writable([]);
      return {
        subscribe,
        init: () =>
          set(
            quizDict[lang]
              .sort(() => 0.5 - Math.random())
              .slice(0, QUESTION_COUNT)
              .map((item) => ({
                ...item,
                answers: item.answers.sort(() => 0.5 - Math.random()),
              }))
          ),
      };
    })();
    quizQuestions.init();

    const activeQuestion = derived(
      [activeQuestionIndex, quizQuestions],
      ([$activeQuestionIndex, $quizQuestions]) =>
        $quizQuestions[$activeQuestionIndex]
    );

    const isAnswerCorrect = derived(
      [activeQuestion, selectedAnswer],
      ([$activeQuestion, $selectedAnswer]) => {
        if ($selectedAnswer === null) return null;
        return $activeQuestion.answer === $selectedAnswer;
      }
    );
    isAnswerCorrect.subscribe((state) => {
      if (state === false) {
        quizTimer.reset();
        playWrongQuiz();
        sendMetrik("QuizFail", {
          reason: "wrongAnswer",
          question: get_store_value(activeQuestion),
          userAnwser: get_store_value(selectedAnswer),
        });
      } else if (state === true) {
        playRightQuiz();
        if (get_store_value(activeQuestionIndex) === QUESTION_COUNT - 1) {
          quizTimer.stop();
        }
      }
    });

    const nextQuestion = () => {
      if (!get_store_value(isAnswerCorrect)) {
        gameView.set(GAME_VIEWS.LOSE);
        return;
      }
      selectedAnswer.set(null);
      if (get_store_value(activeQuestionIndex) === QUESTION_COUNT - 1) {
        sendMetrik("QuizWin");
        gameView.set(GAME_VIEWS.WIN);
      } else {
        activeQuestionIndex.update((state) => state + 1);
      }
    };

    const startQuiz = () => {
      activeQuestionIndex.set(0);
      quizTimer.reset();
      quizTimer.start();
      selectedAnswer.set(null);
      quizQuestions.init();
    };

    gameView.subscribe((state) => {
      if (state === GAME_VIEWS.QUIZ) {
        startQuiz();
      }
      if (state === GAME_VIEWS.LOSE || state === GAME_VIEWS.WIN) {
        quizTimer.reset();
      }
    });

    /* src/assets/Like.svelte generated by Svelte v3.20.1 */

    const file$f = "src/assets/Like.svelte";

    function create_fragment$g(ctx) {
    	let svg;
    	let g;
    	let path0;
    	let path1;
    	let defs;
    	let clipPath;
    	let rect;

    	const block = {
    		c: function create() {
    			svg = svg_element("svg");
    			g = svg_element("g");
    			path0 = svg_element("path");
    			path1 = svg_element("path");
    			defs = svg_element("defs");
    			clipPath = svg_element("clipPath");
    			rect = svg_element("rect");
    			attr_dev(path0, "d", "M4.08934 13.8549C2.3024 13.8549 0.847412 15.3098 0.847412\n      17.0968V27.471C0.847412 29.258 2.3024 30.713 4.08934\n      30.713H7.97968C8.70978 30.713 9.38148 30.4666 9.92485\n      30.0568V13.8549H4.08934Z");
    			attr_dev(path0, "fill", "white");
    			add_location(path0, file$f, 6, 4, 92);
    			attr_dev(path1, "d", "M31.9701 18.7178C31.9701 17.9384 31.6615 17.2122 31.1259\n      16.6766C31.7315 16.014 32.0402 15.1257 31.9558 14.2011C31.8041 12.5529\n      30.3193 11.2613 28.5739 11.2613H20.5637C20.9605 10.0566 21.5959 7.84818\n      21.5959 6.07418C21.5959 3.26147 19.2059 0.887085 17.7055 0.887085C16.3582\n      0.887085 15.396 1.6457 15.3545 1.67682C15.2015 1.80004 15.112 1.98677\n      15.112 2.18384V6.5812L11.3773 14.6718L11.2217 14.7509V28.6498C12.2772\n      29.1478 13.6129 29.4162 14.4636 29.4162H26.3668C27.779 29.4162 29.0148\n      28.4644 29.3052 27.1508C29.4544 26.4751 29.3675 25.7943 29.0706\n      25.203C30.0289 24.7206 30.6734 23.7337 30.6734 22.6081C30.6734 22.1491\n      30.5683 21.7095 30.3686 21.3114C31.3269 20.829 31.9701 19.8421 31.9701\n      18.7178Z");
    			attr_dev(path1, "fill", "white");
    			add_location(path1, file$f, 12, 4, 342);
    			attr_dev(g, "clip-path", "url(#clip0)");
    			add_location(g, file$f, 5, 2, 60);
    			attr_dev(rect, "x", "0.847656");
    			attr_dev(rect, "y", "0.238678");
    			attr_dev(rect, "width", "31.1227");
    			attr_dev(rect, "height", "31.1227");
    			attr_dev(rect, "fill", "white");
    			add_location(rect, file$f, 28, 6, 1188);
    			attr_dev(clipPath, "id", "clip0");
    			add_location(clipPath, file$f, 27, 4, 1160);
    			add_location(defs, file$f, 26, 2, 1149);
    			attr_dev(svg, "width", "32");
    			attr_dev(svg, "height", "32");
    			attr_dev(svg, "viewBox", "0 0 32 32");
    			add_location(svg, file$f, 0, 0, 0);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, svg, anchor);
    			append_dev(svg, g);
    			append_dev(g, path0);
    			append_dev(g, path1);
    			append_dev(svg, defs);
    			append_dev(defs, clipPath);
    			append_dev(clipPath, rect);
    		},
    		p: noop,
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(svg);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$g.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$g($$self, $$props) {
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<Like> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("Like", $$slots, []);
    	return [];
    }

    class Like extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$g, create_fragment$g, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Like",
    			options,
    			id: create_fragment$g.name
    		});
    	}
    }

    /* src/assets/Dislike.svelte generated by Svelte v3.20.1 */

    const file$g = "src/assets/Dislike.svelte";

    function create_fragment$h(ctx) {
    	let svg;
    	let g;
    	let path0;
    	let path1;
    	let defs;
    	let clipPath;
    	let rect;

    	const block = {
    		c: function create() {
    			svg = svg_element("svg");
    			g = svg_element("g");
    			path0 = svg_element("path");
    			path1 = svg_element("path");
    			defs = svg_element("defs");
    			clipPath = svg_element("clipPath");
    			rect = svg_element("rect");
    			attr_dev(path0, "d", "M4.08934 17.7452C2.3024 17.7452 0.847412 16.2903 0.847412\n      14.5033V4.12906C0.847412 2.34212 2.3024 0.887129 4.08934\n      0.887129H7.97968C8.70978 0.887129 9.38148 1.1335 9.92485\n      1.54332V17.7452H4.08934Z");
    			attr_dev(path0, "fill", "white");
    			add_location(path0, file$g, 7, 4, 140);
    			attr_dev(path1, "d", "M31.9701 12.8823C31.9701 13.6617 31.6615 14.3879 31.1259\n      14.9235C31.7315 15.5861 32.0402 16.4744 31.9558 17.399C31.8041 19.0472\n      30.3193 20.3388 28.5739 20.3388H20.5637C20.9605 21.5435 21.5959 23.7519\n      21.5959 25.5259C21.5959 28.3386 19.2059 30.713 17.7055 30.713C16.3582\n      30.713 15.396 29.9544 15.3545 29.9233C15.2015 29.8001 15.112 29.6133\n      15.112 29.4163V25.0189L11.3773 16.9283L11.2217 16.8492V2.95028C12.2772\n      2.45232 13.6129 2.18388 14.4636 2.18388H26.3668C27.779 2.18388 29.0148\n      3.13574 29.3052 4.44934C29.4544 5.12498 29.3675 5.80579 29.0706\n      6.39712C30.0289 6.87952 30.6734 7.86639 30.6734 8.99198C30.6734 9.45104\n      30.5683 9.89064 30.3686 10.2887C31.3269 10.7711 31.9701 11.758 31.9701\n      12.8823Z");
    			attr_dev(path1, "fill", "white");
    			add_location(path1, file$g, 13, 4, 397);
    			attr_dev(g, "clip-path", "url(#clip0)");
    			add_location(g, file$g, 6, 2, 108);
    			attr_dev(rect, "width", "31.1227");
    			attr_dev(rect, "height", "31.1227");
    			attr_dev(rect, "transform", "matrix(1 0 0 -1 0.847656 31.3615)");
    			attr_dev(rect, "fill", "white");
    			add_location(rect, file$g, 29, 6, 1240);
    			attr_dev(clipPath, "id", "clip0");
    			add_location(clipPath, file$g, 28, 4, 1212);
    			add_location(defs, file$g, 27, 2, 1201);
    			attr_dev(svg, "width", "32");
    			attr_dev(svg, "height", "32");
    			attr_dev(svg, "viewBox", "0 0 32 32");
    			attr_dev(svg, "fill", "none");
    			attr_dev(svg, "xmlns", "http://www.w3.org/2000/svg");
    			add_location(svg, file$g, 0, 0, 0);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, svg, anchor);
    			append_dev(svg, g);
    			append_dev(g, path0);
    			append_dev(g, path1);
    			append_dev(svg, defs);
    			append_dev(defs, clipPath);
    			append_dev(clipPath, rect);
    		},
    		p: noop,
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(svg);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$h.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$h($$self, $$props) {
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<Dislike> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("Dislike", $$slots, []);
    	return [];
    }

    class Dislike extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$h, create_fragment$h, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Dislike",
    			options,
    			id: create_fragment$h.name
    		});
    	}
    }

    /* src/components/Timer.svelte generated by Svelte v3.20.1 */

    const file$h = "src/components/Timer.svelte";

    // (10:2) {#if visible}
    function create_if_block$5(ctx) {
    	let svg;
    	let path0;
    	let path1;
    	let path1_stroke_dasharray_value;
    	let text_1;
    	let t;

    	const block = {
    		c: function create() {
    			svg = svg_element("svg");
    			path0 = svg_element("path");
    			path1 = svg_element("path");
    			text_1 = svg_element("text");
    			t = text(/*seconds*/ ctx[2]);
    			attr_dev(path0, "class", "timerBackground");
    			attr_dev(path0, "d", "M18 2.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0\n        -31.831");
    			attr_dev(path0, "fill", "none");
    			attr_dev(path0, "stroke-width", "1");
    			attr_dev(path0, "stroke-dasharray", "100, 100");
    			add_location(path0, file$h, 11, 6, 279);
    			attr_dev(path1, "class", "timer svelte-a11n2f");
    			attr_dev(path1, "d", "M18 2.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0\n        -31.831");
    			attr_dev(path1, "fill", "none");
    			attr_dev(path1, "stroke", "#444");
    			attr_dev(path1, "stroke-width", "4");
    			attr_dev(path1, "stroke-dasharray", path1_stroke_dasharray_value = `${/*percent*/ ctx[1]}, 100`);
    			add_location(path1, file$h, 18, 6, 505);
    			attr_dev(text_1, "x", "18");
    			attr_dev(text_1, "y", "23");
    			attr_dev(text_1, "class", "text svelte-a11n2f");
    			attr_dev(text_1, "text-anchor", "middle");
    			add_location(text_1, file$h, 26, 6, 752);
    			attr_dev(svg, "viewBox", "0 0 36 36");
    			attr_dev(svg, "class", "timerSvg");
    			add_location(svg, file$h, 10, 4, 230);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, svg, anchor);
    			append_dev(svg, path0);
    			append_dev(svg, path1);
    			append_dev(svg, text_1);
    			append_dev(text_1, t);
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*percent*/ 2 && path1_stroke_dasharray_value !== (path1_stroke_dasharray_value = `${/*percent*/ ctx[1]}, 100`)) {
    				attr_dev(path1, "stroke-dasharray", path1_stroke_dasharray_value);
    			}

    			if (dirty & /*seconds*/ 4) set_data_dev(t, /*seconds*/ ctx[2]);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(svg);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$5.name,
    		type: "if",
    		source: "(10:2) {#if visible}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$i(ctx) {
    	let div;
    	let if_block = /*visible*/ ctx[0] && create_if_block$5(ctx);

    	const block = {
    		c: function create() {
    			div = element("div");
    			if (if_block) if_block.c();
    			attr_dev(div, "class", "timerContainer svelte-a11n2f");
    			add_location(div, file$h, 8, 0, 181);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			if (if_block) if_block.m(div, null);
    		},
    		p: function update(ctx, [dirty]) {
    			if (/*visible*/ ctx[0]) {
    				if (if_block) {
    					if_block.p(ctx, dirty);
    				} else {
    					if_block = create_if_block$5(ctx);
    					if_block.c();
    					if_block.m(div, null);
    				}
    			} else if (if_block) {
    				if_block.d(1);
    				if_block = null;
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			if (if_block) if_block.d();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$i.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$i($$self, $$props, $$invalidate) {
    	let { time = 0 } = $$props;
    	let { totalTime = 1 } = $$props;
    	let { visible = false } = $$props;
    	const writable_props = ["time", "totalTime", "visible"];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<Timer> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("Timer", $$slots, []);

    	$$self.$set = $$props => {
    		if ("time" in $$props) $$invalidate(3, time = $$props.time);
    		if ("totalTime" in $$props) $$invalidate(4, totalTime = $$props.totalTime);
    		if ("visible" in $$props) $$invalidate(0, visible = $$props.visible);
    	};

    	$$self.$capture_state = () => ({
    		time,
    		totalTime,
    		visible,
    		percent,
    		seconds
    	});

    	$$self.$inject_state = $$props => {
    		if ("time" in $$props) $$invalidate(3, time = $$props.time);
    		if ("totalTime" in $$props) $$invalidate(4, totalTime = $$props.totalTime);
    		if ("visible" in $$props) $$invalidate(0, visible = $$props.visible);
    		if ("percent" in $$props) $$invalidate(1, percent = $$props.percent);
    		if ("seconds" in $$props) $$invalidate(2, seconds = $$props.seconds);
    	};

    	let percent;
    	let seconds;

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	$$self.$$.update = () => {
    		if ($$self.$$.dirty & /*time, totalTime*/ 24) {
    			 $$invalidate(1, percent = time / totalTime * 100);
    		}

    		if ($$self.$$.dirty & /*time*/ 8) {
    			 $$invalidate(2, seconds = Math.ceil(time / 1000));
    		}
    	};

    	return [visible, percent, seconds, time, totalTime];
    }

    class Timer extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$i, create_fragment$i, safe_not_equal, { time: 3, totalTime: 4, visible: 0 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Timer",
    			options,
    			id: create_fragment$i.name
    		});
    	}

    	get time() {
    		throw new Error("<Timer>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set time(value) {
    		throw new Error("<Timer>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get totalTime() {
    		throw new Error("<Timer>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set totalTime(value) {
    		throw new Error("<Timer>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get visible() {
    		throw new Error("<Timer>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set visible(value) {
    		throw new Error("<Timer>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/views/Quiz.svelte generated by Svelte v3.20.1 */
    const file$i = "src/views/Quiz.svelte";

    function get_each_context$1(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[8] = list[i];
    	child_ctx[10] = i;
    	return child_ctx;
    }

    // (46:43) 
    function create_if_block_2(ctx) {
    	let div;
    	let div_transition;
    	let current;
    	const dislike = new Dislike({ $$inline: true });

    	const block = {
    		c: function create() {
    			div = element("div");
    			create_component(dislike.$$.fragment);
    			attr_dev(div, "class", "resultIcon dislike svelte-5qcsvv");
    			add_location(div, file$i, 46, 8, 1289);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			mount_component(dislike, div, null);
    			current = true;
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(dislike.$$.fragment, local);

    			if (local) {
    				add_render_callback(() => {
    					if (!div_transition) div_transition = create_bidirectional_transition(div, fade, {}, true);
    					div_transition.run(1);
    				});
    			}

    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(dislike.$$.fragment, local);

    			if (local) {
    				if (!div_transition) div_transition = create_bidirectional_transition(div, fade, {}, false);
    				div_transition.run(0);
    			}

    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			destroy_component(dislike);
    			if (detaching && div_transition) div_transition.end();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_2.name,
    		type: "if",
    		source: "(46:43) ",
    		ctx
    	});

    	return block;
    }

    // (42:6) {#if $isAnswerCorrect === true}
    function create_if_block_1(ctx) {
    	let div;
    	let div_transition;
    	let current;
    	const like = new Like({ $$inline: true });

    	const block = {
    		c: function create() {
    			div = element("div");
    			create_component(like.$$.fragment);
    			attr_dev(div, "class", "resultIcon svelte-5qcsvv");
    			add_location(div, file$i, 42, 8, 1156);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			mount_component(like, div, null);
    			current = true;
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(like.$$.fragment, local);

    			if (local) {
    				add_render_callback(() => {
    					if (!div_transition) div_transition = create_bidirectional_transition(div, fade, {}, true);
    					div_transition.run(1);
    				});
    			}

    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(like.$$.fragment, local);

    			if (local) {
    				if (!div_transition) div_transition = create_bidirectional_transition(div, fade, {}, false);
    				div_transition.run(0);
    			}

    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			destroy_component(like);
    			if (detaching && div_transition) div_transition.end();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_1.name,
    		type: "if",
    		source: "(42:6) {#if $isAnswerCorrect === true}",
    		ctx
    	});

    	return block;
    }

    // (54:6) {#each $activeQuestion.answers as answer, i}
    function create_each_block$1(ctx) {
    	let div;
    	let button;
    	let t0_value = /*answer*/ ctx[8] + "";
    	let t0;
    	let t1;
    	let dispose;

    	function click_handler(...args) {
    		return /*click_handler*/ ctx[7](/*answer*/ ctx[8], ...args);
    	}

    	const block = {
    		c: function create() {
    			div = element("div");
    			button = element("button");
    			t0 = text(t0_value);
    			t1 = space();
    			attr_dev(button, "class", "button answerButton svelte-5qcsvv");
    			toggle_class(button, "isCorrect", /*$selectedAnswer*/ ctx[0] === /*answer*/ ctx[8] && /*$activeQuestion*/ ctx[2].answer === /*answer*/ ctx[8]);
    			toggle_class(button, "isCorrectAnswer", /*$selectedAnswer*/ ctx[0] !== null && /*$selectedAnswer*/ ctx[0] !== /*answer*/ ctx[8] && /*$activeQuestion*/ ctx[2].answer === /*answer*/ ctx[8]);
    			toggle_class(button, "isWrong", /*$selectedAnswer*/ ctx[0] === /*answer*/ ctx[8] && /*$activeQuestion*/ ctx[2].answer !== /*answer*/ ctx[8]);
    			add_location(button, file$i, 55, 10, 1537);
    			attr_dev(div, "class", "buttonWrapper svelte-5qcsvv");
    			add_location(div, file$i, 54, 8, 1499);
    		},
    		m: function mount(target, anchor, remount) {
    			insert_dev(target, div, anchor);
    			append_dev(div, button);
    			append_dev(button, t0);
    			append_dev(div, t1);
    			if (remount) dispose();
    			dispose = listen_dev(button, "click", click_handler, false, false, false);
    		},
    		p: function update(new_ctx, dirty) {
    			ctx = new_ctx;
    			if (dirty & /*$activeQuestion*/ 4 && t0_value !== (t0_value = /*answer*/ ctx[8] + "")) set_data_dev(t0, t0_value);

    			if (dirty & /*$selectedAnswer, $activeQuestion*/ 5) {
    				toggle_class(button, "isCorrect", /*$selectedAnswer*/ ctx[0] === /*answer*/ ctx[8] && /*$activeQuestion*/ ctx[2].answer === /*answer*/ ctx[8]);
    			}

    			if (dirty & /*$selectedAnswer, $activeQuestion*/ 5) {
    				toggle_class(button, "isCorrectAnswer", /*$selectedAnswer*/ ctx[0] !== null && /*$selectedAnswer*/ ctx[0] !== /*answer*/ ctx[8] && /*$activeQuestion*/ ctx[2].answer === /*answer*/ ctx[8]);
    			}

    			if (dirty & /*$selectedAnswer, $activeQuestion*/ 5) {
    				toggle_class(button, "isWrong", /*$selectedAnswer*/ ctx[0] === /*answer*/ ctx[8] && /*$activeQuestion*/ ctx[2].answer !== /*answer*/ ctx[8]);
    			}
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block$1.name,
    		type: "each",
    		source: "(54:6) {#each $activeQuestion.answers as answer, i}",
    		ctx
    	});

    	return block;
    }

    // (69:4) {#if $isAnswerCorrect === false}
    function create_if_block$6(ctx) {
    	let button;
    	let t_value = text$1("PROCEED") + "";
    	let t;
    	let button_disabled_value;
    	let button_transition;
    	let current;
    	let dispose;

    	const block = {
    		c: function create() {
    			button = element("button");
    			t = text(t_value);
    			attr_dev(button, "class", "button submitButton svelte-5qcsvv");
    			button.disabled = button_disabled_value = /*$selectedAnswer*/ ctx[0] === null;
    			add_location(button, file$i, 69, 6, 2121);
    		},
    		m: function mount(target, anchor, remount) {
    			insert_dev(target, button, anchor);
    			append_dev(button, t);
    			current = true;
    			if (remount) dispose();
    			dispose = listen_dev(button, "click", /*onGameContinue*/ ctx[5], false, false, false);
    		},
    		p: function update(ctx, dirty) {
    			if (!current || dirty & /*$selectedAnswer*/ 1 && button_disabled_value !== (button_disabled_value = /*$selectedAnswer*/ ctx[0] === null)) {
    				prop_dev(button, "disabled", button_disabled_value);
    			}
    		},
    		i: function intro(local) {
    			if (current) return;

    			if (local) {
    				add_render_callback(() => {
    					if (!button_transition) button_transition = create_bidirectional_transition(button, fade, {}, true);
    					button_transition.run(1);
    				});
    			}

    			current = true;
    		},
    		o: function outro(local) {
    			if (local) {
    				if (!button_transition) button_transition = create_bidirectional_transition(button, fade, {}, false);
    				button_transition.run(0);
    			}

    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(button);
    			if (detaching && button_transition) button_transition.end();
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$6.name,
    		type: "if",
    		source: "(69:4) {#if $isAnswerCorrect === false}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$j(ctx) {
    	let div7;
    	let div1;
    	let div0;
    	let t0_value = /*$activeQuestion*/ ctx[2].question + "";
    	let t0;
    	let t1;
    	let div5;
    	let div3;
    	let div2;
    	let t2;
    	let current_block_type_index;
    	let if_block0;
    	let t3;
    	let div4;
    	let t4;
    	let div6;
    	let current;

    	const timer = new Timer({
    			props: {
    				time: /*$quizTimer*/ ctx[3],
    				totalTime: QUIZ_TIME,
    				visible: /*$isAnswerCorrect*/ ctx[1] !== false
    			},
    			$$inline: true
    		});

    	const if_block_creators = [create_if_block_1, create_if_block_2];
    	const if_blocks = [];

    	function select_block_type(ctx, dirty) {
    		if (/*$isAnswerCorrect*/ ctx[1] === true) return 0;
    		if (/*$isAnswerCorrect*/ ctx[1] === false) return 1;
    		return -1;
    	}

    	if (~(current_block_type_index = select_block_type(ctx))) {
    		if_block0 = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);
    	}

    	let each_value = /*$activeQuestion*/ ctx[2].answers;
    	validate_each_argument(each_value);
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block$1(get_each_context$1(ctx, each_value, i));
    	}

    	let if_block1 = /*$isAnswerCorrect*/ ctx[1] === false && create_if_block$6(ctx);

    	const block = {
    		c: function create() {
    			div7 = element("div");
    			div1 = element("div");
    			div0 = element("div");
    			t0 = text(t0_value);
    			t1 = space();
    			div5 = element("div");
    			div3 = element("div");
    			div2 = element("div");
    			create_component(timer.$$.fragment);
    			t2 = space();
    			if (if_block0) if_block0.c();
    			t3 = space();
    			div4 = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			t4 = space();
    			div6 = element("div");
    			if (if_block1) if_block1.c();
    			add_location(div0, file$i, 30, 4, 824);
    			attr_dev(div1, "class", "questionContainer svelte-5qcsvv");
    			add_location(div1, file$i, 29, 2, 788);
    			attr_dev(div2, "class", "timer svelte-5qcsvv");
    			add_location(div2, file$i, 35, 6, 952);
    			attr_dev(div3, "class", "resultContainer svelte-5qcsvv");
    			add_location(div3, file$i, 34, 4, 916);
    			attr_dev(div4, "class", "buttonsContainer svelte-5qcsvv");
    			add_location(div4, file$i, 52, 4, 1409);
    			attr_dev(div5, "class", "timeAndButtonsContainer svelte-5qcsvv");
    			add_location(div5, file$i, 33, 2, 874);
    			attr_dev(div6, "class", "bottomButtonContainer svelte-5qcsvv");
    			add_location(div6, file$i, 67, 2, 2042);
    			attr_dev(div7, "class", "quizContainer svelte-5qcsvv");
    			add_location(div7, file$i, 28, 0, 758);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div7, anchor);
    			append_dev(div7, div1);
    			append_dev(div1, div0);
    			append_dev(div0, t0);
    			append_dev(div7, t1);
    			append_dev(div7, div5);
    			append_dev(div5, div3);
    			append_dev(div3, div2);
    			mount_component(timer, div2, null);
    			append_dev(div3, t2);

    			if (~current_block_type_index) {
    				if_blocks[current_block_type_index].m(div3, null);
    			}

    			append_dev(div5, t3);
    			append_dev(div5, div4);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div4, null);
    			}

    			append_dev(div7, t4);
    			append_dev(div7, div6);
    			if (if_block1) if_block1.m(div6, null);
    			current = true;
    		},
    		p: function update(ctx, [dirty]) {
    			if ((!current || dirty & /*$activeQuestion*/ 4) && t0_value !== (t0_value = /*$activeQuestion*/ ctx[2].question + "")) set_data_dev(t0, t0_value);
    			const timer_changes = {};
    			if (dirty & /*$quizTimer*/ 8) timer_changes.time = /*$quizTimer*/ ctx[3];
    			if (dirty & /*$isAnswerCorrect*/ 2) timer_changes.visible = /*$isAnswerCorrect*/ ctx[1] !== false;
    			timer.$set(timer_changes);
    			let previous_block_index = current_block_type_index;
    			current_block_type_index = select_block_type(ctx);

    			if (current_block_type_index !== previous_block_index) {
    				if (if_block0) {
    					group_outros();

    					transition_out(if_blocks[previous_block_index], 1, 1, () => {
    						if_blocks[previous_block_index] = null;
    					});

    					check_outros();
    				}

    				if (~current_block_type_index) {
    					if_block0 = if_blocks[current_block_type_index];

    					if (!if_block0) {
    						if_block0 = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);
    						if_block0.c();
    					}

    					transition_in(if_block0, 1);
    					if_block0.m(div3, null);
    				} else {
    					if_block0 = null;
    				}
    			}

    			if (dirty & /*$selectedAnswer, $activeQuestion, selectAnswer*/ 21) {
    				each_value = /*$activeQuestion*/ ctx[2].answers;
    				validate_each_argument(each_value);
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context$1(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    					} else {
    						each_blocks[i] = create_each_block$1(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(div4, null);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value.length;
    			}

    			if (/*$isAnswerCorrect*/ ctx[1] === false) {
    				if (if_block1) {
    					if_block1.p(ctx, dirty);
    					transition_in(if_block1, 1);
    				} else {
    					if_block1 = create_if_block$6(ctx);
    					if_block1.c();
    					transition_in(if_block1, 1);
    					if_block1.m(div6, null);
    				}
    			} else if (if_block1) {
    				group_outros();

    				transition_out(if_block1, 1, 1, () => {
    					if_block1 = null;
    				});

    				check_outros();
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(timer.$$.fragment, local);
    			transition_in(if_block0);
    			transition_in(if_block1);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(timer.$$.fragment, local);
    			transition_out(if_block0);
    			transition_out(if_block1);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div7);
    			destroy_component(timer);

    			if (~current_block_type_index) {
    				if_blocks[current_block_type_index].d();
    			}

    			destroy_each(each_blocks, detaching);
    			if (if_block1) if_block1.d();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$j.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$j($$self, $$props, $$invalidate) {
    	let $selectedAnswer;
    	let $isAnswerCorrect;
    	let $activeQuestion;
    	let $quizTimer;
    	validate_store(selectedAnswer, "selectedAnswer");
    	component_subscribe($$self, selectedAnswer, $$value => $$invalidate(0, $selectedAnswer = $$value));
    	validate_store(isAnswerCorrect, "isAnswerCorrect");
    	component_subscribe($$self, isAnswerCorrect, $$value => $$invalidate(1, $isAnswerCorrect = $$value));
    	validate_store(activeQuestion, "activeQuestion");
    	component_subscribe($$self, activeQuestion, $$value => $$invalidate(2, $activeQuestion = $$value));
    	validate_store(quizTimer, "quizTimer");
    	component_subscribe($$self, quizTimer, $$value => $$invalidate(3, $quizTimer = $$value));
    	let correctIndex = null;

    	const selectAnswer = i => {
    		if ($selectedAnswer !== null) return;
    		selectedAnswer.set(i);
    		if ($isAnswerCorrect !== false) setTimeout(nextQuestion, 1000);
    	};

    	const onGameContinue = () => {
    		sendMetrik("QuizContinueFail");
    		nextQuestion();
    	};

    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<Quiz> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("Quiz", $$slots, []);
    	const click_handler = answer => selectAnswer(answer);

    	$$self.$capture_state = () => ({
    		fade,
    		sendMetrik,
    		activeQuestion,
    		selectedAnswer,
    		nextQuestion,
    		isAnswerCorrect,
    		quizTimer,
    		QUIZ_TIME,
    		Like,
    		Dislike,
    		Timer,
    		text: text$1,
    		correctIndex,
    		selectAnswer,
    		onGameContinue,
    		$selectedAnswer,
    		$isAnswerCorrect,
    		$activeQuestion,
    		$quizTimer
    	});

    	$$self.$inject_state = $$props => {
    		if ("correctIndex" in $$props) correctIndex = $$props.correctIndex;
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [
    		$selectedAnswer,
    		$isAnswerCorrect,
    		$activeQuestion,
    		$quizTimer,
    		selectAnswer,
    		onGameContinue,
    		correctIndex,
    		click_handler
    	];
    }

    class Quiz extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$j, create_fragment$j, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Quiz",
    			options,
    			id: create_fragment$j.name
    		});
    	}
    }

    /* src/views/QuizIntro.svelte generated by Svelte v3.20.1 */
    const file$j = "src/views/QuizIntro.svelte";

    function create_fragment$k(ctx) {
    	let div2;
    	let div0;
    	let t1;
    	let div1;
    	let t3;
    	let div3;
    	let button;
    	let dispose;

    	const block = {
    		c: function create() {
    			div2 = element("div");
    			div0 = element("div");
    			div0.textContent = `${text$1("VIRUS_DEFEATED")}`;
    			t1 = space();
    			div1 = element("div");
    			div1.textContent = `${text$1("TEST_KNOWLEDGE")}`;
    			t3 = space();
    			div3 = element("div");
    			button = element("button");
    			button.textContent = `${text$1("ANSWER_QUESTIONS")}`;
    			attr_dev(div0, "class", "primary svelte-1hhibex");
    			add_location(div0, file$j, 7, 2, 222);
    			attr_dev(div1, "class", "secondary svelte-1hhibex");
    			add_location(div1, file$j, 8, 2, 276);
    			attr_dev(div2, "class", "message svelte-1hhibex");
    			toggle_class(div2, "isWidget", IS_MOBILE_VK);
    			add_location(div2, file$j, 6, 0, 168);
    			attr_dev(button, "class", "button svelte-1hhibex");
    			add_location(button, file$j, 12, 2, 362);
    			attr_dev(div3, "class", "buttons svelte-1hhibex");
    			add_location(div3, file$j, 11, 0, 338);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor, remount) {
    			insert_dev(target, div2, anchor);
    			append_dev(div2, div0);
    			append_dev(div2, t1);
    			append_dev(div2, div1);
    			insert_dev(target, t3, anchor);
    			insert_dev(target, div3, anchor);
    			append_dev(div3, button);
    			if (remount) dispose();
    			dispose = listen_dev(button, "click", proceedToQuiz, false, false, false);
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*IS_MOBILE_VK*/ 0) {
    				toggle_class(div2, "isWidget", IS_MOBILE_VK);
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div2);
    			if (detaching) detach_dev(t3);
    			if (detaching) detach_dev(div3);
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$k.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$k($$self, $$props, $$invalidate) {
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<QuizIntro> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("QuizIntro", $$slots, []);

    	$$self.$capture_state = () => ({
    		gameView,
    		proceedToQuiz,
    		IS_MOBILE_VK,
    		text: text$1
    	});

    	return [];
    }

    class QuizIntro extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$k, create_fragment$k, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "QuizIntro",
    			options,
    			id: create_fragment$k.name
    		});
    	}
    }

    class Sprite {
      constructor({
        ctx,
        image,
        ticksPerFrame = 0,
        numberOfFrames = 16,
        width,
        height,
        velocity = Math.floor(Math.random() * 5) + 1,
        radius = Math.floor(Math.random() * 15) + 10,
        position = {
          x: tweened(Math.random() * get_store_value(gameWidth), { duration: 0 }),
          y: tweened(Math.random() * get_store_value(gameHeight), { duration: 0 })
        },
        isGameObject = false,
        repeat = true
      }) {
        this.ctx = ctx;
        this.image = image;
        this.frameIndex = 0;
        this.tickCount = 0;
        this.ticksPerFrame = ticksPerFrame;
        this.numberOfFrames = numberOfFrames;
        this.width = width;
        this.height = height;
        this.direction = 1;
        this.position = position;
        this.velocity = velocity;
        this.radius = radius;
        this.isGameObject = isGameObject;
        this.targetPosition = { x: 0, y: 0 };
        this.repeat = repeat;
        this.yShift = 0;
        this.setNewTargetPosition();
      }

      updateVelocity(force=false){
        const min = force ? 5 : 1;
        const max = force ? 10 : 5;
        this.velocity = Math.floor(Math.random() * max) + min;
        this.setNewTargetPosition(true);
      }

      updateSprite({image, width, height}){
        this.image = image;
        this.width = width;
        this.height  = height;
        // this.numberOfFrames = numberOfFrames;
        this.frameIndex = 0;
        this.direction=1;
        this.updateAnimation();
      }

      setNewTargetPosition(changeDirection = true) {
          const diametr = this.radius * 1.5;
          const isHoldingVirus = get_store_value(isHolding);
          const minX = this.isGameObject && isHoldingVirus ? 0 : -diametr;
          const maxX = this.isGameObject && isHoldingVirus ? get_store_value(gameWidth) - diametr : get_store_value(gameWidth);
          const minY = this.isGameObject && isHoldingVirus ? 0 : -diametr;
          const maxY = this.isGameObject && isHoldingVirus ? get_store_value(gameHeight) - diametr : get_store_value(gameHeight);
          
          
        if (changeDirection) {
          this.targetPosition.x =
            Math.random() * maxX + minX;
          this.targetPosition.y =
            Math.random() * maxY + minY;
        }
        const length = Math.sqrt(
          Math.pow(this.targetPosition.x - get_store_value(this.position.x), 2) +
            Math.pow(this.targetPosition.y - get_store_value(this.position.y), 2)
        );
        const duration = (length / this.velocity) * 100;
        this.position.x.set(this.targetPosition.x, { duration });
        this.position.y.set(this.targetPosition.y, { duration });
      }

      updatePosition() {
        if (this.targetPosition.x === get_store_value(this.position.x))
          this.setNewTargetPosition();
      }

      updateAnimation() {
        this.tickCount++;
        if (this.tickCount > this.ticksPerFrame) {
          this.tickCount = 0;
          this.frameIndex = this.frameIndex + this.direction;
          if(!this.repeat) return
          if (this.frameIndex === this.numberOfFrames - 1 || this.frameIndex === 0)
            this.direction = -this.direction;
        }
      }

      render() {
        this.updatePosition();
        this.updateAnimation();
        this.ctx.drawImage(
          this.image,
          (this.frameIndex * this.width) / this.numberOfFrames,
          this.yShift,
          this.width / this.numberOfFrames,
          this.height,
          get_store_value(this.position.x),
          get_store_value(this.position.y),
          this.radius*2,
          this.radius*2
          //   this.width / this.numberOfFrames,
          //   this.height
        );

      }
    }

    /* DISCLAMER
     * deadline was only 6 hours... so, sorry for this piece of shit
     */

    let requestAnimationFrameId = null;
    let ctx = null;
    let canvas = null;

    let isVurusProtected = false;
    const unprotectVirus = () => {
      if (!isVurusProtected) return;
      if (Date.now() - isVurusProtected >= VIRUS_PROTECTED_TIME)
        isVurusProtected = false;
    };

    const getBase64Image = async (url) => {
      let blob = null;
      for (let i = 1; i < 5; i++) {
        try {
          blob = await fetch(url).then((resp) => {
            if (!resp.ok) {
              throw Error(`is not ok: ` + resp.status);
            } else {
              preloadIndicator.assetLoaded();
            }
            return resp.blob();
          });
        } catch (err) {
          preloadIndicator.assetNotLoaded(url, err.toString());
        }
        if (blob) break;
      }
      return await new Promise((resolve) => {
        const reader = new FileReader();
        reader.onload = function () {
          resolve(this.result);
        };
        reader.readAsDataURL(blob);
      });
    };

    const virusImage = new Image();
    const backgroundCirusImage = new Image();
    const virusBoomImage = new Image();

    const getImages = () => {
      const baseFileurl = IS_WIDGET
        ? `${BASE_URL}/assets/?file=`
        : `${BASE_URL}/assets/`;
      getBase64Image(`${baseFileurl}virusAll512.png`).then(
        (base64) => (virusImage.src = base64)
      );
      getBase64Image(`${baseFileurl}virus200.png`).then(
        (base64) => (backgroundCirusImage.src = base64)
      );
      getBase64Image(`${baseFileurl}boom512.png`).then(
        (base64) => (virusBoomImage.src = base64)
      );
    };
    getImages();

    let backgroundViruses = [];
    let virusSprite = null;
    let boomSprite = null;

    gameView.subscribe((state) => {
      if (!boomSprite) return;
      if (state === GAME_VIEWS.QUIZ_INTRO) {
        boomSprite.position.x.set(get_store_value(virusSprite.position.x) - 70);
        boomSprite.position.y.set(get_store_value(virusSprite.position.y) - 70);
      }
      if (state === GAME_VIEWS.GAME) {
        virusSprite.velocity = INITIAL_VIRUS_VELOCITY;
        setVirusImage(false);
      }
    });
    const setVirusImage = (isVirusHolding) => {
      virusSprite.yShift = isVirusHolding ? 512 : 0;
    };
    const start = (canvasElement) => {
      canvas = canvasElement;
      ctx = canvas.getContext("2d");

      virusSprite = new Sprite({
        ctx: canvas.getContext("2d"),
        image: virusImage,
        width: 8192,
        height: 512,
        ticksPerFrame: 2,
        radius: VIRUS_RADIUS,
        velocity: 10,
        isGameObject: true,
      });

      boomSprite = new Sprite({
        ctx: canvas.getContext("2d"),
        image: virusBoomImage,
        width: 8192,
        height: 512,
        ticksPerFrame: 2,
        radius: 200,
        velocity: 0,
        repeat: false,
      });

      setVirusImage(false);
      backgroundViruses = new Array(40).fill(0).map(
        (i) =>
          new Sprite({
            ctx: canvas.getContext("2d"),
            image: backgroundCirusImage,
            width: 3200,
            height: 200,
            ticksPerFrame: 2,
          })
      );
      cancelAnimationFrame(requestAnimationFrameId);
      gameLoop();
    };

    let runAwayTimeout = null;
    let lastHoldTime = null;
    const checkCollision = () => {
      const { x, y } = pointer;
      const distance =
        Math.pow(get_store_value(virusSprite.position.x) + VIRUS_TOUCH_RADIUS - x, 2) +
        Math.pow(get_store_value(virusSprite.position.y) + VIRUS_TOUCH_RADIUS - y, 2);

      const userIsHoldingVirus =
        (x === 0 && y === 0) || isVurusProtected
          ? false
          : distance < Math.pow(VIRUS_TOUCH_RADIUS, 2);
      isHolding.update((state) => {
        if (state === userIsHoldingVirus) return state;

        setVirusImage(userIsHoldingVirus);
        if (userIsHoldingVirus) {
          sendMetrik("Pressed");
          (lastHoldTime = Date.now()),
            backgroundViruses.forEach((item) => item.updateVelocity(true));
          clearTimeout(runAwayTimeout);
          virusSprite.velocity =
            INITIAL_VIRUS_VELOCITY *
            VIRUS_VELOCITY_CHANGE *
            Math.pow(VIRUS_VELOCITY_REPLAY_VALUE, get_store_value(replayCount));
          virusSprite.setNewTargetPosition(true);
        } else {
          sendMetrik("Released", {
            value: Math.round((Date.now() - lastHoldTime) / 100) / 10,
          });
          lastHoldTime = null;
          backgroundViruses.forEach((item) => item.updateVelocity());
          isVurusProtected = Date.now();
          virusSprite.velocity =
            INITIAL_VIRUS_VELOCITY * RELEASE_VIRUS_VELOCITY_CHANGE;
          virusSprite.setNewTargetPosition();
          runAwayTimeout = setTimeout(
            () => (virusSprite.velocity = INITIAL_VIRUS_VELOCITY),
            2000
          );
        }
        return userIsHoldingVirus;
      });
    };

    const gameLoop = () => {
      ctx.clearRect(0, 0, canvas.width, canvas.height);

      unprotectVirus();
      if (get_store_value(immunity) >= 100) {
        boomSprite.render();
      } else {
        boomSprite.frameIndex = 0;

        backgroundViruses
          .slice(
            0,
            Math.round(((100 - get_store_value(immunity)) / 100) * backgroundViruses.length)
          )
          .forEach((item) => item.render());
        checkCollision();
        virusSprite.render();
      }

      requestAnimationFrameId = requestAnimationFrame(gameLoop);
    };

    /* src/views/Game.svelte generated by Svelte v3.20.1 */
    const file$k = "src/views/Game.svelte";

    // (50:0) {#if $gameState === 'loaded'}
    function create_if_block$7(ctx) {
    	let div;
    	let canvas_1;
    	let t;
    	let current;
    	let dispose;
    	var switch_value = /*views*/ ctx[6][/*$gameView*/ ctx[2]];

    	function switch_props(ctx) {
    		return { $$inline: true };
    	}

    	if (switch_value) {
    		var switch_instance = new switch_value(switch_props());
    	}

    	const block = {
    		c: function create() {
    			div = element("div");
    			canvas_1 = element("canvas");
    			t = space();
    			if (switch_instance) create_component(switch_instance.$$.fragment);
    			attr_dev(canvas_1, "width", /*$gameWidth*/ ctx[4]);
    			attr_dev(canvas_1, "height", /*$gameHeight*/ ctx[5]);
    			attr_dev(canvas_1, "class", "svelte-3wbuot");
    			toggle_class(canvas_1, "isBackground", /*$gameView*/ ctx[2] !== GAME_VIEWS.GAME);
    			add_location(canvas_1, file$k, 58, 4, 1636);
    			attr_dev(div, "class", "game svelte-3wbuot");
    			add_location(div, file$k, 50, 2, 1315);
    		},
    		m: function mount(target, anchor, remount) {
    			insert_dev(target, div, anchor);
    			append_dev(div, canvas_1);
    			/*canvas_1_binding*/ ctx[7](canvas_1);
    			append_dev(div, t);

    			if (switch_instance) {
    				mount_component(switch_instance, div, null);
    			}

    			current = true;
    			if (remount) run_all(dispose);

    			dispose = [
    				listen_dev(
    					div,
    					"mousedown",
    					function () {
    						if (is_function(/*isPlayig*/ ctx[1] ? startPointer : null)) (/*isPlayig*/ ctx[1] ? startPointer : null).apply(this, arguments);
    					},
    					false,
    					false,
    					false
    				),
    				listen_dev(
    					div,
    					"touchstart",
    					function () {
    						if (is_function(/*isPlayig*/ ctx[1] ? startPointer : null)) (/*isPlayig*/ ctx[1] ? startPointer : null).apply(this, arguments);
    					},
    					false,
    					false,
    					false
    				),
    				listen_dev(
    					div,
    					"mousemove",
    					function () {
    						if (is_function(/*isPlayig*/ ctx[1] ? setPointer : null)) (/*isPlayig*/ ctx[1] ? setPointer : null).apply(this, arguments);
    					},
    					false,
    					false,
    					false
    				),
    				listen_dev(
    					div,
    					"touchmove",
    					function () {
    						if (is_function(/*isPlayig*/ ctx[1] ? setPointer : null)) (/*isPlayig*/ ctx[1] ? setPointer : null).apply(this, arguments);
    					},
    					false,
    					false,
    					false
    				),
    				listen_dev(
    					div,
    					"touchend",
    					function () {
    						if (is_function(/*isPlayig*/ ctx[1] ? resetPointer : null)) (/*isPlayig*/ ctx[1] ? resetPointer : null).apply(this, arguments);
    					},
    					false,
    					false,
    					false
    				),
    				listen_dev(
    					div,
    					"mouseup",
    					function () {
    						if (is_function(/*isPlayig*/ ctx[1] ? resetPointer : null)) (/*isPlayig*/ ctx[1] ? resetPointer : null).apply(this, arguments);
    					},
    					false,
    					false,
    					false
    				)
    			];
    		},
    		p: function update(new_ctx, dirty) {
    			ctx = new_ctx;

    			if (!current || dirty & /*$gameWidth*/ 16) {
    				attr_dev(canvas_1, "width", /*$gameWidth*/ ctx[4]);
    			}

    			if (!current || dirty & /*$gameHeight*/ 32) {
    				attr_dev(canvas_1, "height", /*$gameHeight*/ ctx[5]);
    			}

    			if (dirty & /*$gameView, GAME_VIEWS*/ 4) {
    				toggle_class(canvas_1, "isBackground", /*$gameView*/ ctx[2] !== GAME_VIEWS.GAME);
    			}

    			if (switch_value !== (switch_value = /*views*/ ctx[6][/*$gameView*/ ctx[2]])) {
    				if (switch_instance) {
    					group_outros();
    					const old_component = switch_instance;

    					transition_out(old_component.$$.fragment, 1, 0, () => {
    						destroy_component(old_component, 1);
    					});

    					check_outros();
    				}

    				if (switch_value) {
    					switch_instance = new switch_value(switch_props());
    					create_component(switch_instance.$$.fragment);
    					transition_in(switch_instance.$$.fragment, 1);
    					mount_component(switch_instance, div, null);
    				} else {
    					switch_instance = null;
    				}
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			if (switch_instance) transition_in(switch_instance.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			if (switch_instance) transition_out(switch_instance.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			/*canvas_1_binding*/ ctx[7](null);
    			if (switch_instance) destroy_component(switch_instance);
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$7.name,
    		type: "if",
    		source: "(50:0) {#if $gameState === 'loaded'}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$l(ctx) {
    	let if_block_anchor;
    	let current;
    	let if_block = /*$gameState*/ ctx[3] === "loaded" && create_if_block$7(ctx);

    	const block = {
    		c: function create() {
    			if (if_block) if_block.c();
    			if_block_anchor = empty();
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			if (if_block) if_block.m(target, anchor);
    			insert_dev(target, if_block_anchor, anchor);
    			current = true;
    		},
    		p: function update(ctx, [dirty]) {
    			if (/*$gameState*/ ctx[3] === "loaded") {
    				if (if_block) {
    					if_block.p(ctx, dirty);
    					transition_in(if_block, 1);
    				} else {
    					if_block = create_if_block$7(ctx);
    					if_block.c();
    					transition_in(if_block, 1);
    					if_block.m(if_block_anchor.parentNode, if_block_anchor);
    				}
    			} else if (if_block) {
    				group_outros();

    				transition_out(if_block, 1, 1, () => {
    					if_block = null;
    				});

    				check_outros();
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(if_block);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(if_block);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (if_block) if_block.d(detaching);
    			if (detaching) detach_dev(if_block_anchor);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$l.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$l($$self, $$props, $$invalidate) {
    	let $gameView;
    	let $gameState;
    	let $gameWidth;
    	let $gameHeight;
    	validate_store(gameView, "gameView");
    	component_subscribe($$self, gameView, $$value => $$invalidate(2, $gameView = $$value));
    	validate_store(gameState, "gameState");
    	component_subscribe($$self, gameState, $$value => $$invalidate(3, $gameState = $$value));
    	validate_store(gameWidth, "gameWidth");
    	component_subscribe($$self, gameWidth, $$value => $$invalidate(4, $gameWidth = $$value));
    	validate_store(gameHeight, "gameHeight");
    	component_subscribe($$self, gameHeight, $$value => $$invalidate(5, $gameHeight = $$value));
    	let canvas = null;

    	const views = {
    		[GAME_VIEWS.GAME]: Hud,
    		[GAME_VIEWS.INTRO]: Intro,
    		[GAME_VIEWS.AGE]: Age,
    		[GAME_VIEWS.LOSE]: Lose,
    		[GAME_VIEWS.WIN]: Win,
    		[GAME_VIEWS.QUIZ]: Quiz,
    		[GAME_VIEWS.QUIZ_INTRO]: QuizIntro
    	};

    	onMount(() => {
    		const { overscrollBehavior, height, overflow } = getComputedStyle(document.body);
    		document.body.style.overscrollBehavior = "none";
    		document.body.style.overflow = "hidden";

    		return () => {
    			document.body.style.overscrollBehavior = overscrollBehavior;
    			document.body.style.overflow = overflow;
    			document.body.style.height = height;
    		};
    	});

    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<Game> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("Game", $$slots, []);

    	function canvas_1_binding($$value) {
    		binding_callbacks[$$value ? "unshift" : "push"](() => {
    			$$invalidate(0, canvas = $$value);
    		});
    	}

    	$$self.$capture_state = () => ({
    		onMount,
    		gameWidth,
    		gameHeight,
    		gameState,
    		setPointer,
    		resetPointer,
    		startPointer,
    		gameView,
    		GAME_VIEWS,
    		IS_VK,
    		Hud,
    		Intro,
    		Age,
    		Lose,
    		Win,
    		Quiz,
    		QuizIntro,
    		start,
    		canvas,
    		views,
    		isPlayig,
    		$gameView,
    		$gameState,
    		$gameWidth,
    		$gameHeight
    	});

    	$$self.$inject_state = $$props => {
    		if ("canvas" in $$props) $$invalidate(0, canvas = $$props.canvas);
    		if ("isPlayig" in $$props) $$invalidate(1, isPlayig = $$props.isPlayig);
    	};

    	let isPlayig;

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	$$self.$$.update = () => {
    		if ($$self.$$.dirty & /*canvas*/ 1) {
    			 {
    				if (canvas) start(canvas);
    			}
    		}

    		if ($$self.$$.dirty & /*$gameView*/ 4) {
    			 $$invalidate(1, isPlayig = $gameView === GAME_VIEWS.GAME);
    		}
    	};

    	return [
    		canvas,
    		isPlayig,
    		$gameView,
    		$gameState,
    		$gameWidth,
    		$gameHeight,
    		views,
    		canvas_1_binding
    	];
    }

    class Game extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$l, create_fragment$l, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Game",
    			options,
    			id: create_fragment$l.name
    		});
    	}
    }

    /* src/assets/Logo.svelte generated by Svelte v3.20.1 */

    const file$l = "src/assets/Logo.svelte";

    function create_fragment$m(ctx) {
    	let img;
    	let img_src_value;

    	const block = {
    		c: function create() {
    			img = element("img");
    			attr_dev(img, "alt", "logo");
    			attr_dev(img, "class", "virus svelte-u4a96f");
    			if (img.src !== (img_src_value = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAL4AAADGCAMAAABb28oDAAADAFBMVEUAAAARBQURBgYMBAQoDQ0cCQrLtbYWBwczFxfRrazOGBXuTkmgBwU5CgnYcG3qQDtYDg7To6LUm5rRhYO3Dw7ZJR4gAAAqDhKbO02gUWKqcIbFYGu8TR+MSlmqKAtYEBKAFRBbEg56Dw5mNklKFBglAgOxSlq0PE56PUyBICxIBAMoAQF0XXpfSUqACgeaFg2tHCQrAQFbZYZUOEuwKjedBwSKFB6xQCHAFAVnDhCKBgQnAADHpaVUDBGoCwWbOzG0lZWehIN+aGfVLSelKgu8MSmAQUFXM0SaYF9JKjpsBQi9WVFTUWhtaYSVMEF6QFSKWGe/cnOuLTHj2Njf1tbh09NAAgHgz883AADSyMkuAABtBQHa0dLby8vLCQVTBQK4BALVzc5KAgFlBQLDtrnFsLDiuLlYAwHNwsPCBQLLvL3NFwGMi6fCHAFfAwHmIRrbxcbrMivVCgSsBALVwsLZe3ffBgJ3BwHWvL3Dqqriv8CClrTer67XtbW5qa3lFAzCu7/msbKJgJ6VDQPEo6NdFxHaFg+8sLTdp6XVkI+5oKXFMQmLCQLjyMjel5bIJAPinp/CFhjop6eBCgOYnKp+epiFN0C7PRH4XVnyJh7Sa1xtFgr2QjyEjqzFkJDijIvMLSnqdHLpV1TGRxnLOxDULQZ9haS5lZ74MyzUVFC9IyjdhYLdYGB4Gg+roar6UEvJUybCmpiBcI33amfgR0i4dXTogH/wFQvtYV6VKhXrmJioY3WIHAnnamiblZu/XDG9JwLCMTvbMi3tkI/dOzrEe2adGw3yhoW5hoXEhnqYKjSbfpiUi5NxLDFaJCyRaH6yXmbRSDh8U25/Y3+XTUPHZDyomKD1eXfLQEdlQlrDb0+lkJCPd462Sz6AKhl8j6+4h5hpUGt6SF3ETlrXY0bYUijXQxVrICKKXG6cPCBmgKOsepFcLjvLaH7bfUOZiqa7a11aWHfXTl+4XklyaYeXZF7bh1hndJS/eIyHOSjcaC3ZknCWen1YSmbZnYesPTbqVrK4AAAAU3RSTlMACxwSOi7+JUj+/v7+U/7+dv7+/vz+/mH+/v79/v7+kopmof79ef7+/v7Zmf5ss7f+vf79/Of+/uPOzOLgrc+vzbON2dnCkb6oj+rQt9nZ3MLU4ocf2xYAADTQSURBVHja7JlbaJJhHMY9fOan5mEeCCpKvairzS4aSVRbLcdgEBVEBDUWlHRgONscJo4hxXCMmfOmzyHzUJA7gIWxiTBqWUh37SKMIAhc3dVFFxEF0fO+On3XZdhh0HPpp/X7P+/z/t//+030X//1X//1X/+MxBK5TCaXS8WijScpp7MeOmS32zscZotctMEkO9hq7BkaWYymY0JG6+BFG0rinTc29wLf5c5ki0Uh5thY/kv2bx6YVI6ORKMxoVTKpN0by37J/oGB2YTJFXXHMoKw4fClLZsHll89cbu1KgH4mQ5OtJEk3knwn2q1qlQumxUEh0S0obSF4D98mMrlkqVSNtspZpPF6cw6Cyf5h88DrtW4XJ5J5ZaSpWKplLSwj2x2lyvqMjnMnFT0T0gq49VqmWTd3gX+p/ml5EIR0nDMkxZ6HkSj6XSH+R/YEjhfbXa7Umm36eoFiK3G5U8Ev1goFIonmMosRuC7KH461qEW/V1J1VZ7z7VroyMjrkWXuR5nHfCJ+YV8Pl/oEjOburfnGsFPZ2Io4C+3JBmBh0DviqY7+LrJOxIz85qFeCEfyec7mZ7U0nuZHseqbCkrZGJtfzP/8pbLoK+YT/w0157wrYkZjWcuno9EIvlm9kS4jJ8MjQC/WChmMx1/bpwQy/n1zUJqNVL80VFqZzbWVksJR/HjoXAkEAnx7J7uJfhD+H6p+DO+mON/WzeSIOUmk1kiYrchrOwB/SihKWTTthq+7Cjw5+KhSCAQ0HPMelF88I9gGI2lozYGV2q2owH8lvFazNuUyEfabRYz7aW3iu9yaVPJXCxdT7Kc4sP8n/BlreQ3wB9aJDKxrUdn7MGnboe64SsgN9uHXFoBe62DY/AnjUgPxXdrtelo2rEOn2Qn4HQGDshENfFV/JGREYJvZUjlhy7j88V07Km5wQvAtfSAPoewCqpOpjvu3r08O1nhp72wm8G/M+MZCwUDPp+Txd8yQPCvkoIheMFGkazKYjQjqNpkjaTn96NXA7+QL2az3XVrZMfK5eVJGmUcpG6V0C1h8Kc8Y+Gf8aU7K/imJ0+0Wu1Di5jZW+hio1iTaCxbzDq4Bnq/34h/mLifzxfZAUas20vc7yH4mI1z2RPyWmXAN4TCfqfP56vjy3e2VumfYp5LtUlYj+w9SlNlEUtFwdEo/2HL7KwS+NidhUi+WOyS1h9ZZ2cnSfpNTzAbL5U83Dp8fdDvnJjwHeBq3t8g5vcpQZ9K5bbxbG+w9imxJkgUuR8LjTvP1HvLy8rRUbc2tVTEGVQw8MzCnAT/pNJEeDAizKlrDxh8xRq+unUz8PtuJVZm5ueXljpZQu6Q0vT0qRZKU7ktjaHHBt32KgFC4C/kCT+xn43PbCLx5OHM/NJCsdBci8LR+1NjwPdNTEwo1urduXlzby+lxziU1Kwz33zLRAOlUpHjAEKHbogsu8vLy4mKvwUconkDzyYL/InEysPKfFarTL0P+E3+YYLv5dfGtT3G3r6+cdBrPAsLGjVr/lEUlZrP5XKkgDR6qrlRPd+2DL2i+PF8IBAJd0mYTtg6C/tX6HyWD7fL6/ga4Dt9E6cnvM1r8d5j7IP5hH4uHk+ZWfPpkhDlcvA/umjnGtZ5bKB/9Qp5hcER8OubWfuNxkmKT+YzPb+2ZLfva/RNw87+0yz+rLHv1t2VKUIfiscYfPXJlfklTTK5kExSfrcdS9NA/lfl8swn4nA4AP52jrXf2DcOR4EUjkSOVBOru/3So1cMe4F/ur+OT6ND6UMLWguzwCskT1TgT6kcanFDx/rd5XIZ+J7KEOZHxpkZkgRiqjJetkuqYbjz0kDxT506PVGtSdoySc33zI2FwuGkQ1aPzgqqR54gwp9q4xo8bpp3lbdtI5mF/VBTM3uxYvCrXUZqu//a0KTwDp4D/unDlWIlNtDDfEqfn68fueo3ZO3ioXweN8uF5HxjZwbKo9u17cM2uuxBOobVuw+P9MBTgu8POOE0nRnuPxtrGgb+afAfkFe7y61x8kXQB4Px+pHLOfBj/BoC/4KmE19vPP+LDx80HgrphNplzOZFoglVEPiVCcGy/eVzvaKKf+oKX4nI+N3371Y9c4Q+eIKrBb8N9IDHzgnDf08nravh/J3bPnzwkNwGCf/wYUnt9l3Hx+fN9BpGzKf4586ePYX0VKY40JPoBIP+evqkZkofjEDgj6Mr/xZJOj98MXgMY9R+n09xRFqbBfrGK+MxeYCyEOYpw3STYpN38My582fPnr3A0xW5/3UVVRJ6f7143WtPHM770RCAH+r6bZdf+fEvXwyGMcwCdJBUrBko39/3+M6MxqCn+Jt4hPnllMegr+AT/8+3c8jI6tQU6PWgD/ib1+jV3aDHJ1SRMDLVKLc5GfxlxZ+g/PomP2KBQZivDYuP765Q/GGfb/Aw1/ZydZX4jPQPDvajgPNn2jndChkntbk5eO+s7Xy+2xOi9E4nwZ9WNwqevDuzrT89xM1f3k4Dn6a6v98LT6ut8zHCbxgjZfX3b2r/9vHj169fP378/O07/eaZM2e83QkX7gQoIJYLY+NXI8J1z1F6JwR+/ZH1/5/41w8qI7lhdaA5szV1vZ2eHqvyD3pvHueqrfPx+P01fJTV9O3zRyJaAFkT77DHheuhyo1JUsgmI77qScB1zYUoPMIIfLIj6pJYzGa15NfoW4z0ZRJ5ccaKOwH+Cr7Xq3j+2saL6SX78fidVQ96DcE/1+9VfP/27fPnSgGfUcL3KdBrU9T9VLZU9FOXpfwJwPspPMVv51gE22g0rXVYpL+QHOvAZWK+KhNzyNbH5+296enpm+gqVzbdfPZo+9GdMjGmAbSel5oqPrI+6N2kaPqOGgBPSvhoGnK5UloXoi8kcd9PNovEUu7IAThfYZ8g+OgF7Jw7tJhOZzIqs/QX/jjSC/xRbTKbYS+kkOQ44X9wU3Fx08Xp14+2396x36pTWx+PY++OVQ6q8+g0589cuuQdHlboUQO0Si7D7qgqiVlYJWRLya7mI4cPDAOdskMEn42O1NoD/IzwC/zI8kAvXiC4UoWSILB3Oojfeu8e8G9ev3h9+hnwb9/eAQEf4aczGvok4cd2vTToRQnBph+s2wtQVFUYB/DZhYUFhAWBQR4WVIKaCs1gpmOaPazUygrGaWDJgjWzElHWXAU1RZ6JkgmIK/JQCBZCWlYwIJKHsDoWqYipKCNaOVYyo1NZM9L/++7e5Qo0DdbfdbW0md/59uy953zn5pZWg66gKk5VWKjHWZ1eX2VsiV+13RqsqeGnq5FkF+exem0tzvXIj+/fKIKRj1njkRWV3FzXjoOdbu09ftn8XvbrdLpqM8o/Y8aMgYHye/hh5OcBxMfHYwCJY1vXro2Lo9YO+ggwGY3t2MWI9nCsisBPlF51nKdHEd9oNMKP799oYjvHI6Qc/DinujojTncM8+0lB1H2mD69+aXwp+d3mPo7OzvrO3syRD4mT1hYGPjsxwDoE3ADP5b6anF6PfTwd28PX8IB/vXXmY9rqTXKOdyJ0xsxUnwAozvZcw0JCSV+LPVu0AduSVsY/NBg4zGg849efALpSGlpb++NGzfqwS+mvS3zIwW+1I/JQ23BPahHHcqZm9vNNQceegTTx26qtH5oBsG/x9iea+TpM6ovbkgo+HTlQf0L2+mAxKV8uqvotw++ZfoDfuLDf8OnsycnI6e4S8t8/uqK/Lcs/oq15I9FH0HvVAV+VUs4B3ji0+yRfG+Vs94Fn8uPJwrAR/lHww9F7xL8tcxH/wCLWcO56a4y8dOZ0cd+AY+pMyMnp5jmDvPfYDnjhfLDr12LRlyyCk0ElRPPh2VAi3rmYz9sXQWh9gifAtS1dONv5zo5j6YzElgfSNVfHUvdBfA/QTLzXnO2NjV6wO9lvEnQ5/h1tWGJyYs0sFnPfsrydYbk1WiE0mTEOTUeEmiHGiE9BzsahXVfgT4ih++bxm4art51NCvLuUGhHvj0kj0tzZGdCUiqu7iHkwczn/CmzkBB39RPNwM7uxXxVH5mi3rcAtw9wcf0oYsPelBVVPwwYd7gV3zXw+fZiIuV6UuXwo+fUezPRfVx6jS6fngI/eeOnmieoTmCDgItCBPcaC3OF9a+PyjAC/oZPbf6O3Av0BAffkmWk36FlvmUWkT/CeAI4xG8RQpXTRvnWUutQfnh36NH103vLxvdcnMWujdZ6J5xS4k3KIjDPFteq8z1Jf0vvr6W0t/qN2EqJemIv2IFo/nFgd4h76DIh3/Pl+FhZA8LD8OLw3w+BBHta9aI/to999HwlHtNCy0Hn/37M3mDglXaU1Pl+LO5Jqq9b/+tHj/g/W6ZOnpxHQIfC6F1KyBntYhfYZf23cH3rNWPa1+Cggt+xMKfh5VTAL6zIp75PH8wgNpg+f0cOL+G5lpZWZ4L72+xGIvHUmzeVLliMfRt/ee7mvz8/A41kR4X0Y06S/UlctDxcnD5gfgkwbW4cL2FzPpIS+bJbXFEDL7Vz9MHWb3aMVhxfyfmc8+xX8s7LPgxgBVJi+f6/mLuMLe1nef0mzr4HgB9TAz7Cc56O0Rjp8svO8B81tftRPGF8J1LTUvUt9Sz5z974L0oq57C1Y96dw7dMe8vti/3IXmCn/ZN8Ynac46OOI2oyK+uzu8wdyD5hN+4caOFj1DdN2/eDLtGo0vKz/vu4A7iJycfjCs6vD5S5LN+fUIqMtZwFZ8QrpYWuuj3CHbFgvw+g2vM0T7fPvTXtJYt4tgyR1TEszk5tiZNo6uuri6tLhX0KSkp4McQfzP0MYidRge8Oe+7AzveAx/6H64W7lRb9ZHMv0ubgqt//jCEP2bMmjXTH8Ju67/EeU5okC/SZnbPh9+9mfSOnp7Jgj8pKb2UKw8+EsP+zQh+g8onVQPfdQhTB1kN/bXG/QlUfGn1U3/Dzvga6aPu5U+fNopZI1NOmGArG/ZvvWaFBnX6IuR3L4Ad8YQfdzQ3qq5EDz4CPv/KlQf+B+g5Bw+ixZa5DnwxPPUT/sK+Hnr8rSgpf5aXfASlvXMAlMMim7AgImLyo8O/4grXh17urPf1NZnN7n1ZURZ/sifa2Fqa2jTpgWc9XkI0MRodKn++61DxAaseU8dlf+oq5r9u+eqCv+zub+AfZD77MQCaN4oR70dzkmupfTA0zgs+RCIme9mM9LzR4noT4otTOIHfnNzs0lhYNBZKnW5jOvM3pmyOSWF6iiYpqdrcZsFj3ovFz9NmJqiZzwNgvno9lf8aV5/9fPXB+nakK2Fwcy1ue8O3L/YzP0Z2fRix4EmlbKSb2GKsb/rK2c/Vr2ksNDg1apgvRPwANDqLnfAZhJcWPxF8wnP5wafyp4p+6wBmOQ9HyBT+z6ji9tTiqYFh62fXMZhtW2kA0ZMnjHSpsl3s48OPAJQXOFJoLaQqSuPpY/XjLR1zBvamQ8XFBzIydpBeOvMT1oEPPCLww4f6EY9pihHwrgsrinBoh+3msO2LfNqarKyjoce20geAAYzw7VAs9glC8bOOHmV+RZlLY5yTwU3gY+OCVHeYQWe7gP/ggw+GFn8V8/GSzH74Db+xn/OEv3z4/J2/0AWnduz/zNjN5ZfujD0cC3B8NXCsoYEH8KiXvWyYPyg0KCur/GiBY3lWQVlNRU1sDY6v+PLYYTb19/eTvOmQn2D/Gnarfgf01/IMKP6qSJo8vF62zB741y9L3U9+jt/QaW+jmPrCqwYcGhUK/s+MLUXOQ7aHOB08V+bb13P9CPkjIhbMfNCZRyC9BQcFHs3ywCcAfk1ra1xskTbTzUFDM/1WU5MfQvScDNGOsB14PpdIS01cJ059HoDUf/e3X/9Eri4MkEllclvY03Dosp/8dXXEr2o3zpVJxzcNfEecHmIJPHCkoRKXIB7BkxOUctng1/floJCg8qVLs5CCZk/PTXEV2jTit50HPofC9K8Zz9mBHCQ862nZBD7rOdBb/QmZ3Bg1YE0r0lF2NLPo0AWnLuxH+Wn2DHmqWPFyqMfSrHLMHt/OzjPZlyrBj46IwJWUh6Cwl9vbuj7/k099YGBQuQfxoxzxBFhrhYH4OvN50oMtlTM+4wAC+zWxXU589ku+vch28qdm3v3rbqJ6+VNPK20VCiXR3RKoa46wn/kq9Ir0Va1KCV/pPQ5LjJDygaBAtG28z9yp3BsdvTKa/DSEBTNnzrwz/qeLJ27U18Nf7iGsxD3LXNBU1mk01cQfggc9A/ji4kOHuq7ymk9swUEcJupRf776sB8DSE1QY0TqFXazZzskLuN8hJBfwq/dc+/+xdl73NatY8YcCzk+MC7Q2xv13xu9cmU0fQDIrl0NDdB/e/HmTR+UPyRE2Ek44ogTqCTw2yx8AY7fSOltWnd38UwAfEn1kXDms38VtTmFezL+mdue60U/8/nE14n4eIZYunf38j6+lXLkyPHjx7Ozsyed/HHvtm3R8BO/svJSNunBv1kfFOIh7ITwVe9zcaduYUo68zkszylGDsHelUcrJYob6VF8Pmgc5HP5xflPUYeLiznyI+xnvlD8GlVc7KYhD2w4jzu2devHjyPQU06ePPkjsnfv3krgJwn6Le/Df8I7NIT4HxDf14xeORYNJpGfkZPD8Kamrq7zbW3maqzbHBzcgHdI5OKrI6mzBrXFzw0HCvzq7dvVSyQbGdFv0RsON1YU1bQ2e26ilpfrPYcRY1D7xx+fNGkS4SciDyM0iEknT2ZPvPLT6S9I/z5y8wS+waF4iiTnaE+/yZxfnS7yc2jfa5Wbq5Oo9aBxQPMHnXLLKRfhqcCinfXMR9T4af1D9gv6BBw44mkBF9ZjrQ7+vc+bOE+bjuKDz/grnO+/n/jwRMqVU6dOf/HFli2wb9gH/80TNAI8I4P2iIn2WqXgsx308+fbzNw/12lgx9YdcGqT82EYTx3wiShplYh+iZ794EOPDhP0Bq1LRdl3wGPXhg3kkEWnXOk14cEHn3xy5sSJ48dfuXIKZLxxgId+N/TIPqr/lhMYQGdgUA/7e0uJj8CO6QJ4jBDgETvo16HNKRaf/RIlX4DYbx2byEfxoU9lPeELnpgzJ3jaQ/6uAYqRlvwyG7m90uvF5185RZlCL9gZv5uLz9nyPvFxEQ0MtPh7TVgsNMGOiUQ7reW83cI2nXa9gBOd9VR9af2lXwAxw/Rc+ryy5+b6B9jay21k/3LciEMnL4zg9JTTU6ZMEfBbSkr27aPKMx+h8nsHIuzvwIKnzdSRnhKzmYJ3DACFJz7ob7B9FfPFEqP+Ejsv4CR6jIamDusZv9A/QDGK//cRI1gEOuw8bfZx2M76i1suXvQeGLD6kVLsWcjOhcdeHXKqfTzxQRdqb/XzrWtIIBfspAcfeiq9wWXhfFp9jTJy5YuLLPoNGwR/SQnoHOjHXX9gYCAI87+Hm2xY7VvweNnFgA89dcfxosrTjyWRS5B/mj74gdCvgh781ExD21xM9Pt8XN9r0UuY9fs2kP+yoN+9GyO6+K338esPUEJwtNXTjy6bpdMg8DWY98SH/w31G/FqMYQHf+T6i99n6DHxufhjX52v/C+dErly0e4S8OG/fLlkN4LP4/Tpn8YfP3IMGUMjyJmB8pfSZgtbXWoyEB8hvtAsx81KTaUnP0K1Jf7rQ+rPL8HP/ISn5t9n4aW73Je4+hsul5SQ/fbp06fGZ99paMAaCVkDP8rPU1/Y6WKrDj77l8eDj0SyH3zYKcCBj4QNkzNe4CfOUwL/n6Mk/2Uq/he3b0/B/SB70qWGho+3in4u/4XSdOg3p8RoOHYYAPc6+YglEi8EMx8vCvOti4ew4QPA1F83T/E/PQb5EvjQE/7KxJMnL1VW7tr1Mfxi+Xn2oF3FbRId9BgEzx6x+hwuvhgAhfvU4MZXMnWI/9To9SPfEWRTLyO7b5+6AvyPlZXYjIEPPw8A+hm3OsEnPwI/FgwI+8FnPR+1W2a/Gi8AOZHwWyJe8YXB2U0drd12wqOPTlCOcImVL7pc8vsU2LGORiI+lPiP0dzvNHVcSBf9OoT6P1hpYqG1M4HkHDXz8QMB0HoxkjY917fQOX7LRzgnld5IJbcsLA1sRuzGzox45823H5lMQ7ARPwcZryRevD3l+4dReMYjg/5jxx643tMJ/tmzn4OPpGzUYdmD5q17RXMsnSUeXmb1I6zGo7V4E6PmAaLfvP2jbj0/NJBrbH/aZrCr6R8c7K+UCRv3AP/gZ54LkA/TK2ZOqox+503K5MmP4XOg4PN49LHHJn/zM5b/ol30gw/9kesD3vU+PhfOfv457KTfqEtPQpMwzzN5E/GL6goTpH5201v8Ol6J4sxyGQVPphfiwXFBn9vt/sLUqUpb7HhfWFhB7YVnXG2dA/yfeyYWf2X4QybolWCxH4EtIucdhIaCN/xm5bZt22CPjrCGd5DIkSPXr5+p97lwAcWHXxgBZlFSfllyMvzNNXis4HD3R4P8SLr/WuwJtJrcmZmJOUb7QBX1L6uc9FVVRpS/JcHBYfbs2W78aCr2iS6+eRVOKtooYnxD22xoNtAmEQ2Gway0JppzL5/wd46fOYPan/jqLPhIOuuR0l8cVyc3o5Hb6OTkdLilfb2gf4v4sEOfmAh7Ztp+g0Gr5faTKq4WUeXW4RjU2G3s7m7B3l0cII+vAtt0FF/f3T38KQFn6MdnVw4KwZXipXza/zZcupM9DnvIb7H4vAk9/BzS44GZcwWelOYiFX60H94plh/TJp4r75DqNhZbkLyrFCeVKg5b2LW1sU7UyPlMT0fphZ8QnvU0SN4pxm1C8fGQUVWN7ZAerfd4b/A/hI1iGYGVv9Ki5zfavPPuHRtg7N+/Ovsp7DT5LfjS3l8KCgpaW3EKUNPaqKprPFy4ip/SgJ/5ibgqjU3T5l398xqntbVG1Qr/pj1FhUVOOHjX63Nz61qgh50+Ii3ttbBTjAU/FxenKlXAkKn/d3tnH53lHMbxszfP1qyWLc9mHo9ss7aRrWVU5rVjyrs/OgijkNdlNHkPEcMhqmFLZ/LaalTixKg2K5q2VaZWCoWzxbxUQhQ+1/W773v3s54/jCGH7zaL5zh9ftd9/d6u3/W77oQE7J+LV1jSNgC9h+3ZvhN62MxqVFae733zzdSpa6ZOfcnSmhc+QUkVPSrqZtURycJibxHcu1zxyROQRajSxxV/9rlGNblVsm3btgULisaSKblQ8Yumvxj9RPTTAl9SUjJbNorQm1362GhypKZPj90D3+crzWU8FL2pYoYqN4LalsA3bGb3q6t+PGcqwvRGWF70FWHoilW1kyYtqJtA750ZfT/TF/yw4zpRvaP2K4FeToT61G3b9tUc+Ama1o0dGx1dJLdvFpLyNv0t4GfPjsPymJ59boXu0if0ovbAiy8evAd+fUJp7kMPMZMyHIo+EtEEbYTL9A39Vr+vy2asv0KMj+2n2saX9KRPXviq5dEeaNKkOXNq60hi6zUZeCs/7Kp9CJwo/XOodttXKrH/AvB7cTfv7YVPL4xeWLSwZLagl5XBvqqCYwWO98CXbMQng+DDXz+RLLbKgRs3pjWLSkt3Im3EeFUV8KWbD8NvkG5/YYdevvhe8+GXqhd+/LSyR0UPAqGzesLE6H+bk13FJgzbx5VBL+cRWN8I/G19JvR6KxpnK4p+euHMopnFFjrselYgkUlNMpr+ZBH4bsWCT4htXwKwEuiXDLXq6h0iH06186OqKaiqKr1U4g7TkIPvNGDNmg8PQODj/NwmEoP1WcDUSz7o7aCLNFHgshlxZavsw5QejE+1daLaukk3jqW0g+TakrQ3dk7ZZ59zwKSHNLBfrKFJyfEiTDi208DvTUBZy0iAtPFfrUa7mnY1NTW1+XZ+RF8g3KZ+YyIPyKHXFiz6snpl9QFfHnAA+CsrK2sEvwdsNKD4Cnqtjb8PGZQfPMOBo9FzqI8Ix6Zn1vXS27N047Kf5ir7tcg6seMX/Iw/FeGdj//F/OATvKxRfoTtq3cIfhv2Z6wkYgW+y/Zu/EUEr1Yyian9qzkEgx//qWX8rIvSfSPeI/SX3bOco2qOvTr46cTSL9Gksb0WFBVFzyya8Nlnc59VdqHWDBnL/pIacWynaSvML86zjOxZ/t6NqNmST1WKiL7hORb+82p78G34FbuqUWJ1tXGgtEfjK+m+mkDQJ46dF5Z3jL9ewBz4Z4n/4+B1tcb8MoGRs1pXtviZx48+Oj7epDfcwNrcJMnIneQ+5NUFKvzMBO25F8dnDRx6Rv9D/Jby8/PPPNNHtE0iVxr1mY/twRf+W6fe6qJPRJifVuBCafF2Qkuf4ignT4Zkh8vUdaA3h17PPsuFEImg71fSsw58TRkjdf24k2P7xkRGxvgHHSnbisGxMbH+/oOPlBZc38O+ZO1Ouz2jfiLj5pGD/d5IjWapWGuHRgzD398XqddDT7jHChpO5Xvq1BW7mnaAjv/BX60eRDafPuqrVxXfY28d7xT6edDbxuf4RVIQe+8jj2fcTVGzF/AEZGwvPjE0xDndivX7ySvR06KY2EMGH3tErCdIaMqbN8g+zQpUxOkmzCDwBh94l+5esWtHc2IzTw8loEQkaW6VDz44dz1RcrZe4As/qQ44/jPQ65nds8/OJQPxHuCv0I5NA3rOqv285/3XsVcMvguk3hiEwRSG0YNtw4Y9//GtGmmYxo896PAFuf5pw45mVF/PVEf0RBbRW35FGzcu3m7izZJnJaka6jrrhR586DE99Dwc4FUsh3r/8v33vcddeB7Jbd2ikBiMT6yEOBUi3Iks6z9/N43Z0LSjeecW0Cci8NFBiPgV8bfl75K2RJIb+MJ/p3Edob9WHYdnA70a/xJrPfTL9z9HcYjEZaNuED51+vNPgY80ToiecvDFoZgUdr7OrDDRyOBvgV6inyw/H7BD5hJBvGfpB9aoY+jjOBqDHnwr5VYujvx8+3UsTo85POTPmz5i2OnTAFb8j61Ip/wYp582//3VpTql6c5XhfsctGULpodeM53Z/F5m6C3Hx/iPWLZ36K3cVe69fP81+BJKv/REz5+FT80/7DsHHwm+PAKCzuL489/frPSsUy18y/ebd5BC864aX/g1zwd6HB/XURn6m7VT65yGxPr7Kf4FEgO67vjDPX8ikS1yWP66TeBbYU7nAcCu1ld6Mf0UB9+G37XrQ9EaTZO3Un3uNCP+g4H0wBt6RDj39l/Aj8L6elx60/Enhof9QcPn5eSsE/z5056/1Ti//FJ6a8Bs8kFv4NGe8GtMiqHgM2TeR7cV17k20PbAQ28k+NzUYeTBe0yg9pgTeARdDo1nZqQ0ticnr9sk5n9KwVVqfQu/iZUc8A69Bb/jw0WLZPNO8ITdu0n0kbC5TX8tg47SS+Kb7ToXWuZX/F9uUnyN8kuk+UQmo99fJ0vYiZLcq/x74kNvbJ9Q/3oH/ZuO5VdAD76I3a9EfjR669DTbRnv6bWKj+8EWJ8bdzi/nuMpv56O9j7hRA3/BZcTT4vggDE7I2WUCH4aAD99F3yLH3pk6F9nvATcoQcedhWeg+/YoStk06vxZa6dJ6cxQg+7wdfd8O2SV/j9/RyjItgR/HIf8PhTiFvpaiZIfNOr8bQhQ84dYUlbQAMMPzFaN72crnNCMRFZe2MHnuXbItiRHfrB8++7Y4Zle4TjL146T+YCg48UXvCvu+17ue34y+03oVtEDj8FLkrizj71uOMOPrjTeVFYasa56HwRv1UjkD6B9uQAfgNf3bzloGWK7zL9Ctj5Vuvb/CZwe8e73y4+UOmROv7NwINvOQ/08IM/7n7Fn33b7UaXizQ1AHySYogycPniycBcyJjhBSNsfG2CaYbF32jxW/QrOJJO+zWLdQH4KujV9LJwVq0B3/DTAhzoBejJsgLdGP+DpffQl/Edl/Ut/N7gf/7Z7N73q24T3YU0L0PxCfJ0LoiWOn7MiBHnCvX+IuCRhQ9/a8Pm76YZ/GtIyaiUdLBKFz/0anqVmH6Rg4/YwL+6XZIeQLeNP8PQA49seOEf9ws5beCXiB5+eL/Jk7UZ5iK4jU8UNCDOk1pVOAp8pPQY3sZ3+Oez8IG+rV4Su9lGVVRWGn6NMovXQ25pBfwCz/Djold8HTQX4zrQI/ht5yG87+CXFUuEpGcxtRtohJFVBWGt3KN7Ze3L7khDbProUaPOPX9/xQdcJPjw04Axwr8a/lufaluWlZZlroVU1Bh+5ipxHJcWyQ8CHxF+WLl9I/Ru499n0SPo98An7Pk54leZtsGuQKHZeFy6W/tOdEA6UmlrwagR6jKKT0cQmRGIcD/JYMmr5z/1VFN9/UCyYcCvIf4CP91X6TfsEq3gGfCDeAJ8ffPhex9+yMUuNi5u4zPsyCyG4N/H6bsGvzcJhXPPGXrE0KEHPmOy4Egl40GIyITUVMiX164NiHFGntlg48Ov+Ocb+w/JzvQy2IZ6yBKYdrePQNahK9MGYv1ZxWUtNWL+1+sTCUXsEO0SfKNv5GuRZG4Y/KOWiMT4jPnLb1bbc4QKPssGPUDF8824v/44Sb1gCup7yBFHP0gmH/v4WbPmGM2USDrFhQO6rufMzfca5zfuAzpSdHci5JkJ7AUHpmkyUk3P4hYx/+v1vjbiQOy2BH+DcR5094oVwCPiPsqvWZII32HGEpnzR7nrReIDx++qccdHhrmSOPseMpQmoFXSBuKIvchlI4JLoMS9HWxrBR908JGy77HrjSAWkUssiyuBjD4tZS0VmH9Lc5vEgdp8Pn7PJ2Au7BtETRts/upqvF+TVNHcxYyauA555qoo+Pe5SfN9VMdHhnRK2PcfcbTmJdLfNOZF+UN37Unk9TWMGjECdCPY91ynek7z1fuUnwYweLasqqg5aktzk6QJtIneb3sffiK3oCNaADxTnLjPyu3bt5PzI2lu+A7w4JsD1JujTLrYTUhaMK7zLouHEOk/Nj4+fsmSRyR3XW5CHR0ZiHZmekEHfrY3yAIvIn91Yr3RsmVZWY9KKnnNr81NGwT/MOJXq2kAkZQN83katKdpAy0QfJLfSJ5n7yJ3jT74QFY7cvh+n8HnCFIy9RBzrC4U4A8Lkj8+eNkNN1AaiiAh/FrJ0O096YU4jw0f7GpIfr/N2D5X4Amhx0suc4/K5h0gEwI6DJkUrDaRT771ASj+lxL1ZONCrvbS9SyUxW24pMDpKfD8xxmaKWm1gAbsE2yX6ImlNLV1l65H3z2ORUcy8ojfpHqC7tfzySuU44tcyfjUgB38vxIhIVXPFn8kc6/Uks9HAxx8do3YmvCmhn0w/s2CPw/2uOVxqKSEFtwvSx0t3xIeBCLCfyR/r0yb/fcwsHd4AfhDMsNDgm0A8nPaG+9t7dcvfUp6Yu5Dhp9c7B3NGv1v6FDrR4527iyVxGGDz6ZdwyX73EzYBHrQ+QafjP8yUU/ShHkGssqhAbcdTwcIErLpr3/z4D0bx6KTQT7IzRUPwbfk9sYUznYbNpdWpftyH8qtx3+wwq87oJfDF5eq0JQqjk314LQ5scmMnRQHgVsGStAR7AKv13Pozzo7WQ2gBeiYE0ODXwU9kitFwZoW7o0MDdLgQcMbgE+RtX9rcr/0dM4p0uvrJ8K/rLkZegt7tHyNLOdL057lhA9+mdMUn3zPGfNwb+FX9JsfmPcAniNL0WecLHNtwH5mjTY5aDoP46gXB/m9ihxU1dCeksLaQdY+jeQ1pJOsmqtb253ESBRfD1DdB5DjRYZ/C/yCL/zaPw0+gp4URBbSkq9tLiioC7FGm6ya8aeTqcLyqlobgbf3Lo2trelV6VN0gwu9je86/ZXfHVkbhp+Bn5HT6p5RjJXqONC/a9ODLw1YJQ9AGoDe5sLE2ScHtXQXjN+acq5I8JW/sbUqF65czomQpmrTKwo7pRCA7+KXZUNS0lfFpgHG+jiO2P5x0YGfthC7gp91gTyAOF1gao02kiD/cK1yT2p2yihr6e9a++f5OdU1g/2mTZuS2+FX/xmD+AcNAN/hZyuTqPhJZgVfAr+a/122AUq/+NtXk+Y+zsrguVXbkrT+FWtk66rNE9ELqFX+BxoQAjzQ0CODr/6f5w31EHKWxLwfRANMA+C2Jfg2/0PKz6KHe7Oz0Mw5b82O0quObCCh10v6785YOndoRMwRj1S0QI9oZ09dHOs9LWqVe7oeZMu2t7+KjwQ/IzWUHnHarcr/heB/QQOEXzNoDD782gDbfcA/NK2lpba2j1QKWFCs/C+8u/1ApScMTRiCOjTcVDf8PKY5SNbGwL8stbJDuwafmaFrfmTTK3+2PseQ0ySxEH3xwxeiHzYlN957bwESfrf/Kz/ev/JQbnvJjV3wi3rBDz5XLoztH7jjnuVLjwvTYsk1yp80Z44cUyv9E++sfXl6UZeqfHgHFbKCU4+HX+n137IjzOdYHwk/Ev6t7QH48uPwq/ezRJWCzbULuCv41szZrB5mrD/wQKUnhZKba6eEmWnpnJZPhV/o68jeeTJ67droF7tWWzR8UNXIMaPUWwRb6PX3kHBr7jOZkV/gPjb/bmN+JPzaCOvmi4z+zYmUrdC6D3VSGvutmSVytgu+0t8hl5S1Pox5AMIPvqS/jOWEl0N21vddMP6whNLW0QUy0ICvreA39F7nqI6Qict50ICcRvBV7g5s+Fk7sL8HXy8qc095wZwoKQwlJRJmPHDHZWScX3FfuFOw+pxPk+BvqeszAfxepAdQGj+2K/i+ts2tkpYHNvAixs8hqfYD9ErQQU7pfkCW+6+zzY9ML7aHzypuTSVmCT93ZaVySB/8YjZ3vYQe47N5ke26cxQXFgs/7tOiuXyINKU+fbuE71vdr7ywQIdKSKAZcf6QzFAnrZPjIYX/7genAVvbMb9pKQ1QfPhN7tVOX8LAgXJXDf+/miQ3RqA5M8CHXvBNjB/vsRTSF/6kpE8rJsE/QegnHNuVkXOYT9LyRhewTigcTT8sH57Nrt1ZjHo5bplGQrNqwIAByj9gd04jkWmV3YfN8FlO7lXCoQMluiUVpCWphSyrpR8s3r5S9mAs/xX/KpbIzjrxHLk+2UJLJ0wC/sZJXem5xE0SwC8nCXI06WDl6YO8oa6ZO+I0znYBl0l369ZNW4Uf/K2GX53NwR9t4ydy2Ut770UGv7bsg+3UheL0Tg9GuSByHcWmXfYnK6SGoQrdiPEjurRQ8ycm+Oi9hSPRlPozvCEBrvUdhtf1wjrRbtpAAwQ/ZQR9xeK3Bp9yJPgJy/a1cxxIUgJ//eKNIvruvJv30TDDuBMDyzvXVEqJFbnodLR4flfN36COOzHrnNiQwHx4sTzsycnt7Y3t7e3JOTm7B1j4Ktv970WtooZSUm1ke2Y679XgVzDlIlL/l8+grI/GGMi6dlmQe/GyrUU9uliYjcanyx208SOn7HtxpT8sMB1b6IGnqxpl5KV6Txugvu/gp6SkNCI5XUrmCW321Zt0Fst7SAkht2LoQUuEH/eJ0lKGAbXBPP3jtcQQl8wOCe3ynYm8XDR+PDW3A//nEC/wQi/wmu+cGR7GIkiND74Rp3o5aF3Out3rduNlq1fXy/5Y+RW/5XBZmfiHLiH3f+mMeVHKH1iZLXywKRHz6CGeP/K2svT6KeDfwKX9QNcxjmNGeZZwWpHAA35Oewd9zu7du7fyhdbtVn5fLvzgA0S1ktr9TjRTbP+DHj8Q/v0IkdyCjgm4RMxLr66MPzYW83VdHn96Ls7TP7xTJv9hxvYKzxIu0vxNOu6Ab9ZJOVvpzfwIPcKB+pXW2/hYv67kphNCjJWEf/1yriLfpkdAJ3gC6nP0P+QP14cJlfQef0SnCU3pcww9SzircV6r45qNWUr+1gEIfhmbctgOtFZ9tHPQkbb3z+o9bhzVph3+xWVxWh8WfCl06dKfeklgiI72bsWcKZ6Dm5j1RIYpN4DrQ4/vgC74meE2vw5Pja3lBE7ywmMTssg0q2yJi5JAZrjj4kseX7zU8Et5ZzrFX6Xw/NWG3izihtjzgUfwG8FHurKLMPzgy/gkK89B4dQyrN7IODkv6ibwOwbJmKFLKOUZV0JJV/ClouhfJA/0OANmVvoRzhIuZtMmNb5RNn4RmQO/4ss2npoJ8pwO55RIhpnbpYytq5DxQQ/O/awYfq2u/Rju/5codNjmzYbeKNMBGCbdweCzQ0sVd8qEH3zWobpzzwszZUJWMkv1xvwc8/cNcWYo6vASIHlY+J3i/t1P349BpF0wlT7b43ySj5WF/nyR2dPENOZI1002+BhfK3l++eq3OkpKJz01vGOGWqJl4R+muDz8D/8V/B4/99cNveJnuEqoW8aHHWXqJO3JaMwx84NsHTM8ViHVH5PopbwTAfxi5kOnfiVFtLU6u7yboNvLeOs0JvBKD77pto7v8FAs43O26jUekS3ZELh+oYR+8B2rju2rScsZZGSMKRnYsZaKOdp6qcAb8FOQ4ZSI7mRnesxraCBSqzZWfmzc0aN5KBY+5wPWnjVTsiEYd8aIMg1o2FlfqvknyxgzuWWw6/UBuM8ci59Xu7yGZ3WfCDNTeIJgp3qIji4u83iTk1nog6/HG5CqUgsIabVifFSQanXS/AMoZKhvIrr8tsfKboh1VS02L9Tg7RtkXnA55dTIkG6j9w6vqpJQZoFNr5t2p+NCL/jmUJVPHPxCgldu/JC8xB9/TOop5sfHZ8f7XRPi0Y+sgv/p13g3x2NvU6m5216iR9xkyvjyctnJ2vi4jsv4jS7jD7Efi5cNi4PvtfETwJcxZrLUJ62JDXhlT0UL79YzL9eTyNRJ3eX/sblSMIk1jvFvjfc48uQ1QO/gZzs1+SG/F/wCcaIYizEztxp8fYcb+SFJkYHv7Gnh1SLSgHfgf2J6UWw34Q8rTa8CH0IR+OrfFlFqQyvPxcFPdUZT2BvpusKfEukk3iT+mDSrTLoo2S1nB2RZ+8X822bO5KWk77zx9ivTnzy4m/BjfL70kYUFSsg3xncfAHBP04XvdT7IuJetlsHPCHfSnuq/Spo1S7ro29TbOTyw+POjLXJ7iFuC8kbhFzl57hYxq/tKy8F3JqYQ165sysjCMaMsfE4mIx2ajHWrNyU3BOKH+dNXJvH2HDyctxS+E2h+3ovZIhWvuPpBvhfH/t03ZxF2cPCHxLhcp2q8hW/46blOAsT8+av7gQ9/RoTTXH/ipy2rhJ8xZu1rJ4cEVCerbGkhgqW1vicwJ3ebwvOqRuI8xsLZoa4xKT29dbQb3+Pgn353EHxeH/JpTa28uJYGPP1KQOcN7Y/5KwjroElHeLp3zTB89CjwZVWQ6hqTCIY2lEsw2sKnaZ3x4QffUeyhFfLuq+iZUs77iYBjh9gbHuUUQErW9mB73q0KTc0YQ5hWCD3uYCjRLJkRoA+OXy7Trhs/Jq1S7nLJe4+pr9zLHUOKGHwxJcfQsRSW62aFhHPgwthCnDnghnWVmdBs/LCO6ggbDH5hIH7kOdyH0vcyLly79gne2ug2/5GymY/vz4Kh+xUWmUmWbarbMDEN6eZMEft3xn9/QxP48vGYjHD3rexKyWnhZtZbvLVxepF7gAnjyH+wFM79a0RyVqfsW/9w5uNC+MV9AsbUSPJ52uT6NfhMW448Z9Q8qvhFjPAcXgVmdUWEk2j99yk0c7jeCtfBB/yObu218Mv5sCDG1eIzKIspp3RoAgL/HxRdWvHHgB8wJcSSzKO3x+XDVHdt4UNX1sgxnaX+uMo/J7q0f9BwvEcjJ6muLVh1E5cASz8aP5IPM93dPS0tyyoQzT+O/cdfdy9XLbIzMjLcBzDsTPQKY8JHVSOxPj3aUd+sLOvqpJac3Rterh4S5gkn4zMgaTKxmtpbCa9PEfwMj3t0NxldXGGl0PLeQB90lZoIfdqvil+QEuP+aPBE4adKtHevfVV/bELioWlp4FObl4BoamBSYx73afvuLe+0D1q7XPBragZa+PTdQGcL2Uu9xp6cEhLTSHXOWobzsKbI3mvdJJhYGiSSKE/i7r4Gn1XPv0mKn8U984mM+8wIDD3/JoUdIqnOpIw+hPHBz9x7u2lw8/sHS7YkHZfV3JDMf5fv6PDo9XM4JpNxauS/zPYd9T1DQ8P26iHyf/2H9RuD2dOujfs1HgAAAABJRU5ErkJggg==")) attr_dev(img, "src", img_src_value);
    			add_location(img, file$l, 0, 0, 0);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, img, anchor);
    		},
    		p: noop,
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(img);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$m.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$m($$self, $$props) {
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<Logo> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("Logo", $$slots, []);
    	return [];
    }

    class Logo extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$m, create_fragment$m, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Logo",
    			options,
    			id: create_fragment$m.name
    		});
    	}
    }

    /* src/assets/Headphones.svelte generated by Svelte v3.20.1 */

    const file$m = "src/assets/Headphones.svelte";

    function create_fragment$n(ctx) {
    	let svg;
    	let path0;
    	let path1;
    	let path2;

    	const block = {
    		c: function create() {
    			svg = svg_element("svg");
    			path0 = svg_element("path");
    			path1 = svg_element("path");
    			path2 = svg_element("path");
    			attr_dev(path0, "d", "M256,31C115.39,31,0,145.39,0,286v120c0,24.814,20.186,45,45,45h15V301H45c-5.284,0-10.285,1.082-15,2.763V286\n    C30,161.928,131.928,61,256,61s226,100.928,226,225v17.763c-4.715-1.681-9.716-2.763-15-2.763h-15v150h15\n    c24.814,0,45-20.186,45-45V286C512,145.39,396.61,31,256,31z");
    			attr_dev(path0, "fill", "#ef3340");
    			add_location(path0, file$m, 1, 2, 53);
    			attr_dev(path1, "d", "M135,271h-15c-16.569,0-30,13.431-30,30v150c0,16.569,13.431,30,30,30h15c8.284,0,15-6.716,15-15V286\n    C150,277.716,143.284,271,135,271z");
    			attr_dev(path1, "fill", "#ef3340");
    			add_location(path1, file$m, 7, 2, 368);
    			attr_dev(path2, "d", "M392,271h-15c-8.284,0-15,6.716-15,15v180c0,8.284,6.716,15,15,15h15c16.569,0,30-13.431,30-30V301\n    C422,284.431,408.569,271,392,271z");
    			attr_dev(path2, "fill", "#ef3340");
    			add_location(path2, file$m, 12, 2, 543);
    			attr_dev(svg, "viewBox", "0 0 512 512");
    			attr_dev(svg, "width", "32");
    			attr_dev(svg, "height", "32");
    			add_location(svg, file$m, 0, 0, 0);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, svg, anchor);
    			append_dev(svg, path0);
    			append_dev(svg, path1);
    			append_dev(svg, path2);
    		},
    		p: noop,
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(svg);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$n.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$n($$self, $$props) {
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<Headphones> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("Headphones", $$slots, []);
    	return [];
    }

    class Headphones extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$n, create_fragment$n, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Headphones",
    			options,
    			id: create_fragment$n.name
    		});
    	}
    }

    /* src/views/Widget.svelte generated by Svelte v3.20.1 */
    const file$n = "src/views/Widget.svelte";

    // (38:6) {#if $preloadIndicator === 1}
    function create_if_block_1$1(ctx) {
    	let div;
    	let button0;
    	let button0_transition;
    	let t1;
    	let button1;
    	let current;
    	let dispose;

    	const block = {
    		c: function create() {
    			div = element("div");
    			button0 = element("button");
    			button0.textContent = `${text$1("START")}`;
    			t1 = space();
    			button1 = element("button");
    			button1.textContent = `${lang === "ru" ? "EN" : "RU"}`;
    			attr_dev(button0, "class", "button startButton svelte-1oi5zz7");
    			add_location(button0, file$n, 39, 10, 1240);
    			attr_dev(button1, "class", "lang svelte-1oi5zz7");
    			add_location(button1, file$n, 45, 10, 1415);
    			attr_dev(div, "class", "startButtonCOntainer svelte-1oi5zz7");
    			add_location(div, file$n, 38, 8, 1195);
    		},
    		m: function mount(target, anchor, remount) {
    			insert_dev(target, div, anchor);
    			append_dev(div, button0);
    			append_dev(div, t1);
    			append_dev(div, button1);
    			current = true;
    			if (remount) run_all(dispose);

    			dispose = [
    				listen_dev(button0, "click", startWidget, false, false, false),
    				listen_dev(button1, "click", /*changeLang*/ ctx[3], false, false, false)
    			];
    		},
    		p: noop,
    		i: function intro(local) {
    			if (current) return;

    			if (local) {
    				add_render_callback(() => {
    					if (!button0_transition) button0_transition = create_bidirectional_transition(button0, fade, {}, true);
    					button0_transition.run(1);
    				});
    			}

    			current = true;
    		},
    		o: function outro(local) {
    			if (local) {
    				if (!button0_transition) button0_transition = create_bidirectional_transition(button0, fade, {}, false);
    				button0_transition.run(0);
    			}

    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			if (detaching && button0_transition) button0_transition.end();
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_1$1.name,
    		type: "if",
    		source: "(38:6) {#if $preloadIndicator === 1}",
    		ctx
    	});

    	return block;
    }

    // (59:2) {#if showPlayWithSound && $preloadIndicator !== 1}
    function create_if_block$8(ctx) {
    	let div1;
    	let t0;
    	let div0;
    	let div1_transition;
    	let current;
    	const headphones = new Headphones({ $$inline: true });

    	const block = {
    		c: function create() {
    			div1 = element("div");
    			create_component(headphones.$$.fragment);
    			t0 = space();
    			div0 = element("div");
    			div0.textContent = `${text$1("PLAY_WITH_SOUND")}`;
    			attr_dev(div0, "class", "playWithSoundText svelte-1oi5zz7");
    			add_location(div0, file$n, 61, 6, 1969);
    			attr_dev(div1, "class", "playWithSound svelte-1oi5zz7");
    			add_location(div1, file$n, 59, 4, 1892);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div1, anchor);
    			mount_component(headphones, div1, null);
    			append_dev(div1, t0);
    			append_dev(div1, div0);
    			current = true;
    		},
    		p: noop,
    		i: function intro(local) {
    			if (current) return;
    			transition_in(headphones.$$.fragment, local);

    			if (local) {
    				add_render_callback(() => {
    					if (!div1_transition) div1_transition = create_bidirectional_transition(div1, fade, {}, true);
    					div1_transition.run(1);
    				});
    			}

    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(headphones.$$.fragment, local);

    			if (local) {
    				if (!div1_transition) div1_transition = create_bidirectional_transition(div1, fade, {}, false);
    				div1_transition.run(0);
    			}

    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div1);
    			destroy_component(headphones);
    			if (detaching && div1_transition) div1_transition.end();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$8.name,
    		type: "if",
    		source: "(59:2) {#if showPlayWithSound && $preloadIndicator !== 1}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$o(ctx) {
    	let div10;
    	let div9;
    	let div2;
    	let div0;
    	let t1;
    	let div1;
    	let t2_value = text$1("CATCH_VIRUSES") + "";
    	let t2;
    	let t3;
    	let br;
    	let t4;
    	let t5_value = text$1("ANSWER_QUESTIONS_WIDGET") + "";
    	let t5;
    	let t6;
    	let div3;
    	let t7;
    	let div8;
    	let t8;
    	let div7;
    	let div5;
    	let div4;
    	let div4_style_value;
    	let t9;
    	let div6;
    	let t10_value = (/*introText*/ ctx[1] || "") + "";
    	let t10;
    	let t11;
    	let current;
    	const logo = new Logo({ $$inline: true });
    	let if_block0 = /*$preloadIndicator*/ ctx[2] === 1 && create_if_block_1$1(ctx);
    	let if_block1 = /*showPlayWithSound*/ ctx[0] && /*$preloadIndicator*/ ctx[2] !== 1 && create_if_block$8(ctx);

    	const block = {
    		c: function create() {
    			div10 = element("div");
    			div9 = element("div");
    			div2 = element("div");
    			div0 = element("div");
    			div0.textContent = `${text$1("IMMUNITY")}`;
    			t1 = space();
    			div1 = element("div");
    			t2 = text(t2_value);
    			t3 = space();
    			br = element("br");
    			t4 = space();
    			t5 = text(t5_value);
    			t6 = space();
    			div3 = element("div");
    			create_component(logo.$$.fragment);
    			t7 = space();
    			div8 = element("div");
    			if (if_block0) if_block0.c();
    			t8 = space();
    			div7 = element("div");
    			div5 = element("div");
    			div4 = element("div");
    			t9 = space();
    			div6 = element("div");
    			t10 = text(t10_value);
    			t11 = space();
    			if (if_block1) if_block1.c();
    			attr_dev(div0, "class", "text primary svelte-1oi5zz7");
    			add_location(div0, file$n, 26, 6, 882);
    			add_location(br, file$n, 29, 8, 1008);
    			attr_dev(div1, "class", "text secondary svelte-1oi5zz7");
    			add_location(div1, file$n, 27, 6, 939);
    			add_location(div2, file$n, 25, 4, 870);
    			add_location(div3, file$n, 33, 4, 1085);
    			attr_dev(div4, "class", "indicator svelte-1oi5zz7");
    			attr_dev(div4, "style", div4_style_value = `width:${/*$preloadIndicator*/ ctx[2] * 100}%`);
    			add_location(div4, file$n, 52, 10, 1662);
    			attr_dev(div5, "class", "loader svelte-1oi5zz7");
    			add_location(div5, file$n, 51, 8, 1631);
    			attr_dev(div6, "class", "introText svelte-1oi5zz7");
    			add_location(div6, file$n, 54, 8, 1755);
    			attr_dev(div7, "class", "loaderContainer svelte-1oi5zz7");
    			toggle_class(div7, "hidden", /*$preloadIndicator*/ ctx[2] === 1);
    			add_location(div7, file$n, 50, 6, 1554);
    			attr_dev(div8, "class", "bottomContainer svelte-1oi5zz7");
    			add_location(div8, file$n, 36, 4, 1121);
    			attr_dev(div9, "class", "container svelte-1oi5zz7");
    			add_location(div9, file$n, 24, 2, 842);
    			attr_dev(div10, "class", "widget svelte-1oi5zz7");
    			attr_dev(div10, "id", "covidWidget");
    			toggle_class(div10, "isVk", IS_VK);
    			add_location(div10, file$n, 23, 0, 783);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div10, anchor);
    			append_dev(div10, div9);
    			append_dev(div9, div2);
    			append_dev(div2, div0);
    			append_dev(div2, t1);
    			append_dev(div2, div1);
    			append_dev(div1, t2);
    			append_dev(div1, t3);
    			append_dev(div1, br);
    			append_dev(div1, t4);
    			append_dev(div1, t5);
    			append_dev(div9, t6);
    			append_dev(div9, div3);
    			mount_component(logo, div3, null);
    			append_dev(div9, t7);
    			append_dev(div9, div8);
    			if (if_block0) if_block0.m(div8, null);
    			append_dev(div8, t8);
    			append_dev(div8, div7);
    			append_dev(div7, div5);
    			append_dev(div5, div4);
    			append_dev(div7, t9);
    			append_dev(div7, div6);
    			append_dev(div6, t10);
    			append_dev(div10, t11);
    			if (if_block1) if_block1.m(div10, null);
    			current = true;
    		},
    		p: function update(ctx, [dirty]) {
    			if (/*$preloadIndicator*/ ctx[2] === 1) {
    				if (if_block0) {
    					if_block0.p(ctx, dirty);
    					transition_in(if_block0, 1);
    				} else {
    					if_block0 = create_if_block_1$1(ctx);
    					if_block0.c();
    					transition_in(if_block0, 1);
    					if_block0.m(div8, t8);
    				}
    			} else if (if_block0) {
    				group_outros();

    				transition_out(if_block0, 1, 1, () => {
    					if_block0 = null;
    				});

    				check_outros();
    			}

    			if (!current || dirty & /*$preloadIndicator*/ 4 && div4_style_value !== (div4_style_value = `width:${/*$preloadIndicator*/ ctx[2] * 100}%`)) {
    				attr_dev(div4, "style", div4_style_value);
    			}

    			if ((!current || dirty & /*introText*/ 2) && t10_value !== (t10_value = (/*introText*/ ctx[1] || "") + "")) set_data_dev(t10, t10_value);

    			if (dirty & /*$preloadIndicator*/ 4) {
    				toggle_class(div7, "hidden", /*$preloadIndicator*/ ctx[2] === 1);
    			}

    			if (/*showPlayWithSound*/ ctx[0] && /*$preloadIndicator*/ ctx[2] !== 1) {
    				if (if_block1) {
    					if_block1.p(ctx, dirty);
    					transition_in(if_block1, 1);
    				} else {
    					if_block1 = create_if_block$8(ctx);
    					if_block1.c();
    					transition_in(if_block1, 1);
    					if_block1.m(div10, null);
    				}
    			} else if (if_block1) {
    				group_outros();

    				transition_out(if_block1, 1, 1, () => {
    					if_block1 = null;
    				});

    				check_outros();
    			}

    			if (dirty & /*IS_VK*/ 0) {
    				toggle_class(div10, "isVk", IS_VK);
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(logo.$$.fragment, local);
    			transition_in(if_block0);
    			transition_in(if_block1);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(logo.$$.fragment, local);
    			transition_out(if_block0);
    			transition_out(if_block1);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div10);
    			destroy_component(logo);
    			if (if_block0) if_block0.d();
    			if (if_block1) if_block1.d();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$o.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$o($$self, $$props, $$invalidate) {
    	let $preloadIndicator;
    	validate_store(preloadIndicator, "preloadIndicator");
    	component_subscribe($$self, preloadIndicator, $$value => $$invalidate(2, $preloadIndicator = $$value));
    	let showPlayWithSound = true;

    	onMount(() => {
    		sendMetrik("Visible", { lang });
    		setTimeout(() => $$invalidate(0, showPlayWithSound = false), 1000);
    	});

    	const changeLang = () => {
    		localStorage.setItem("covidlang", lang === "ru" ? "en" : "ru");
    		location.reload();
    	};

    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<Widget> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("Widget", $$slots, []);

    	$$self.$capture_state = () => ({
    		onMount,
    		sendMetrik,
    		fade,
    		IS_VK,
    		Logo,
    		Headphones,
    		startWidget,
    		preloadIndicator,
    		gameState,
    		text: text$1,
    		lang,
    		showPlayWithSound,
    		changeLang,
    		introText,
    		$preloadIndicator
    	});

    	$$self.$inject_state = $$props => {
    		if ("showPlayWithSound" in $$props) $$invalidate(0, showPlayWithSound = $$props.showPlayWithSound);
    		if ("introText" in $$props) $$invalidate(1, introText = $$props.introText);
    	};

    	let introText;

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	 $$invalidate(1, introText = text$1("INTRO_PHRASES")[Math.floor(Math.random() * text$1("INTRO_PHRASES").length)]);
    	return [showPlayWithSound, introText, $preloadIndicator, changeLang];
    }

    class Widget extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$o, create_fragment$o, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Widget",
    			options,
    			id: create_fragment$o.name
    		});
    	}
    }

    var screenliferLogo = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAHbSURBVHgBvZS9TxRBGMafmd2TUwtdrE0kVCYWFCqXEHEgd5qrbSwgHCb8ARhRg1/4Fa3U3ihaGC0sCeHrLhvCd0EoqAgJRyCBAOGOCrIs8/LuBcgRFu5YCE8zO7OT38zzvG8GOGWJ/MlKRDUYEHUANVkjdhoBJPfRNWpAFAVhJhOpfb9YX38Rx5T0W9SE3yD9smRqbnLllkrgpMArY3aCTKPO+zYk2rNV0f+ZiLqGoEBPpYPJP9aoXcYpvyXXfZCL4bZ6g6DAXXFx2hhaRtLo4rEtU6lm1u7cexgYuANNlw4n4wxs9Obacf4yuH3BJ4aigHngXxyqwjn5laeJMIn+zN1oY2BgDjqUmtWbNM+ZZrlfr245dCH/v4ljaPmmUqaJn9yrXrFseb6kybK7pwMBlyqrH/HmH2xqlX01W8Opb377ClpWLT0V3hjSctxrIYR1+WGwI28Yf5Wqclx84JzU/Rf2O+tjrgcnUEC+N6xtTX1xXBogUAUBaVdvva5+2nEdRcgXKIiapaB/6xub5XCgGJoNyXBH9FnvJRTQPsvS8CyJG4L0k95PMXtneTX2vKdFC+M7pPRsPz4KKFCkYq3JTn6F4nxYTd/nvcMOqOjGNinUwAWaICEu4yy1DXcOpjUEL7M9AAAAAElFTkSuQmCC';

    /* src/components/NotificationIos.svelte generated by Svelte v3.20.1 */
    const file$o = "src/components/NotificationIos.svelte";

    function create_fragment$p(ctx) {
    	let a;
    	let div4;
    	let div2;
    	let div0;
    	let img;
    	let img_src_value;
    	let t0;
    	let div1;
    	let t2;
    	let div3;
    	let t4;
    	let div5;
    	let t5_value = /*notification*/ ctx[0].title + "";
    	let t5;
    	let t6;
    	let div6;
    	let raw_value = /*notification*/ ctx[0].content + "";
    	let a_href_value;
    	let dispose;

    	const block = {
    		c: function create() {
    			a = element("a");
    			div4 = element("div");
    			div2 = element("div");
    			div0 = element("div");
    			img = element("img");
    			t0 = space();
    			div1 = element("div");
    			div1.textContent = "Screenlifer";
    			t2 = space();
    			div3 = element("div");
    			div3.textContent = "сейчас";
    			t4 = space();
    			div5 = element("div");
    			t5 = text(t5_value);
    			t6 = space();
    			div6 = element("div");
    			attr_dev(img, "class", "screenliferLogo");
    			if (img.src !== (img_src_value = screenliferLogo)) attr_dev(img, "src", img_src_value);
    			attr_dev(img, "alt", "screenlife logo");
    			add_location(img, file$o, 13, 8, 321);
    			attr_dev(div0, "class", "imageContainer svelte-1y894yq");
    			add_location(div0, file$o, 12, 6, 284);
    			attr_dev(div1, "class", "notificationLogoTitile svelte-1y894yq");
    			add_location(div1, file$o, 18, 6, 446);
    			attr_dev(div2, "class", "logoContainer svelte-1y894yq");
    			add_location(div2, file$o, 11, 4, 250);
    			attr_dev(div3, "class", "notificationNow svelte-1y894yq");
    			add_location(div3, file$o, 20, 4, 515);
    			attr_dev(div4, "class", "notificationTitleContainer svelte-1y894yq");
    			add_location(div4, file$o, 10, 2, 205);
    			attr_dev(div5, "class", "notificationTitle svelte-1y894yq");
    			add_location(div5, file$o, 22, 2, 568);
    			attr_dev(div6, "class", "notificationContent svelte-1y894yq");
    			add_location(div6, file$o, 23, 2, 628);
    			attr_dev(a, "class", "notificationContainer svelte-1y894yq");
    			attr_dev(a, "href", a_href_value = /*notification*/ ctx[0].link);
    			attr_dev(a, "target", "_blank");
    			add_location(a, file$o, 5, 0, 111);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor, remount) {
    			insert_dev(target, a, anchor);
    			append_dev(a, div4);
    			append_dev(div4, div2);
    			append_dev(div2, div0);
    			append_dev(div0, img);
    			append_dev(div2, t0);
    			append_dev(div2, div1);
    			append_dev(div4, t2);
    			append_dev(div4, div3);
    			append_dev(a, t4);
    			append_dev(a, div5);
    			append_dev(div5, t5);
    			append_dev(a, t6);
    			append_dev(a, div6);
    			div6.innerHTML = raw_value;
    			if (remount) dispose();
    			dispose = listen_dev(a, "click", /*click_handler*/ ctx[1], false, false, false);
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*notification*/ 1 && t5_value !== (t5_value = /*notification*/ ctx[0].title + "")) set_data_dev(t5, t5_value);
    			if (dirty & /*notification*/ 1 && raw_value !== (raw_value = /*notification*/ ctx[0].content + "")) div6.innerHTML = raw_value;
    			if (dirty & /*notification*/ 1 && a_href_value !== (a_href_value = /*notification*/ ctx[0].link)) {
    				attr_dev(a, "href", a_href_value);
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(a);
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$p.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$p($$self, $$props, $$invalidate) {
    	let { notification = {} } = $$props;
    	const writable_props = ["notification"];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<NotificationIos> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("NotificationIos", $$slots, []);

    	function click_handler(event) {
    		bubble($$self, event);
    	}

    	$$self.$set = $$props => {
    		if ("notification" in $$props) $$invalidate(0, notification = $$props.notification);
    	};

    	$$self.$capture_state = () => ({ screenliferLogo, notification });

    	$$self.$inject_state = $$props => {
    		if ("notification" in $$props) $$invalidate(0, notification = $$props.notification);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [notification, click_handler];
    }

    class NotificationIos extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$p, create_fragment$p, safe_not_equal, { notification: 0 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "NotificationIos",
    			options,
    			id: create_fragment$p.name
    		});
    	}

    	get notification() {
    		throw new Error("<NotificationIos>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set notification(value) {
    		throw new Error("<NotificationIos>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/components/NotificationAndroid.svelte generated by Svelte v3.20.1 */
    const file$p = "src/components/NotificationAndroid.svelte";

    function create_fragment$q(ctx) {
    	let a;
    	let div0;
    	let img;
    	let img_src_value;
    	let t0;
    	let div5;
    	let div3;
    	let div1;
    	let t1_value = /*notification*/ ctx[0].title + "";
    	let t1;
    	let t2;
    	let div2;
    	let t4;
    	let div4;
    	let raw_value = /*notification*/ ctx[0].content + "";
    	let a_href_value;
    	let dispose;

    	const block = {
    		c: function create() {
    			a = element("a");
    			div0 = element("div");
    			img = element("img");
    			t0 = space();
    			div5 = element("div");
    			div3 = element("div");
    			div1 = element("div");
    			t1 = text(t1_value);
    			t2 = space();
    			div2 = element("div");
    			div2.textContent = "сейчас";
    			t4 = space();
    			div4 = element("div");
    			attr_dev(img, "class", "screenliferLogo svelte-b3q4ro");
    			if (img.src !== (img_src_value = screenliferLogo)) attr_dev(img, "src", img_src_value);
    			attr_dev(img, "alt", "screenlife logo");
    			add_location(img, file$p, 12, 6, 222);
    			add_location(div0, file$p, 11, 4, 210);
    			attr_dev(div1, "class", "notificationTitle svelte-b3q4ro");
    			add_location(div1, file$p, 20, 6, 408);
    			add_location(div2, file$p, 21, 6, 472);
    			attr_dev(div3, "class", "titleContainer svelte-b3q4ro");
    			add_location(div3, file$p, 19, 4, 373);
    			attr_dev(div4, "class", "notificationContent svelte-b3q4ro");
    			add_location(div4, file$p, 23, 4, 505);
    			attr_dev(div5, "class", "contentContainer svelte-b3q4ro");
    			add_location(div5, file$p, 18, 2, 338);
    			attr_dev(a, "class", "notificationContainer svelte-b3q4ro");
    			attr_dev(a, "href", a_href_value = /*notification*/ ctx[0].link);
    			attr_dev(a, "target", "_blank");
    			add_location(a, file$p, 5, 0, 111);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor, remount) {
    			insert_dev(target, a, anchor);
    			append_dev(a, div0);
    			append_dev(div0, img);
    			append_dev(a, t0);
    			append_dev(a, div5);
    			append_dev(div5, div3);
    			append_dev(div3, div1);
    			append_dev(div1, t1);
    			append_dev(div3, t2);
    			append_dev(div3, div2);
    			append_dev(div5, t4);
    			append_dev(div5, div4);
    			div4.innerHTML = raw_value;
    			if (remount) dispose();
    			dispose = listen_dev(a, "click", /*click_handler*/ ctx[1], false, false, false);
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*notification*/ 1 && t1_value !== (t1_value = /*notification*/ ctx[0].title + "")) set_data_dev(t1, t1_value);
    			if (dirty & /*notification*/ 1 && raw_value !== (raw_value = /*notification*/ ctx[0].content + "")) div4.innerHTML = raw_value;
    			if (dirty & /*notification*/ 1 && a_href_value !== (a_href_value = /*notification*/ ctx[0].link)) {
    				attr_dev(a, "href", a_href_value);
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(a);
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$q.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$q($$self, $$props, $$invalidate) {
    	let { notification = {} } = $$props;
    	const writable_props = ["notification"];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<NotificationAndroid> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("NotificationAndroid", $$slots, []);

    	function click_handler(event) {
    		bubble($$self, event);
    	}

    	$$self.$set = $$props => {
    		if ("notification" in $$props) $$invalidate(0, notification = $$props.notification);
    	};

    	$$self.$capture_state = () => ({ screenliferLogo, notification });

    	$$self.$inject_state = $$props => {
    		if ("notification" in $$props) $$invalidate(0, notification = $$props.notification);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [notification, click_handler];
    }

    class NotificationAndroid extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$q, create_fragment$q, safe_not_equal, { notification: 0 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "NotificationAndroid",
    			options,
    			id: create_fragment$q.name
    		});
    	}

    	get notification() {
    		throw new Error("<NotificationAndroid>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set notification(value) {
    		throw new Error("<NotificationAndroid>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    var notifications = {
      vk: [
        {
          title: "Узнай больше о вирусе",
          content:
            'Читай тут <a href="https://vk.com/stopcoronavirusrf" target="_blank">стопкоронавирус</a>',
          link: "https://vk.com/stopcoronavirusrf",
          id: "vk стопкоронавирус.рф",
        },
        {
          title: "Что делать на карантине?",
          content:
            'Развлекись на <a href="https://vk.com/screenlifer" target="_blank">screenlifer</a>',
          link: "https://vk.com/screenlifer",
          id: "vk screenlifer",
        },
        {
          title: "Защити себя",
          content:
            'Закажи наобходимое на <a href="https://vk.com/ae_app#/" target="_blank">aliexpress</a>',
          link: "https://vk.com/ae_app#/",
          id: "vk aliexpress",
        },
      ].sort(() => 0.5 - Math.random()),
      widget: [
        {
          title: "Узнай больше о вирусе",
          content:
            'Читай тут <a href="https://стопкоронавирус.рф" target="_blank">стопкоронавирус.рф</a>',
          link: "https://стопкоронавирус.рф",
          id: "стопкоронавирус.рф",
        },
        {
          title: "Что делать на карантине?",
          content:
            'Развлекись на <a href="https://screenlifer.com/quarantine" target="_blank">screenlifer.com</a>',
          link: "https://screenlifer.com/quarantine",
          id: "screenlifer",
        },
        {
          title: "Защити себя",
          content:
            'Закажи наобходимое на <a href="https://aliexpress.ru" target="_blank">aliexpress.ru</a>',
          link: "https://aliexpress.ru",
          id: "aliexpress",
        },
      ].sort(() => 0.5 - Math.random()),
    };

    /* src/components/NotificationController.svelte generated by Svelte v3.20.1 */
    const file$q = "src/components/NotificationController.svelte";

    // (68:0) {#if isOpen}
    function create_if_block$9(ctx) {
    	let div;
    	let current_block_type_index;
    	let if_block;
    	let div_transition;
    	let current;
    	const if_block_creators = [create_if_block_1$2, create_else_block$3];
    	const if_blocks = [];

    	function select_block_type(ctx, dirty) {
    		if (/*isAndroid*/ ctx[3]) return 0;
    		return 1;
    	}

    	current_block_type_index = select_block_type(ctx);
    	if_block = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);

    	const block = {
    		c: function create() {
    			div = element("div");
    			if_block.c();
    			attr_dev(div, "class", "notificaiton svelte-d4qew4");
    			toggle_class(div, "isVkIos", /*isIos*/ ctx[4] && IS_MOBILE_VK);
    			add_location(div, file$q, 68, 2, 1965);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			if_blocks[current_block_type_index].m(div, null);
    			current = true;
    		},
    		p: function update(ctx, dirty) {
    			if_block.p(ctx, dirty);

    			if (dirty & /*isIos, IS_MOBILE_VK*/ 16) {
    				toggle_class(div, "isVkIos", /*isIos*/ ctx[4] && IS_MOBILE_VK);
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(if_block);

    			if (local) {
    				add_render_callback(() => {
    					if (!div_transition) div_transition = create_bidirectional_transition(div, slide, { duration: 200 }, true);
    					div_transition.run(1);
    				});
    			}

    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(if_block);

    			if (local) {
    				if (!div_transition) div_transition = create_bidirectional_transition(div, slide, { duration: 200 }, false);
    				div_transition.run(0);
    			}

    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			if_blocks[current_block_type_index].d();
    			if (detaching && div_transition) div_transition.end();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$9.name,
    		type: "if",
    		source: "(68:0) {#if isOpen}",
    		ctx
    	});

    	return block;
    }

    // (75:4) {:else}
    function create_else_block$3(ctx) {
    	let current;

    	const notificationios = new NotificationIos({
    			props: { notification: /*notification*/ ctx[1] },
    			$$inline: true
    		});

    	notificationios.$on("click", /*notificationClick*/ ctx[2]);

    	const block = {
    		c: function create() {
    			create_component(notificationios.$$.fragment);
    		},
    		m: function mount(target, anchor) {
    			mount_component(notificationios, target, anchor);
    			current = true;
    		},
    		p: function update(ctx, dirty) {
    			const notificationios_changes = {};
    			if (dirty & /*notification*/ 2) notificationios_changes.notification = /*notification*/ ctx[1];
    			notificationios.$set(notificationios_changes);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(notificationios.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(notificationios.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(notificationios, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_else_block$3.name,
    		type: "else",
    		source: "(75:4) {:else}",
    		ctx
    	});

    	return block;
    }

    // (73:4) {#if isAndroid}
    function create_if_block_1$2(ctx) {
    	let current;

    	const notificationandroid = new NotificationAndroid({
    			props: { notification: /*notification*/ ctx[1] },
    			$$inline: true
    		});

    	notificationandroid.$on("click", /*notificationClick*/ ctx[2]);

    	const block = {
    		c: function create() {
    			create_component(notificationandroid.$$.fragment);
    		},
    		m: function mount(target, anchor) {
    			mount_component(notificationandroid, target, anchor);
    			current = true;
    		},
    		p: function update(ctx, dirty) {
    			const notificationandroid_changes = {};
    			if (dirty & /*notification*/ 2) notificationandroid_changes.notification = /*notification*/ ctx[1];
    			notificationandroid.$set(notificationandroid_changes);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(notificationandroid.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(notificationandroid.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(notificationandroid, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_1$2.name,
    		type: "if",
    		source: "(73:4) {#if isAndroid}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$r(ctx) {
    	let if_block_anchor;
    	let current;
    	let if_block = /*isOpen*/ ctx[0] && create_if_block$9(ctx);

    	const block = {
    		c: function create() {
    			if (if_block) if_block.c();
    			if_block_anchor = empty();
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			if (if_block) if_block.m(target, anchor);
    			insert_dev(target, if_block_anchor, anchor);
    			current = true;
    		},
    		p: function update(ctx, [dirty]) {
    			if (/*isOpen*/ ctx[0]) {
    				if (if_block) {
    					if_block.p(ctx, dirty);
    					transition_in(if_block, 1);
    				} else {
    					if_block = create_if_block$9(ctx);
    					if_block.c();
    					transition_in(if_block, 1);
    					if_block.m(if_block_anchor.parentNode, if_block_anchor);
    				}
    			} else if (if_block) {
    				group_outros();

    				transition_out(if_block, 1, 1, () => {
    					if_block = null;
    				});

    				check_outros();
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(if_block);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(if_block);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (if_block) if_block.d(detaching);
    			if (detaching) detach_dev(if_block_anchor);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$r.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function slide(node, { duration }) {
    	return {
    		duration,
    		css: t => {
    			return `transform: translate(-50%, -${100 - t * 100}%);`;
    		}
    	};
    }

    function instance$r($$self, $$props, $$invalidate) {
    	let $gameView;
    	validate_store(gameView, "gameView");
    	component_subscribe($$self, gameView, $$value => $$invalidate(7, $gameView = $$value));
    	const targetNotifications = IS_VK ? notifications.vk : notifications.widget;
    	let notificationIndex = 0;
    	let isOpen = false;
    	const toggle = () => $$invalidate(0, isOpen = !isOpen);
    	let timeoutId = null;

    	const scheduleNotification = () => {
    		const timeout = 12000 * (notificationIndex + 1);
    		clearTimeout(timeoutId);
    		timeoutId = setTimeout(showNotification, timeout);
    	};

    	const showNotification = () => {
    		if (!targetNotifications[notificationIndex]) return;

    		if ($gameView !== GAME_VIEWS.GAME && $gameView !== GAME_VIEWS.QUIZ) {
    			playSample("SMS");
    			toggle();
    			sendMetrik("ViewPush", { shownPushId: notification.id });
    		}

    		scheduleNotification();
    	};

    	gameView.subscribe(state => {
    		if (state !== GAME_VIEWS.GAME && state !== GAME_VIEWS.QUIZ && targetNotifications[notificationIndex]) {
    			clearTimeout(timeoutId);
    			timeoutId = setTimeout(showNotification, 1000);
    		}
    	});

    	const notificationClick = () => sendMetrik("PushPush", { clickedPushId: notification.id });
    	const isAndroid = (/android/i).test(navigator.userAgent);
    	const isIos = (/iPad|iPhone|iPod/).test(navigator.userAgent);
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<NotificationController> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("NotificationController", $$slots, []);

    	$$self.$capture_state = () => ({
    		BASE_URL,
    		IS_VK,
    		IS_MOBILE_VK,
    		playSample,
    		gameView,
    		GAME_VIEWS,
    		sendMetrik,
    		NotificationIos,
    		NotificationAndroid,
    		notifications,
    		targetNotifications,
    		notificationIndex,
    		isOpen,
    		toggle,
    		timeoutId,
    		scheduleNotification,
    		showNotification,
    		notificationClick,
    		slide,
    		isAndroid,
    		isIos,
    		notification,
    		$gameView
    	});

    	$$self.$inject_state = $$props => {
    		if ("notificationIndex" in $$props) $$invalidate(5, notificationIndex = $$props.notificationIndex);
    		if ("isOpen" in $$props) $$invalidate(0, isOpen = $$props.isOpen);
    		if ("timeoutId" in $$props) timeoutId = $$props.timeoutId;
    		if ("notification" in $$props) $$invalidate(1, notification = $$props.notification);
    	};

    	let notification;

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	$$self.$$.update = () => {
    		if ($$self.$$.dirty & /*isOpen, notificationIndex*/ 33) {
    			 if (isOpen) {
    				setTimeout(
    					() => {
    						$$invalidate(5, notificationIndex++, notificationIndex);
    						toggle();
    					},
    					5000
    				);
    			}
    		}

    		if ($$self.$$.dirty & /*notificationIndex*/ 32) {
    			 $$invalidate(1, notification = targetNotifications[notificationIndex]);
    		}
    	};

    	return [isOpen, notification, notificationClick, isAndroid, isIos];
    }

    class NotificationController extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$r, create_fragment$r, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "NotificationController",
    			options,
    			id: create_fragment$r.name
    		});
    	}
    }

    /* src/components/HeaderFiles.svelte generated by Svelte v3.20.1 */

    const { document: document_1 } = globals;
    const file$r = "src/components/HeaderFiles.svelte";

    // (42:2) {#if IS_WIDGET}
    function create_if_block$a(ctx) {
    	let link;
    	let link_href_value;
    	let t0;
    	let style;

    	const block = {
    		c: function create() {
    			link = element("link");
    			t0 = space();
    			style = element("style");
    			style.textContent = "#covidWidget .playWithSound {\n        position: absolute;\n        width: 100%;\n        height: 100%;\n        background: white;\n        display: flex;\n        flex-direction: column;\n        align-items: center;\n        padding-top: 200px;\n        font-family: -apple-system, BlinkMacSystemFont, \"Segoe UI\", Roboto,\n          Oxygen, Ubuntu, Cantarell, \"Open Sans\", \"Helvetica Neue\", sans-serif;\n        color: var(--color1);\n      }\n      #covidWidget .playWithSoundText {\n        padding-top: 8px;\n        text-align: center;\n        white-space: pre-wrap;\n      }\n      #covidWidget .loader {\n        margin-top: 23px;\n        margin-bottom: 24px;\n        min-width: 300px;\n        height: 4px;\n        border-radius: 4px;\n        border: 1px solid var(--color2);\n      }\n      #covidWidget .indicator {\n        background: var(--color2);\n        height: 100%;\n      }\n      #covidWidget.widget {\n        width: 100%;\n        display: flex;\n        justify-content: center;\n        position: relative;\n        --color1: #ef3340;\n        --color2: #c8161d;\n      }\n      #covidWidget .container {\n        flex-grow: 1;\n        display: flex;\n        flex-direction: column;\n        align-items: center;\n        padding: 24px 0;\n        max-width: 600px;\n        justify-content: flex-start;\n      }\n      #covidWidget .text {\n        font-family: -apple-system, BlinkMacSystemFont, \"Segoe UI\", Roboto,\n          Oxygen, Ubuntu, Cantarell, \"Open Sans\", \"Helvetica Neue\", sans-serif;\n        color: var(--color1);\n        padding: 8px 16px;\n        text-align: center;\n      }\n      #covidWidget .primary {\n        font-size: 40px;\n      }\n      #covidWidget .secondary {\n        font-size: 19px;\n      }\n      #covidWidget .button {\n        border: none;\n        outline: none;\n        min-width: 300px;\n        padding: 16px;\n        background: #c8161d;\n        border-radius: 20px;\n        color: white;\n        font-weight: 600;\n        font-size: 18px;\n        font-family: -apple-system, BlinkMacSystemFont, \"Segoe UI\", Roboto,\n          Oxygen, Ubuntu, Cantarell, \"Open Sans\", \"Helvetica Neue\", sans-serif;\n      }\n      #covidWidget .introText {\n        font-family: -apple-system, BlinkMacSystemFont, \"Segoe UI\", Roboto,\n          Oxygen, Ubuntu, Cantarell, \"Open Sans\", \"Helvetica Neue\", sans-serif;\n        color: var(--color1);\n        padding: 8px 16px;\n        text-align: center;\n      }\n      #covidWidget.isVk {\n        position: fixed;\n        top: 0px;\n        left: 0px;\n        bottom: 0px;\n        right: 0px;\n        display: flex;\n        align-items: center;\n        justify-content: center;\n      }\n      #covidWidget.isVk .container {\n        height: 100%;\n        justify-content: space-around;\n        overflow: auto;\n      }\n      #covidWidget.isVk .playWithSound {\n        position: fixed;\n        top: 0px;\n        left: 0px;\n        bottom: 0px;\n        right: 0px;\n        justify-content: center;\n        padding-top: 0;\n      }\n      #covidWidget .loaderContainer {\n        padding: 0 24px;\n        display: flex;\n        flex-direction: column;\n        align-items: center;\n      }\n      #covidWidget.isVk .primary {\n        margin-top: 50px;\n      }\n      #covidWidget .bottomContainer {\n        position: relative;\n      }\n      #covidWidget .hidden {\n        visibility: hidden;\n      }\n      #covidWidget .startButton {\n        position: absolute;\n        left: 50%;\n        transform: translate(-50%, 0%);\n        top: 0%;\n      }\n      #covidWidget.isVk .startButton {\n        transform: translate(-50%, -50%);\n        top: 50%;\n      }";
    			attr_dev(link, "rel", "stylesheet");
    			attr_dev(link, "href", link_href_value = `${BASE_URL}/build/bundle.css`);
    			add_location(link, file$r, 42, 4, 934);
    			add_location(style, file$r, 43, 4, 1002);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, link, anchor);
    			insert_dev(target, t0, anchor);
    			insert_dev(target, style, anchor);
    		},
    		p: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(link);
    			if (detaching) detach_dev(t0);
    			if (detaching) detach_dev(style);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$a.name,
    		type: "if",
    		source: "(42:2) {#if IS_WIDGET}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$s(ctx) {
    	let style;
    	let if_block_anchor;
    	let if_block = IS_WIDGET && create_if_block$a(ctx);

    	const block = {
    		c: function create() {
    			style = element("style");
    			style.textContent = "html {\n      overflow: hidden;\n      width: 100%;\n    }\n\n    body {\n      position: fixed;\n      /* prevent overscroll bounce*/\n      overflow-y: scroll;\n      -webkit-overflow-scrolling: touch;\n      /* iOS velocity scrolling */\n      width: 100%;\n      margin: 0;\n    }\n  ";
    			if (if_block) if_block.c();
    			if_block_anchor = empty();
    			add_location(style, file$r, 25, 2, 617);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			append_dev(document_1.head, style);
    			if (if_block) if_block.m(document_1.head, null);
    			append_dev(document_1.head, if_block_anchor);
    		},
    		p: function update(ctx, [dirty]) {
    			if (IS_WIDGET) if_block.p(ctx, dirty);
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			detach_dev(style);
    			if (if_block) if_block.d(detaching);
    			detach_dev(if_block_anchor);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$s.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$s($$self, $$props, $$invalidate) {
    	(function (m, e, t, r, i, k, a) {
    		m[i] = m[i] || function () {
    			(m[i].a = m[i].a || []).push(arguments);
    		};

    		m[i].l = 1 * new Date();
    		(k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a));
    	})(window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    	ym(53183122, "init", {
    		clickmap: true,
    		trackLinks: true,
    		accurateTrackBounce: true,
    		webvisor: false
    	});

    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<HeaderFiles> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("HeaderFiles", $$slots, []);
    	$$self.$capture_state = () => ({ BASE_URL, IS_WIDGET });
    	return [];
    }

    class HeaderFiles extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$s, create_fragment$s, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "HeaderFiles",
    			options,
    			id: create_fragment$s.name
    		});
    	}
    }

    /* Points - v0.1.1 - 2013-07-11
     * Another Pointer Events polyfill
     * http://rich-harris.github.io/Points
     * Copyright (c) 2013 Rich Harris; Released under the MIT License */



    (function () {

    	var activePointers,
    		numActivePointers,
    		recentTouchStarts,
    		i,
    		setUpMouseEvent,
    		createUIEvent,
    		createEvent,
    		createMouseProxyEvent,
    		mouseEventIsSimulated,
    		createTouchProxyEvent,
    		buttonsMap,
    		pointerEventProperties;


    	// Pointer events supported? Great, nothing to do, let's go home
    	if ( window.onpointerdown !== undefined ) {
    		return;
    	}

    	pointerEventProperties = 'screenX screenY clientX clientY ctrlKey shiftKey altKey metaKey relatedTarget detail button buttons pointerId pointerType width height pressure tiltX tiltY isPrimary'.split( ' ' );

    	// Can we create events using the MouseEvent constructor? If so, gravy
    	try {
    		i = new UIEvent( 'test' );

    		createUIEvent = function ( type, bubbles ) {
    			return new UIEvent( type, { view: window, bubbles: bubbles });
    		};

    	// otherwise we need to do things oldschool
    	} catch ( err ) {
    		if ( document.createEvent ) {
    			createUIEvent = function ( type, bubbles ) {
    				var pointerEvent = document.createEvent( 'UIEvents' );
    				pointerEvent.initUIEvent( type, bubbles, true, window );

    				return pointerEvent;
    			};
    		}
    	}

    	if ( !createUIEvent ) {
    		throw new Error( 'Cannot create events. You may be using an unsupported browser.' );
    	}

    	createEvent = function ( type, originalEvent, params, noBubble ) {
    		var pointerEvent, i;

    		pointerEvent = createUIEvent( type, !noBubble );

    		i = pointerEventProperties.length;
    		while ( i-- ) {
    			Object.defineProperty( pointerEvent, pointerEventProperties[i], {
    				value: params[ pointerEventProperties[i] ],
    				writable: false
    			});
    		}

    		Object.defineProperty( pointerEvent, 'originalEvent', {
    			value: originalEvent,
    			writable: false
    		});

    		Object.defineProperty( pointerEvent, 'preventDefault', {
    			value: preventDefault,
    			writable: false
    		});

    		return pointerEvent;
    	};


    	// add pointerEnabled property to navigator
    	navigator.pointerEnabled = true;


    	// If we're in IE10, these events are already supported, except prefixed
    	if ( window.onmspointerdown !== undefined ) {
    		[ 'MSPointerDown', 'MSPointerUp', 'MSPointerCancel', 'MSPointerMove', 'MSPointerOver', 'MSPointerOut' ].forEach( function ( prefixed ) {
    			var unprefixed;

    			unprefixed = prefixed.toLowerCase().substring( 2 );

    			// pointerenter and pointerleave are special cases
    			if ( unprefixed === 'pointerover' || unprefixed === 'pointerout' ) {
    				window.addEventListener( prefixed, function ( originalEvent ) {
    					var unprefixedEvent = createEvent( unprefixed, originalEvent, originalEvent, false );
    					originalEvent.target.dispatchEvent( unprefixedEvent );

    					if ( !originalEvent.target.contains( originalEvent.relatedTarget ) ) {
    						unprefixedEvent = createEvent( ( unprefixed === 'pointerover' ? 'pointerenter' : 'pointerleave' ), originalEvent, originalEvent, true );
    						originalEvent.target.dispatchEvent( unprefixedEvent );
    					}
    				}, true );
    			}

    			else {
    				window.addEventListener( prefixed, function ( originalEvent ) {
    					var unprefixedEvent = createEvent( unprefixed, originalEvent, originalEvent, false );
    					originalEvent.target.dispatchEvent( unprefixedEvent );
    				}, true );
    			}
    		});

    		navigator.maxTouchPoints = navigator.msMaxTouchPoints;

    		// Nothing more to do.
    		return;
    	}


    	// https://dvcs.w3.org/hg/pointerevents/raw-file/tip/pointerEvents.html#dfn-chorded-buttons
    	buttonsMap = {
    		0: 1,
    		1: 4,
    		2: 2
    	};

    	createMouseProxyEvent = function ( type, originalEvent, noBubble ) {
    		var button, buttons, pressure, params;

    		// normalise button and buttons
    		if ( originalEvent.buttons !== undefined ) {
    			buttons = originalEvent.buttons;
    			button = !originalEvent.buttons ? -1 : originalEvent.button;
    		}

    		else {
    			if ( event.button === 0 && event.which === 0 ) {
    				button = -1;
    				buttons = 0;
    			} else {
    				button = originalEvent.button;
    				buttons = buttonsMap[ button ];
    			}
    		}

    		// Pressure is 0.5 for buttons down, 0 for no buttons down (unless pressure is
    		// reported, obvs)
    		pressure = originalEvent.pressure || originalEvent.mozPressure || ( buttons ? 0.5 : 0 );


    		// This is the quickest way to copy event parameters. You can't enumerate
    		// over event properties in Firefox (possibly elsewhere), so a traditional
    		// extend function won't work
    		params = {
    			screenX:       originalEvent.screenX,
    			screenY:       originalEvent.screenY,
    			clientX:       originalEvent.clientX,
    			clientY:       originalEvent.clientY,
    			ctrlKey:       originalEvent.ctrlKey,
    			shiftKey:      originalEvent.shiftKey,
    			altKey:        originalEvent.altKey,
    			metaKey:       originalEvent.metaKey,
    			relatedTarget: originalEvent.relatedTarget,
    			detail:        originalEvent.detail,
    			button:        button,
    			buttons:       buttons,

    			pointerId:     1,
    			pointerType:   'mouse',
    			width:         0,
    			height:        0,
    			pressure:      pressure,
    			tiltX:         0,
    			tiltY:         0,
    			isPrimary:     true,

    			preventDefault: preventDefault
    		};

    		return createEvent( type, originalEvent, params, noBubble );
    	};

    	// Some mouse events are real, others are simulated based on touch events.
    	// We only want the real ones, or we'll end up firing our load at
    	// inappropriate moments.
    	//
    	// Surprisingly, the coordinates of the mouse event won't exactly correspond
    	// with the touchstart that originated them, so we need to be a bit fuzzy.
    	if ( window.ontouchstart !== undefined ) {
    		mouseEventIsSimulated = function ( event ) {
    			var i = recentTouchStarts.length, threshold = 10, touch;
    			while ( i-- ) {
    				touch = recentTouchStarts[i];
    				if ( Math.abs( event.clientX - touch.clientX ) < threshold && Math.abs( event.clientY - touch.clientY ) < threshold ) {
    					return true;
    				}
    			}
    		};
    	} else {
    		mouseEventIsSimulated = function () {
    			return false;
    		};
    	}



    	setUpMouseEvent = function ( type ) {
    		if ( type === 'over' || type === 'out' ) {
    			window.addEventListener( 'mouse' + type, function ( originalEvent ) {
    				var pointerEvent;

    				if ( mouseEventIsSimulated( originalEvent ) ) {
    					return;
    				}

    				pointerEvent = createMouseProxyEvent( 'pointer' + type, originalEvent );
    				originalEvent.target.dispatchEvent( pointerEvent );

    				if ( !originalEvent.target.contains( originalEvent.relatedTarget ) ) {
    					pointerEvent = createMouseProxyEvent( ( type === 'over' ? 'pointerenter' : 'pointerleave' ), originalEvent, true );
    					originalEvent.target.dispatchEvent( pointerEvent );
    				}
    			});
    		}

    		else {
    			window.addEventListener( 'mouse' + type, function ( originalEvent ) {
    				var pointerEvent;

    				if ( mouseEventIsSimulated( originalEvent ) ) {
    					return;
    				}

    				pointerEvent = createMouseProxyEvent( 'pointer' + type, originalEvent );
    				originalEvent.target.dispatchEvent( pointerEvent );
    			});
    		}
    	};

    	[ 'down', 'up', 'over', 'out', 'move' ].forEach( function ( eventType ) {
    		setUpMouseEvent( eventType );
    	});





    	// Touch events:
    	if ( window.ontouchstart !== undefined ) {
    		// Set up a registry of current touches
    		activePointers = {};
    		numActivePointers = 0;

    		// Maintain a list of recent touchstarts, so we can eliminate simulate
    		// mouse events later
    		recentTouchStarts = [];

    		createTouchProxyEvent = function ( type, originalEvent, touch, noBubble, relatedTarget ) {
    			var params;

    			params = {
    				screenX:       originalEvent.screenX,
    				screenY:       originalEvent.screenY,
    				clientX:       touch.clientX,
    				clientY:       touch.clientY,
    				ctrlKey:       originalEvent.ctrlKey,
    				shiftKey:      originalEvent.shiftKey,
    				altKey:        originalEvent.altKey,
    				metaKey:       originalEvent.metaKey,
    				relatedTarget: relatedTarget || originalEvent.relatedTarget, // TODO is this right? also: mouseenter/leave?
    				detail:        originalEvent.detail,
    				button:        0,
    				buttons:       1,

    				pointerId:     touch.identifier + 2, // ensure no collisions between touch and mouse pointer IDs
    				pointerType:   'touch',
    				width:         20, // roughly how fat people's fingers are
    				height:        20,
    				pressure:      0.5,
    				tiltX:         0,
    				tiltY:         0,
    				isPrimary:     activePointers[ touch.identifier ].isPrimary,

    				preventDefault: preventDefault
    			};

    			return createEvent( type, originalEvent, params, noBubble );
    		};

    		// touchstart
    		window.addEventListener( 'touchstart', function ( event ) {
    			var touches, processTouch;

    			touches = event.changedTouches;

    			processTouch = function ( touch ) {
    				var pointerdownEvent, pointeroverEvent, pointerenterEvent, pointer;

    				pointer = {
    					target: touch.target,
    					isPrimary: numActivePointers ? false : true
    				};

    				activePointers[ touch.identifier ] = pointer;
    				numActivePointers += 1;

    				pointerdownEvent = createTouchProxyEvent( 'pointerdown', event, touch );
    				pointeroverEvent = createTouchProxyEvent( 'pointerover', event, touch );
    				pointerenterEvent = createTouchProxyEvent( 'pointerenter', event, touch, true );

    				touch.target.dispatchEvent( pointeroverEvent );
    				touch.target.dispatchEvent( pointerenterEvent );
    				touch.target.dispatchEvent( pointerdownEvent );

    				// we need to keep track of recent touchstart events, so we can test
    				// whether later mouse events are simulated
    				recentTouchStarts.push( touch );
    				setTimeout( function () {
    					var index = recentTouchStarts.indexOf( touch );
    					if ( index !== -1 ) {
    						recentTouchStarts.splice( index, 1 );
    					}
    				}, 1500 );
    			};

    			for ( i=0; i<touches.length; i+=1 ) {
    				processTouch( touches[i] );
    			}
    		});

    		// touchmove
    		window.addEventListener( 'touchmove', function ( event ) {
    			var touches, processTouch;

    			touches = event.changedTouches;

    			processTouch = function ( touch ) {
    				var pointermoveEvent, pointeroverEvent, pointeroutEvent, pointerenterEvent, pointerleaveEvent, pointer, previousTarget, actualTarget;

    				pointer = activePointers[ touch.identifier ];
    				actualTarget = document.elementFromPoint( touch.clientX, touch.clientY );

    				if ( pointer.target === actualTarget ) {
    					// just fire a touchmove event
    					pointermoveEvent = createTouchProxyEvent( 'pointermove', event, touch );
    					actualTarget.dispatchEvent( pointermoveEvent );
    					return;
    				}


    				// target has changed - we need to fire a pointerout (and possibly pointerleave)
    				// event on the previous target, and a pointerover (and possibly pointerenter)
    				// event on the current target. Then we fire the pointermove event on the current
    				// target

    				previousTarget = pointer.target;
    				pointer.target = actualTarget;

    				// pointerleave
    				if ( !previousTarget.contains( actualTarget ) ) {
    					// new target is not a child of previous target, so fire pointerleave on previous
    					pointerleaveEvent = createTouchProxyEvent( 'pointerleave', event, touch, true, actualTarget );
    					previousTarget.dispatchEvent( pointerleaveEvent );
    				}

    				// pointerout
    				pointeroutEvent = createTouchProxyEvent( 'pointerout', event, touch, false );
    				previousTarget.dispatchEvent( pointeroutEvent );

    				// pointermove
    				pointermoveEvent = createTouchProxyEvent( 'pointermove', event, touch, false );
    				actualTarget.dispatchEvent( pointermoveEvent );

    				// pointerover
    				pointeroverEvent = createTouchProxyEvent( 'pointerover', event, touch, false );
    				actualTarget.dispatchEvent( pointeroverEvent );

    				// pointerenter
    				if ( !actualTarget.contains( previousTarget ) ) {
    					// previous target is not a child of current target, so fire pointerenter on current
    					pointerenterEvent = createTouchProxyEvent( 'pointerenter', event, touch, true, previousTarget );
    					actualTarget.dispatchEvent( pointerenterEvent );
    				}
    			};

    			for ( i=0; i<touches.length; i+=1 ) {
    				processTouch( touches[i] );
    			}
    		});

    		// touchend
    		window.addEventListener( 'touchend', function ( event ) {
    			var touches, processTouch;

    			touches = event.changedTouches;

    			processTouch = function ( touch ) {
    				var pointerupEvent, pointeroutEvent, pointerleaveEvent, actualTarget;

    				actualTarget = document.elementFromPoint( touch.clientX, touch.clientY );

    				pointerupEvent = createTouchProxyEvent( 'pointerup', event, touch, false );
    				pointeroutEvent = createTouchProxyEvent( 'pointerout', event, touch, false );
    				pointerleaveEvent = createTouchProxyEvent( 'pointerleave', event, touch, true );

    				delete activePointers[ touch.identifier ];
    				numActivePointers -= 1;

    				actualTarget.dispatchEvent( pointerupEvent );
    				actualTarget.dispatchEvent( pointeroutEvent );
    				actualTarget.dispatchEvent( pointerleaveEvent );
    			};

    			for ( i=0; i<touches.length; i+=1 ) {
    				processTouch( touches[i] );
    			}
    		});

    		// touchcancel
    		window.addEventListener( 'touchcancel', function ( event ) {
    			var touches, processTouch;

    			touches = event.changedTouches;

    			processTouch = function ( touch ) {
    				var pointercancelEvent, pointeroutEvent, pointerleaveEvent;

    				pointercancelEvent = createTouchProxyEvent( 'pointercancel', event, touch );
    				pointeroutEvent = createTouchProxyEvent( 'pointerout', event, touch );
    				pointerleaveEvent = createTouchProxyEvent( 'pointerleave', event, touch );

    				touch.target.dispatchEvent( pointercancelEvent );
    				touch.target.dispatchEvent( pointeroutEvent );
    				touch.target.dispatchEvent( pointerleaveEvent );

    				delete activePointers[ touch.identifier ];
    				numActivePointers -= 1;
    			};

    			for ( i=0; i<touches.length; i+=1 ) {
    				processTouch( touches[i] );
    			}
    		});
    	}


    	// Single preventDefault function - no point recreating it over and over
    	function preventDefault () {
    		this.originalEvent.preventDefault();
    	}

    	// TODO stopPropagation?

    }());

    /* src/App.svelte generated by Svelte v3.20.1 */
    const file$s = "src/App.svelte";

    // (45:0) {:else}
    function create_else_block$4(ctx) {
    	let div;
    	let t;
    	let div_intro;
    	let div_outro;
    	let current;
    	const game = new Game({ $$inline: true });
    	const notificationcontroller = new NotificationController({ $$inline: true });

    	const block = {
    		c: function create() {
    			div = element("div");
    			create_component(game.$$.fragment);
    			t = space();
    			create_component(notificationcontroller.$$.fragment);
    			attr_dev(div, "id", "covidsvelteGame");
    			attr_dev(div, "class", "game svelte-glb2dv");
    			add_location(div, file$s, 45, 2, 1274);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			mount_component(game, div, null);
    			append_dev(div, t);
    			mount_component(notificationcontroller, div, null);
    			/*div_binding*/ ctx[7](div);
    			current = true;
    		},
    		p: noop,
    		i: function intro(local) {
    			if (current) return;
    			transition_in(game.$$.fragment, local);
    			transition_in(notificationcontroller.$$.fragment, local);

    			add_render_callback(() => {
    				if (div_outro) div_outro.end(1);
    				if (!div_intro) div_intro = create_in_transition(div, /*receive*/ ctx[5], { key: "game" });
    				div_intro.start();
    			});

    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(game.$$.fragment, local);
    			transition_out(notificationcontroller.$$.fragment, local);
    			if (div_intro) div_intro.invalidate();
    			div_outro = create_out_transition(div, /*send*/ ctx[4], { key: "game" });
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			destroy_component(game);
    			destroy_component(notificationcontroller);
    			/*div_binding*/ ctx[7](null);
    			if (detaching && div_outro) div_outro.end();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_else_block$4.name,
    		type: "else",
    		source: "(45:0) {:else}",
    		ctx
    	});

    	return block;
    }

    // (41:0) {#if !$gameState}
    function create_if_block$b(ctx) {
    	let div;
    	let div_intro;
    	let div_outro;
    	let current;
    	const widget = new Widget({ $$inline: true });

    	const block = {
    		c: function create() {
    			div = element("div");
    			create_component(widget.$$.fragment);
    			add_location(div, file$s, 41, 2, 1194);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			mount_component(widget, div, null);
    			current = true;
    		},
    		p: noop,
    		i: function intro(local) {
    			if (current) return;
    			transition_in(widget.$$.fragment, local);

    			add_render_callback(() => {
    				if (div_outro) div_outro.end(1);
    				if (!div_intro) div_intro = create_in_transition(div, /*receive*/ ctx[5], { key: "widget" });
    				div_intro.start();
    			});

    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(widget.$$.fragment, local);
    			if (div_intro) div_intro.invalidate();
    			div_outro = create_out_transition(div, fade, {});
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			destroy_component(widget);
    			if (detaching && div_outro) div_outro.end();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$b.name,
    		type: "if",
    		source: "(41:0) {#if !$gameState}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$t(ctx) {
    	let current_block_type_index;
    	let if_block;
    	let t;
    	let current;
    	let dispose;
    	add_render_callback(/*onwindowresize*/ ctx[6]);
    	const if_block_creators = [create_if_block$b, create_else_block$4];
    	const if_blocks = [];

    	function select_block_type(ctx, dirty) {
    		if (!/*$gameState*/ ctx[2]) return 0;
    		return 1;
    	}

    	current_block_type_index = select_block_type(ctx);
    	if_block = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);
    	const headerfiles = new HeaderFiles({ $$inline: true });

    	const block = {
    		c: function create() {
    			if_block.c();
    			t = space();
    			create_component(headerfiles.$$.fragment);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor, remount) {
    			if_blocks[current_block_type_index].m(target, anchor);
    			insert_dev(target, t, anchor);
    			mount_component(headerfiles, target, anchor);
    			current = true;
    			if (remount) dispose();
    			dispose = listen_dev(window, "resize", /*onwindowresize*/ ctx[6]);
    		},
    		p: function update(ctx, [dirty]) {
    			let previous_block_index = current_block_type_index;
    			current_block_type_index = select_block_type(ctx);

    			if (current_block_type_index === previous_block_index) {
    				if_blocks[current_block_type_index].p(ctx, dirty);
    			} else {
    				group_outros();

    				transition_out(if_blocks[previous_block_index], 1, 1, () => {
    					if_blocks[previous_block_index] = null;
    				});

    				check_outros();
    				if_block = if_blocks[current_block_type_index];

    				if (!if_block) {
    					if_block = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);
    					if_block.c();
    				}

    				transition_in(if_block, 1);
    				if_block.m(t.parentNode, t);
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(if_block);
    			transition_in(headerfiles.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(if_block);
    			transition_out(headerfiles.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if_blocks[current_block_type_index].d(detaching);
    			if (detaching) detach_dev(t);
    			destroy_component(headerfiles, detaching);
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$t.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$t($$self, $$props, $$invalidate) {
    	let $gameHeight;
    	let $gameState;
    	let $gameWidth;
    	validate_store(gameHeight, "gameHeight");
    	component_subscribe($$self, gameHeight, $$value => $$invalidate(1, $gameHeight = $$value));
    	validate_store(gameState, "gameState");
    	component_subscribe($$self, gameState, $$value => $$invalidate(2, $gameState = $$value));
    	validate_store(gameWidth, "gameWidth");
    	component_subscribe($$self, gameWidth, $$value => $$invalidate(3, $gameWidth = $$value));
    	if (IS_VK) initVk();
    	let gameRef = null;
    	const [send, receive] = crossfade({ duration: 200, fallback: scale });
    	document.addEventListener("visibilitychange", onVisibilityChange);

    	onDestroy(() => {
    		document.body.removeChild(gameRef);
    		document.removeEventListener("visibilitychange", onVisibilityChange);
    	});

    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<App> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("App", $$slots, []);

    	function onwindowresize() {
    		gameWidth.set($gameWidth = window.innerWidth);
    		gameHeight.set($gameHeight = window.innerHeight);
    	}

    	function div_binding($$value) {
    		binding_callbacks[$$value ? "unshift" : "push"](() => {
    			$$invalidate(0, gameRef = $$value);
    		});
    	}

    	$$self.$capture_state = () => ({
    		crossfade,
    		scale,
    		fade,
    		initVk,
    		preloadIndicator,
    		IS_VK,
    		onDestroy,
    		Game,
    		Widget,
    		NotificationController,
    		gameWidth,
    		gameHeight,
    		gameState,
    		onVisibilityChange,
    		HeaderFiles,
    		gameRef,
    		send,
    		receive,
    		$gameHeight,
    		$gameState,
    		$gameWidth
    	});

    	$$self.$inject_state = $$props => {
    		if ("gameRef" in $$props) $$invalidate(0, gameRef = $$props.gameRef);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	$$self.$$.update = () => {
    		if ($$self.$$.dirty & /*gameRef*/ 1) {
    			 if (gameRef) {
    				document.body.appendChild(gameRef);
    			}
    		}

    		if ($$self.$$.dirty & /*$gameHeight*/ 2) {
    			 {
    				document.querySelector("html").style.height = `${$gameHeight - 1}px`;
    				document.querySelector("body").style.height = `${$gameHeight - 1}px`;
    			}
    		}
    	};

    	return [
    		gameRef,
    		$gameHeight,
    		$gameState,
    		$gameWidth,
    		send,
    		receive,
    		onwindowresize,
    		div_binding
    	];
    }

    class App extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$t, create_fragment$t, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "App",
    			options,
    			id: create_fragment$t.name
    		});
    	}
    }

    const app = new App({
    	target: document.getElementById('covidsvelte'),
    	
    });

    return app;

}());
//# sourceMappingURL=bundle.js.map
