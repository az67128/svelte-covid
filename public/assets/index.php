<?php
$filename = $_GET["file"];

//echo finfo_file($finfo, $filename);
if(file_exists($filename)){
    
    header("Access-Control-Allow-Origin: *");
    //Get file type and set it as Content Type
    $finfo = finfo_open(FILEINFO_MIME_TYPE);
    //echo finfo_file($finfo, $filename);
    header('Content-Type: ' . finfo_file($finfo, $filename));
    // header('Content-Type: image/jpeg;');
    finfo_close($finfo);

    //Use Content-Disposition: attachment to specify the filename
    header('Content-Disposition: attachment; filename='.basename($filename));

    //No cache
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');

    //Define file size
    header('Content-Length: ' . filesize($filename));

    ob_clean();
    flush();
    readfile($filename);
    exit;
}
?>