# Defend humanity, arcade game COVID-19
Save earth, defeat virus!

## [Play now](https://az67128.gitlab.io/svelte-covid/)
### [VK](https://vk.com/services?w=app7403841_41081915)
### [Screenlifer](https://screenlifer.com/immunitet/)
![](./public/assets/logo.png)

## [News](https://www.rbc.ru/technology_and_media/29/04/2020/5ea96e759a794736a849a55a)


To install on your site use code:
```html
<div id="covidsvelte"></div>
<script src="https://az67128.gitlab.io/svelte-covid/build/bundle.js"></script>
```

To run local 
```bash
npm i
npm run dev
```

Made with [svelte](https://svelte.dev/)
